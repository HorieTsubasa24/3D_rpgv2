﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System.IO;
#endif

public class CreateAssetBundle : MonoBehaviour {
    [ContextMenu("CreateAssetBundle")]
    public void MakeAssetBundle()
    {
#if UNITY_EDITOR
        Delete(Application.streamingAssetsPath + "/Bundles/Windows");
        Delete(Application.streamingAssetsPath + "/Bundles/Mac");

        Directory.CreateDirectory(Application.streamingAssetsPath + "/Bundles/Windows");
        Directory.CreateDirectory(Application.streamingAssetsPath + "/Bundles/Mac");

        BuildPipeline.BuildAssetBundles("Assets/StreamingAssets/Bundles/Windows", BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
        BuildPipeline.BuildAssetBundles("Assets/StreamingAssets/Bundles/Mac", BuildAssetBundleOptions.None, BuildTarget.StandaloneOSX);
    }

    //Assetsディレクトリ以下にあるTestディレクトリを削除
    /// <summary>
    /// 指定したディレクトリとその中身を全て削除する
    /// </summary>
    public static void Delete(string targetDirectoryPath)
    {
        if (!Directory.Exists(targetDirectoryPath))
        {
            return;
        }

        //ディレクトリ以外の全ファイルを削除
        string[] filePaths = Directory.GetFiles(targetDirectoryPath);
        foreach (string filePath in filePaths)
        {
            File.SetAttributes(filePath, FileAttributes.Normal);
            File.Delete(filePath);
        }

        //ディレクトリの中のディレクトリも再帰的に削除
        string[] directoryPaths = Directory.GetDirectories(targetDirectoryPath);
        foreach (string directoryPath in directoryPaths)
        {
            Delete(directoryPath);
        }

        //中が空になったらディレクトリ自身も削除
        Directory.Delete(targetDirectoryPath, false);
#endif
    }
}
