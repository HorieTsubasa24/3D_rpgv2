﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/// <summary>
/// 場面をコントロールする
/// </summary>
public class PheseManager : Singleton<PheseManager>
{
    [SerializeField]
    private GameObject currentSequence;

    [SerializeField]
    private GameObject Map;
    [SerializeField]
    private GameObject Battle;
    
    public delegate void LoadCallback(); //暗幕が表示仕切った時に表示される処理

    private void Start()
    {
        instance = this;
        currentSequence = Map;
    }

    /// <summary>
    /// Pheseの変更
    /// </summary>
    public void ChangeToGameMode(string str_gamemode, LoadCallback func)
    {
        switch (str_gamemode)
        {
            case "Map": LoadStart(func, Map); break;
            case "Battle": LoadStart(func, Battle); break;
        }
    }

    /// <summary>
    /// シーンをロード
    /// シーン遷移後コールバック
    /// </summary>
    /// <param name="func"></param>
    /// <param name="goSequence"></param>
    public void LoadStart(LoadCallback func, GameObject goSequence)
    {
        Sequence seq = DOTween.Sequence();
        
        seq.AppendCallback(() =>
        {
            currentSequence.SetActive(false);
            goSequence.SetActive(true);
            currentSequence = goSequence;
            if (func != null) func();
        });
    }
    
}