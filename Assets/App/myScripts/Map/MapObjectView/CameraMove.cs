﻿using System;
using UnityEngine;
using MapVariable;
using Minimap;
using DG.Tweening;
using System.Collections;

[System.Serializable]
public class CameraMove : Singleton<CameraMove>
{
    public bool isWallThrough = false;

    //-------外部のクラス--------

    /// <summary>
    /// マップ
    /// </summary>
    public MapTest_Legacy l_map;

    /// <summary>
    /// バトルの背景用カメラ。
    /// </summary>
    public Transform BattleWallCamera;

    /// <summary>
    /// データクラス
    /// </summary>
    private CommonData cd;

    /// <summary>
    /// コントローラー
    /// </summary>
    private PlayerController plycnt;

    /// <summary>
    /// マップ上のAudioListener
    /// </summary>
    [SerializeField]
    private AudioListener mapAudioListener;

    // プレイヤーのコライダー
    [SerializeField]
    private CapsuleCollider capsuleColider;

    // ひとマス移動用のtween
    private Tweener tween;
    private Ease ease = Ease.Linear;

    //--------内部で使うメソッドと変数---------
    /// <summary>
    /// 移動にかかる時間
    /// </summary>
    private const float MOVE_SPAN = 0.33f;
    
    /// <summary>
    /// 前フレーム方向キーが押されたか
    /// </summary>
    bool BEFORE_KEY = true;

    // 移動する向き
    public Arrow Move_Arrow = Arrow.NONE;

    public enum Arrow
    {
        NONE, FORWARD, FORWARD_WALL, RIGHT, DOWN, BACK, BACKWALL, LEFT, LEFTSHIFT, RIGHTSHIFT, LEFTSHIFT_WALL, RIGHTSHIFT_WALL, WARP
    }
    
    void Init()
    {
        CameraMove.instance = this;
        var value = TransformToNowPositionSet();
        cd.SetLocation((int)value.x, (int)value.y, (int)value.z);
        if (MapVariableManager.Instance.isInited)
            MapVariableManager.Instance.SetMapVariable(cd.Floor, cd.Posx, cd.Posy);
        cd.transformForSpawnToPlayer = gameObject.transform;
        cd.SetBeforeLocation(cd.Posx, cd.Posy, cd.Posd, cd.Floor, cd.Hierarchy);
    }

    // セーブからのロード 
    public static void InitPosition()
    {
        CameraMove.instance = FindObjectOfType<CameraMove>();
        CameraMove.Instance.transform.position = CommonData.Instance.IntPos.GetWorldVec();
    }
    
    /// <summary>
    /// キー入力ごとに移動セット
    /// </summary>
    void InputMoveRequest(bool MP)
    {
        // 移動許可がない
        if (MP == false)
            return;

        bool isLeftShift = Input.GetButton("LeftPad");
        bool isRightShift = Input.GetButton("RightPad");
        float x = isLeftShift ? -1.0f : (isRightShift ? 1.0f : Input.GetAxis("Horizontal"));
        float y = Input.GetAxis("Vertical");

        // 静止している状態
        if (Move_Arrow == Arrow.NONE)
        {
            // 方向キーが押されると移動開始セット
            if (Math.Abs(x) > 0.00f || Math.Abs(y) > 0.00f)
            {
                moveArrow_Set(x, y, isLeftShift || isRightShift);
            }
            else
            {
                BEFORE_KEY = false;
            }
        }
    }

    /// <summary>
    /// 方向先に通行不可能な壁があるか
    /// </summary>
    /// <returns><c>true</c>, 壁が存在する, <c>false</c> otherwise.</returns>
    /// <param name="wallArrow">Wall arrow.</param>
    private bool IsWallExist(int wallArrow)
    {
        if (isWallThrough)
            return false;

        return CHeckWallAllKind(l_map.GetWallKind(cd.IntPos, wallArrow));
    }

    private bool CHeckWallAllKind(int wallNo)
    {
        return wallNo == 1;
    }

    /// <summary>
    /// 壁を確認し方向ごとの行動設定
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="isShift"></param>
    void moveArrow_Set(float x, float y, bool isShift)
    {
        // 前フレームで方向キーが押されているとreturn
        if (BEFORE_KEY == true)
            return;

        // メニュー禁止
        plycnt.isMenuAllow = false;

        // 方向キーごとに移動する向きと、移動にかかる時間セット
        if (x >= 0.9f)
        {
            if (isShift)
            {
                if (IsWallExist(1))
                {
                    Move_Arrow = Arrow.RIGHTSHIFT_WALL;
                    BEFORE_KEY = true;
                    RegistMove();
                    return;
                }
                Move_Arrow = Arrow.RIGHTSHIFT;
                BEFORE_KEY = true;
                RegistMove();
                return;

            }
            Move_Arrow = Arrow.RIGHT;
            BEFORE_KEY = true;
            RegistMove();
            return;
        }
        if (x <= -0.9f)
        {
            if (isShift)
            {
                if (IsWallExist(-1))
                {
                    Move_Arrow = Arrow.LEFTSHIFT_WALL;
                    BEFORE_KEY = true;
                    RegistMove();
                    return;
                }
                Move_Arrow = Arrow.LEFTSHIFT;
                BEFORE_KEY = true;
                RegistMove();
                return;
            }
            Move_Arrow = Arrow.LEFT;
            BEFORE_KEY = true;
            RegistMove();
            return;
        }
        if (y >= 0.9f)
        {
            if (IsWallExist(0))
            {
                Move_Arrow = Arrow.FORWARD_WALL;
                BEFORE_KEY = true;
                RegistMove();
                return;
            }
            Move_Arrow = Arrow.FORWARD;
            BEFORE_KEY = true;
            RegistMove();
            return;
        }
        if (y <= -0.9f)
        {
            if (IsWallExist(2))
            {
                Move_Arrow = Arrow.BACKWALL;
                BEFORE_KEY = true;
                RegistMove();
                return;
            }
            Move_Arrow = Arrow.BACK;
            BEFORE_KEY = true;
            RegistMove();
            return;
        }
    }

    // Use this for initialization
    void OnEnable()
    {
        // データクラス
        cd = CommonData.Instance;

        // コントローラー
        plycnt = PlayerController.Instance;

        // 現在の外部からの参照用座標
        //cd.intPos = new Position(0, 0, 0, 1, 1);
        cd.SetLocation(0, 0, 0, 1, 1);
        if (MapVariableManager.Instance.isInited)
            MapVariableManager.Instance.SetMapVariable(1, 0, 0);

        // 復帰用のマップ座標設定読み込み
        if (cd.RetPosition.X != -1 && cd.RetPosition.Y != -1)
        {
            // AudioListener切り替え
            CameraMove.Instance.SetAudioListener(true);

            cd.SetLocationFromReturnPos();
            Debug.Log("CHECK:SetLocationFromReturnPos()");
            //cd.intPos = new Position(cd.RetPosition.x, cd.RetPosition.y, cd.RetPosition.direction, cd.RetPosition.hierarchy, cd.RetPosition.floor);
            gameObject.transform.position = new Vector3(cd.RetPosition.X, 0, -cd.RetPosition.Y);
            gameObject.transform.rotation = Quaternion.Euler(0, cd.RetPosition.Direction * 90f, 0);
        }

        Init();
    }

    /// <summary>
    /// 現在地の位置情報を丸めて情報にする
    /// </summary>
    public Vector3 TransformToNowPositionSet()
    {
        // x, y, zの整数化
        var xyz = gameObject.transform.position;
        var x = (int)Math.Round(xyz.x);
        var y = (int)Math.Round(xyz.y);
        var z = (int)Math.Round(xyz.z);
        gameObject.transform.position = new Vector3(x, y, z);
        BattleWallCamera.position = new Vector3(x, y, z);

        // rx, ry, rzの整数化
        var rot = gameObject.transform.localEulerAngles;
        var rx = (int)Math.Round(rot.x);
        var ry = (int)Math.Round(rot.y);
        var rz = (int)Math.Round(rot.z);
        gameObject.transform.rotation = Quaternion.Euler(rx, ry, rz);
        BattleWallCamera.rotation = Quaternion.Euler(rx, ry, rz);

        if (ry < 0) ry = -ry + 180;
        if (ry > 359) ry = 0;

        int d = ry / 90;

        return new Vector3(x, -z, d);
    }

    /// <summary>
    /// オーディオリスナーを切り替える
    /// </summary>
    /// <param name="toEnable"></param>
    public void SetAudioListener(bool toEnable)
    {
        mapAudioListener.enabled = toEnable;
    }

    /// <summary>
    /// 方向に応じた動きを登録する。
    /// </summary>
    private void RegistMove()
    {
        tween.Kill();
        switch (Move_Arrow)
        {
            case Arrow.RIGHT:
                tween = transform.DORotate(new Vector3(0, transform.rotation.eulerAngles.y + 90f), MOVE_SPAN, RotateMode.FastBeyond360).SetEase(ease).OnComplete(() => StepComplete(true));
                break;
            case Arrow.RIGHTSHIFT:
                tween = transform.DOMove(transform.position + Quaternion.Euler(0, 90, 0) * transform.forward, MOVE_SPAN).SetEase(ease).OnComplete(() => StepComplete(true));
                break;
            case Arrow.RIGHTSHIFT_WALL:
                tween = transform.DOMove(transform.position + Quaternion.Euler(0, 90, 0) * transform.forward / 4f, MOVE_SPAN / 4f)
                    .SetEase(ease).OnComplete(() =>
                    {
                        Map_Sound.Instance.WallHitPlay();
                        transform.DOMove(transform.position + Quaternion.Euler(0, -90, 0) * transform.forward / 4f, MOVE_SPAN / 4f).SetEase(ease).OnComplete(() => StepComplete(false));
                    });
                break;
            case Arrow.LEFT:
                tween = transform.DORotate(new Vector3(0, transform.rotation.eulerAngles.y - 90f), MOVE_SPAN, RotateMode.FastBeyond360).SetEase(ease).OnComplete(() => StepComplete(true));
                break;
            case Arrow.LEFTSHIFT:
                tween = transform.DOMove(transform.position + Quaternion.Euler(0, -90, 0) * transform.forward, MOVE_SPAN).SetEase(ease).OnComplete(() => StepComplete(true));
                break;
            case Arrow.LEFTSHIFT_WALL:
                tween = transform.DOMove(transform.position + Quaternion.Euler(0, -90, 0) * transform.forward / 4f, MOVE_SPAN / 4f)
                    .SetEase(ease).OnComplete(() =>
                    {
                        Map_Sound.Instance.WallHitPlay();
                        transform.DOMove(transform.position + Quaternion.Euler(0, 90, 0) * transform.forward / 4f, MOVE_SPAN / 4f).SetEase(ease).OnComplete(() => StepComplete(false));
                    });
                break;
            case Arrow.FORWARD:
                tween = transform.DOMove(transform.position + transform.forward, MOVE_SPAN).SetEase(ease).OnComplete(() => StepComplete(true));
                break;
            case Arrow.FORWARD_WALL:
                tween = transform.DOMove(transform.position + transform.forward / 4f, MOVE_SPAN / 4f)
                    .SetEase(ease).OnComplete(() =>
                    {
                        Map_Sound.Instance.WallHitPlay();
                        transform.DOMove(transform.position - transform.forward / 4f, MOVE_SPAN / 4f).SetEase(ease).OnComplete(() => StepComplete(false));
                    });
                break;
            case Arrow.BACK:
                tween = transform.DOMove(transform.position - transform.forward, MOVE_SPAN).SetEase(ease).OnComplete(() => StepComplete(true));
                break;
            case Arrow.BACKWALL:
                tween = transform.DOMove(transform.position - transform.forward / 4f, MOVE_SPAN / 4f)
                    .SetEase(ease).OnComplete(() =>
                    {
                        Map_Sound.Instance.WallHitPlay();
                        transform.DOMove(transform.position + transform.forward / 4f, MOVE_SPAN / 4f).SetEase(ease).OnComplete(() => StepComplete(false));
                    });
                break;
        }
    }

    /// <summary>
    /// 移動完了時の処理
    /// </summary>
    /// <param name="steped"></param>
    private void StepComplete(bool steped)
    {
        if (steped)
            Map_Sound.Instance.WalkSoundPlay();

        BEFORE_KEY = false;
        bool isStep = (Move_Arrow == Arrow.FORWARD || Move_Arrow == Arrow.BACK || Move_Arrow == Arrow.RIGHTSHIFT || Move_Arrow == Arrow.LEFTSHIFT);
        Move_Arrow = Arrow.NONE;

        // マス処理を一括して実行
        MassExection.Instance.Exection(TransformToNowPositionSet(), true, isStep);
    }

    /// <summary>
    /// 入力および移動
    /// </summary>
    void Update()
    {
        // キー入力
        // 壁も判定
        InputMoveRequest(plycnt.isMoveAllow && RunEventOnTrigger.isEventOnCollide == false);
    }
}