﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Map = MapWallChecker;

/// <summary>
/// 現在のマップの情報。
/// マップのロード。
/// </summary>
[System.Serializable]
public class MapTest : MonoBehaviour
{
    // 読込完了
    // 使ってない？
    //    public bool isEndRead = false;

    /*~----------------------------------/
    :                                    :
    :      3Dで実体をもつデータ          :
    :                                    :
    /~----------------------------------*/

    // 全オブジェクト
    private List<GameObject> MapObjects;

    // combine
    [SerializeField]
    private GameObject ob_walls;
    [SerializeField]
    private Combine combine;

    // combine以外
    [SerializeField]
    private GameObject ob_wallsOther;

    /// <summary>
    /// 壁オブジェクトのプレハブ
    /// </summary>
    [SerializeField]
    private GameObject[] prefab_Walls;

    /// <summary>
    /// 床オブジェクトのプレハブ
    /// </summary>
    [SerializeField]
    private GameObject[] prefab_Tiles;

    /// <summary>
    /// 空間オブジェクトのプレハブ
    /// </summary>
    [SerializeField]
    private GameObject[] prefab_Space;

    //マップデータ配列
    /*  test.byte
     *  
     *  indexByte(3Byte)
     *  mapData(100Byte)
     *  
     *  total 103Bytes
     */

    //イベントマップデータ配列
    /*  test.byte
     *  
     *  indexByte(2Byte)
     *  iventmapData(100Byte)
     *  
     *  total 103Bytes
     */


    /*~----------------------------------/
    :                                    :
    :        マップ内部のデータ          :
    :                                    :
    /~----------------------------------*/
    // このマップの階層
    [SerializeField]
    private string Map_Hierarchy;

    public string GetMap_Hierarchy()
    {
        return Map_Hierarchy;
    }

    // このマップのフロア
    [SerializeField]
    private int Map_Floor;

    public int GetMap_Floor()
    {
        return Map_Floor;
    }

    //Damage。
    [SerializeField]
    private Vector2Int Map_Size;

    public Vector2Int GetMap_Size()
    {
        return Map_Size;
    }

    //縦壁マップデータ格納配列
    private List<Byte> wallDatas1;

    //横壁マップデータ格納配列
    private List<Byte> wallDatas2;

    //床データ1格納配列
    private List<Byte> floorDatas1;

    //床データ2格納配列
    private List<Byte> floorDatas2;

    //踏破したかしていないか
    private List<List<Byte>> isStepfloors;
    private bool[] isFloorFirstLoads;

    /// <summary>
    /// 踏破セット、リセット
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="val">true = 踏破セット, false = 踏破リセット</param>
    public void SetIsStepfloors(int x, int y, int floor, bool val)
    {
        // フロア数を入力
        isStepfloors[floor - 1][x + GetMap_Size().x * y] = Convert.ToByte(val);
    }

    //イベントマップデータ格納配列
    private List<Byte> iv_buf;


    //位置イベント、ギミックイベント
    // 保留


    // イベントデータ取得
    public List<Byte> GetIventMapData()
    {
        return iv_buf;
    }

    // Use this for initialization
    void Awake()
    {
        print("起動");

        MapObjects = new List<GameObject>();

        wallDatas1 = new List<byte>();
        wallDatas2 = new List<byte>();
        floorDatas1 = new List<byte>();
        floorDatas2 = new List<byte>();
        // ステップの要素はここで作っておく。
        isStepfloors = new List<List<byte>>();
        for (int i = 0; i < 15; i++)
        {
            isStepfloors.Add(new List<byte>());
        }
        iv_buf = new List<byte>();

        isFloorFirstLoads = new bool[15];
        //読み込み
        MapLoad("F1");
        //        isEndRead = true;
    }

    void MapLoad(string filename)
    {
        // マップデータ、イベントマップ読込
        ReadFile(filename);

        //読み込み終了後、カメラオブジェクト群の有効化
        GameObject.Find("Player").transform.Find("CameraMoveObject").gameObject.SetActive(true);

        //セット
        print(Map_Floor);
        print("Damage:" + Map_Size);
        SetMap();
    }


    //読み込み関数
    void ReadFile(string filename)
    {
        int loadMapType = 0;

        //ファイル情報取得
        FileStream map = new FileStream(Application.streamingAssetsPath + "/MapData/Map/" + filename + ".txt", FileMode.Open, FileAccess.Read);
        FileStream iv_map = new FileStream(Application.streamingAssetsPath + "/MapData/Map/iv_" + filename + ".byte", FileMode.Open, FileAccess.Read);
        try
        {

            // フロアマップ
            using (StreamReader sr = new StreamReader(map, Encoding.UTF8))
            {
                print("フロアマップ");
                while (!sr.EndOfStream)
                {
                    //ファイルが空の場合
                    if (map.Length == 0)
                        //                        return false;
                        return;

                    // マップ情報
                    string str = sr.ReadLine();
                    print(str);
                    var isHeadData = FloorInfoSet(str);
                    if (isHeadData == true) continue;

                    // マップロード
                    loadMapType = 0;
                    str = ReadRepeatRowsMapData(str, sr, (int)Map_Size.y, (int)Map_Size.x + 1, wallDatas1);   // 縦マップ
                    loadMapType = 1;
                    str = ReadRepeatRowsMapData(str, sr, (int)Map_Size.y + 1, (int)Map_Size.x, wallDatas2);   // 横マップ
                    loadMapType = 2;
                    str = ReadRepeatRowsMapData(str, sr, (int)Map_Size.y, (int)Map_Size.x, floorDatas1);      // フロア床
                    loadMapType = 3;
                    str = ReadRepeatRowsMapData(str, sr, (int)Map_Size.y, (int)Map_Size.x, floorDatas2);      // フロア空間


                    //TODO: 踏破リスト初期化はまた正式に作る。これが暫定正式版
                    if (isFloorFirstLoads[Map_Floor - 1] == false)
                    {
                        List<byte> bytes = new List<byte>();
                        for (int i = 0; i < (int)(Map_Size.x * Map_Size.y); i++)
                            bytes.Add(0);
                        isStepfloors[Map_Floor - 1] = bytes;
                    }

                    isFloorFirstLoads[Map_Floor - 1] = true;
                    break;
                }
            }
        }
        catch
        {
            print("読み込めませんでした。:" + loadMapType.ToString());
        }


        try
        {
            // イベント(フォーマットはイベントマトリクスのみでフロア情報は含まない)
            using (BinaryReader iv_br = new BinaryReader(iv_map))
            {
                //ファイルが空の場合
                if (iv_map.Length == 0)
                    //                    return false;
                    return;

                //イベントバイナリ読み込み
                int i = 0;
                while (i < iv_map.Length)
                {
                    //List<Byte>に追加
                    iv_buf.Add(iv_br.ReadByte());
                    i++;
                }

                //読み込み終了後、カメラオブジェクト群の有効化
                GameObject.Find("Player").transform.Find("CameraMoveObject").gameObject.SetActive(true);
            }

        }
        catch
        {
            print("読み込めませんでした。イベント");
        }

        //        return true;
    }

    bool FloorInfoSet(string str)
    {
        string[] reserved = { "MapName", "FloorName", "Floor", "MapSizeX", "MapSizeY", "[MapData]" };

        for (int i = 0; i < reserved.Length; i++)
        {
            if (str.IndexOf(reserved[i]) != -1)
            {
                FloorInfoSetOfMap(i, str);
                return true;
            }
        }
        return false;
    }

    void FloorInfoSetOfMap(int i, string str)
    {
        int num = 0;
        switch (i)
        {
            //MapName = 名称不明の迷宮
            // 階層名 = MapName
            case 0:
                num = str.IndexOf('=');
                num += 2;
                string hircy = "";
                hircy = str.Substring(num, str.Length - 10);

                Map_Hierarchy = hircy;
                break;
            // 今のフロアセット
            case 2:
                num = str.IndexOf('=');
                num += 2;
                string flr = "";
                flr += str[num];
                if (num < str.Length - 1) flr += str[num + 1];

                Map_Floor = int.Parse(flr);
                break;
            // 横幅
            case 3:
                num = str.IndexOf('=');
                num += 2;
                string x = "";
                x += str[num];
                if (num < str.Length - 1) x += str[num + 1];

                Map_Size.x = int.Parse(x) + 1;
                break;
            // 縦幅
            case 4:
                num = str.IndexOf('=');
                num += 2;
                string y = "";
                y += str[num];
                if (num < str.Length - 1) y += str[num + 1];

                Map_Size.y = int.Parse(y) + 1;
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// 読み込まれた文字列からマップデータ読込
    /// </summary>
    /// <param name="str"></param>
    /// <param name="sr"></param>
    /// <param name="n"></param>
    /// <param name="m"></param>
    /// <param name="list"></param>
    /// <returns></returns>
    string ReadRepeatRowsMapData(string str, StreamReader sr, int x, int y, List<byte> list)
    {
        for (int j = 0; j < x; j++)
        {
            for (int i = 0; i < y * 2; i += 2)
            {
                string s1 = str[i].ToString() + str[i + 1].ToString();
                Byte value = (Byte)Convert.ToInt32(s1, 16);
                list.Add(value);
            }
            str = sr.ReadLine();
        }
        return str;
    }

    //walldatasのデータをもとに壁を生成
    void SetMap()
    {
        // マップオブジェクト初期化
        DeleteMapObject();
        // 壁置き
        PlaceMapWall();

        // 床
        ArrangementOfMapObjects((int)Map_Size.x, (int)Map_Size.y, floorDatas1, 2, false);
        // 空間
        ArrangementOfMapObjects((int)Map_Size.x, (int)Map_Size.y, floorDatas2, 3, false);
        // 天井
        ArrangementOfMapObjects((int)Map_Size.x, (int)Map_Size.y, floorDatas1, 20, false);

    }

    void PlaceMapWall()
    {
        // 縦壁
        ArrangementOfMapObjects((int)Map_Size.x + 1, (int)Map_Size.y, wallDatas1, 0, true);
        // 横壁
        ArrangementOfMapObjects((int)Map_Size.x, (int)Map_Size.y + 1, wallDatas2, 1, true);
    }


    /// <summary>
    /// マップ物の配置
    /// </summary>
    /// <param name="x">横幅</param>
    /// <param name="y">縦幅</param>
    /// <param name="list">データ</param>
    /// <param name="i">データの参照段落(0 ~ 3)</param>
    void ArrangementOfMapObjects(int x_size, int y_size, List<byte> list, int like, bool iswall)
    {
        for (int yy = 0; yy < y_size; yy++)
        {
            string str = "";
            for (int xx = 0; xx < x_size; xx++)
            {
                str += list[xx + yy * x_size].ToString("X");
                //1マスの壁データ
                Byte objectData = list[xx + yy * x_size];
                WallSet(xx, -yy, like, objectData, false);
                if (iswall && objectData <= 13)
                    WallSet(xx, -yy, like, objectData, iswall);
            }
            print(str);
        }

    }

    /// <summary>
    /// 各種プレハブオブジェクトの取得
    /// </summary>
    /// <returns></returns>
    GameObject GetSetObject(int type, int data)
    {
        switch (type)
        {
            case MapPrefabType.MAP_PREFAB_TYPE_NONE:
            case MapPrefabType.MAP_PREFAB_TYPE_WALL:
                return prefab_Walls[data];
                break;
            case MapPrefabType.MAP_PREFAB_TYPE_TILES:
                return prefab_Tiles[data];
                break;
            case MapPrefabType.MAP_PREFAB_TYPE_SPACE:
                return prefab_Space[data];
                break;
            default:
                print("type: " + type);
                return prefab_Walls[MapPrefabType.WALL_TOP];
                break;
        }
    }

    /// <summary>
    /// オブジェクトのセット。
    /// 壁は2枚重ねる。
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="like"></param>
	/// <param name="wallType"></param>
    void WallSet(int x, int y, int like, Byte wallType, bool isreverse)
    {
        var vec = Map.GetWallVector(x, y, wallType, like);
        var quat = Map.GetWallQuat(x, y, wallType, like, isreverse);
        GameObject setObject;
        setObject = GetSetObject(like, wallType);

        if (setObject == null) return;

        // 普通の壁はメッシュ結合
        if (like < 2 && wallType == 01)
        {
            var a = Instantiate(setObject, vec, quat, ob_walls.transform);
            MapObjects.Add(a);
            //			combine.MeshCombine(a);
        }
        else if (like >= 2 || (like < 2 && wallType != 00))
        {
            var b = Instantiate(setObject, vec, quat, ob_wallsOther.transform);
            //			MapObjects.Add(b);
        }
        return;
    }

    // マップオブジェクト全削除
    public void DeleteMapObject()
    {
        if (MapObjects.Count == 0) return;
        foreach (var obj in MapObjects)
        {
            Destroy(obj);
        }

        MapObjects.Clear();
    }


    /*********************************
     * 
     *          マップの判定
     * 
     * *******************************/
    /// <summary>
    /// 壁があるか？
    /// </summary>
    /// <param name="po"></param>
    /// <returns></returns>
    public int GetWallKind(Position po, int diffDirection)
    {
        var pos = WallRefSet(po);
        var dir = (diffDirection + pos.Direction) & 3;
        var data = CheckisWallInFrontFromData(pos.X, pos.Y, dir);
        return data;
    }

    Position WallRefSet(Position po)
    {
        var pos = new Position(po.X, po.Y, po.Direction, po.Floor, po.Hierarchy);
        switch (pos.Direction)
        {
            case 1:
                pos.X = pos.X + 1;
                break;
            case 2:
                pos.Y = pos.Y + 1;
                break;
            case 0:
            default://d = 3
                break;
        }
        return pos;
    }

    int CheckisWallInFrontFromData(int x, int y, int d)
    {
        if (d == 0 || d == 2)
        {
            return wallDatas2[x + y * (int)Map_Size.x];
        }
        else
        {
            return wallDatas1[x + y * ((int)Map_Size.x + 1)];
        }
    }

}
