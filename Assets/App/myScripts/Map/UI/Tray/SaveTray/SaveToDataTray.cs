﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

public class SaveToDataTray : UITray
{
    // ---------------- UITray -------------------

    public override void LeaveAnime(int befidx)
    {
    }

    public override void CursorAnime(int nowidx)
    {
    }

    public override　async void OnOpenTray()
    {
        var idx = _current._decideIdx;
        textWin.StartTextPlayNoSpan("セーブ中です...");
        await Save(idx);
        textWin.gameObject.SetActive(false);
        CloseTray();
    }

    // --------------- SaveToData -----------------

    [SerializeField] TextWin textWin;
    async Task<bool> Save(int idx)
    {
        await Task.Delay(100);
        SaveToJson.Instance.SaveToData(idx);
        await Task.Delay(500);
        return true;
    }
}
