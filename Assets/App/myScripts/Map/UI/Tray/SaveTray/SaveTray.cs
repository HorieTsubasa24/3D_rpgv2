﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveTray : UITray
{
    // ---------------- UITray -------------------

    public override void LeaveAnime(int befidx) {
        _objectFlame[befidx]._anim.Play("UnZoom");
    }

    public override void CursorAnime(int nowidx) {
        _objectFlame[nowidx]._anim.Play("Zoom");
    }

    public override void OnOpenTray()
    {
        FrameCenterController.Instance?.HideFrame();
        addSaveElements(); // セーブデータのリストを表示する。
        base.SetNumFlame();
    }

    /// <summary>
    /// 方向石フレームを表示
    /// </summary>
    public override void OnCloseTray()
    {
        FrameCenterController.Instance?.ShowFrame();
    }

    // --------------- SaveTray -----------------

    [SerializeField] SaveToDataTray _saveToDataTray;
    [SerializeField] GameObject _pref_Save;
    [SerializeField] GameObject _pref_Savenew;
    [SerializeField] Transform _contents;

    /// <summary>
    /// セーブエレメントの追加
    /// </summary>
    void addSaveElements()
    {
        destroyFlames();
        addSaveFlames();
        for (int i = 0; i < _objectFlame.Length; i++)
            LeaveAnime(i);
        CursorAnime(_nowFlameCursor);
    }

    void destroyFlames()
    {
        var flames = _contents.GetComponentsInChildren<SaveFlame>(true);
        foreach (var f in flames)
            Destroy(f.gameObject);

        _objectFlame = null;
    }

    const int MIN_DATA_NUM = 10;
    List<SaveFlame> saveflames = new List<SaveFlame>();
    /// <summary>
    /// セーブファイル選択の要素を追加。
    /// </summary>
    void addSaveFlames()
    {
        // セーブ要素の最大数 固定長のセーブ数にすることに落ち着いた
        var flameMaxIdx = SaveToJson.MIN_DATA_NUM - 1;
        saveflames.Clear();
        for (int i = 0; i <= flameMaxIdx; i++)
        {
            SaveFlame saveElem;
            SaveData saveData = SaveToJson.Instance.GetSaveData(i);
            if (saveData.IsNewData == false)
            {
                // データ
                saveElem = Instantiate(_pref_Save, _contents).GetComponent<SaveFlame>();
                saveElem.SetViewData(saveData);    // 情報を表示。
            }
            else
            {
                // 新しくセーブ
                saveElem = Instantiate(_pref_Savenew, _contents).GetComponent<SaveFlame>();
            }
            saveElem._gotoTray = _saveToDataTray;
            saveflames.Add(saveElem);
        }
        _objectFlame = saveflames.ToArray();
    }

    private void Update()
    {
        if (!_isRunning)
            return;

        var plycny = PlayerController.Instance;
        CursorMove(plycny.joy_Axis_buf);
        GotoNextTray(plycny.isFire1, () => addSaveElements());
        GotoBackTray(plycny.isFire2);
    }

}
