﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EventTray : UITray
{
    // ---------------- UITray ---------------

    public override void LeaveAnime(int befidx) {
        _objectFlame[befidx].PlayAnimeLeaveCursor();
    }

    public override void CursorAnime(int nowidx) {
        _objectFlame[nowidx].PlayAnimeOnCursor();
    }

    public override void OnOpenTray()
    {
        FrameCenterController.Instance.HideFrame();
        var texts = _trsSubQuest.GetComponentsInChildren<TextMeshProUGUI>();
        foreach (var t in texts)
        {
            Destroy(t.gameObject);
        }

        _mainTitle.text = CommonData.Instance.mainEvent.eventTitle;
        _mainExplanatory.text = CommonData.Instance.mainEvent.eventExplanatory;

        var subQuests = CommonData.Instance.subQuest;
        foreach (var s in subQuests)
        {
            var text = Instantiate(_pref_SubQuest, _trsSubQuest).GetComponent<TextMeshProUGUI>();
            text.text = StringBuild.GetStringFromArgs(s.isDone == false ? "未 : " : "終 : ", s.eventTitle);
        }
    }

    public override void OnCloseTray()
    {
        FrameCenterController.Instance.ShowFrame();
    }

    // ---------------------------------------------

    public TextMeshProUGUI _mainTitle;
    public TextMeshProUGUI _mainExplanatory;
    public GameObject _pref_SubQuest;
    public Transform _trsSubQuest;
    public List<TextMeshProUGUI> _subQuestTitles = new List<TextMeshProUGUI>();

    private void Update() {
        if (!_isRunning)
            return;

        var plycny = PlayerController.Instance;
        GotoBackTray(plycny.isFire2);
    }
}
