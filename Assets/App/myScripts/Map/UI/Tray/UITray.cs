﻿using System;
using UnityEngine;

/// <summary>
/// メニューのUI要素を表す。
/// SetActive{SetActiveFromAnimation(int toActive)}はアニメーションにやってもらう。
/// trueはSetActive(true)時、falseはPlayRemoveAnim()から
/// </summary>
public abstract class UITray : MonoBehaviour
{
    public static bool _isCanOpenTray = true;   // チャタリング防止
    public static UITray _current;
    protected UITray _parent;
    public bool _isRunning;
    public bool _isActiveLock;
    public bool _independence;
    /// <summary>
    /// フレーム要素を予めセット。
    /// </summary>
    [SerializeField]
    public ObjectFlame[] _objectFlame;
    public int _numFlame;
    public int _nowFlameCursor;
    protected Action _closeAction;
    protected Animator _anim;

    /// <summary>
    /// 前のトレイで選択されたインデックス番号
    /// </summary>
    public int _decideIdx = 0;

    // -------------- クローズアクションセット --------------

    private void AddCloseAct(Action act)
    {
        _closeAction = act;
    }

    // --------------- アニメーション関係 --------------------

    /// <summary>
    /// アニメーションから呼ばれる。0で非アクティブに。
    /// </summary>
    /// <param name="b"></param>
    public void SetActiveFromAnimation(int toActive)
    {
        gameObject.SetActive(toActive == 1);
        _isCanOpenTray = true;
    }

    /// <summary>
    /// ここのスクリプトやMapUIから呼ばれる。
    /// </summary>
    public virtual void PlayRemoveAnim()
    {
        if (_anim != null)
        {
            _anim.speed = 0;
            _anim.Play("Remove");
            _anim.speed = 1;
        }
        else
        {
            gameObject.SetActive(false);
            _isCanOpenTray = true;
        }
    }

    /// <summary>
    /// カーソルが離れたときの要素のアニメーション
    /// </summary>
    /// <param name="befidx"></param>
    public abstract void LeaveAnime(int befidx);

    /// <summary>
    /// カーソルが合ったときの要素のアニメーション
    /// </summary>
    /// <param name="befidx"></param>
    public abstract void CursorAnime(int nowidx);


    // ---------------- トレイのメソッド ---------------------

    /// <summary>
    /// 自分のトレーを閉じて親のトレーに切り替える。
    /// </summary>
    private void EndProcessing()
    {
        // 連続入力防止
        PlayerController.Instance.ResetInput(10);
        _isCanOpenTray = false;

        // 独立Tray
        if (_independence == true)
        {
            // 現トレイオフ
            _current._isRunning = false;
            // 一つ戻るためのデリゲードメソッド
            if (_closeAction != null)
            {
                OnCloseTray();
                _closeAction?.Invoke();
            }
            PlayRemoveAnim();
            return;
        }

        // 親が存在しない場合(メニューバーなど)
        if (_parent == null)
        {
            _isRunning = false;

            // MapUIのデリゲートメソッド
            // MenuClose();
            OnCloseTray();
            if (_closeAction != null)
            {
                _closeAction?.Invoke();
            }
            return;
        }

        // 現トレイオフ
        _current._isRunning = false;
        // アクティブロック以外
        // アクティブロックなものはMapUIで
        // 落としてあげよう。
        if (!_current._isActiveLock)
        {
            PlayRemoveAnim();
        }
        else
        {
            _isCanOpenTray = true;
        }
        // 親に切り替える
        _current = _parent;
        _current._isRunning = true;

        // 親からのデリゲートメソッド
        OnCloseTray();
        if (_closeAction != null)
        {
            _closeAction.Invoke();
        }
    }

    /// <summary>
    /// GotoTrayで終わらせる時。
    /// </summary>
    public void EndProcessManual()
    {
        PlayerController.Instance.ResetInput(10);

        _current._isRunning = false;

        // 親に切り替える
        _current = _parent;
        _current._isRunning = true;
    }

    /// <summary>
    /// 子トレイ、親トレイの設定。
    /// </summary>
    /// <param name="toParent"></param>
    /// <param name="toCurrent"></param>
    private void ChangeTray(UITray toParent, UITray toCurrent)
    {
        _parent = toParent;     // 親(this)
        _current = toCurrent;   // 子(tray)が規定だけどどんなふうにでも使える

        _current._isRunning = true;
        _current.gameObject.SetActive(true);
        if (_parent)
            _parent._isRunning = false;
    }

    /// <summary>
    /// 外部からUIを開くときはこれを実行する。
    /// </summary>
    /// <param name="toParent"></param>
    /// <param name="closeact"></param>
    /// <param name="isIndependence"></param>
    public void OpenNewTray(UITray toParent, Action closeact, bool isIndependence = false)
    {
        _independence = isIndependence;
        if (_independence == false)
        {
            ChangeTray(toParent, this);
        }
        OnOpenTray();
        PlayerController.Instance.ResetInput(10);
        AddCloseAct(closeact);
    }

    private void SetFlameLink(UITray gotoTray)
    {
        foreach (var of in _objectFlame)
        {
            of._gotoTray = gotoTray;
        }
    }

    /// <summary>
    /// SelectTrayを開くときに、開いたトレイのフレームオブジェクトに
    /// リンク先を動的に指定する。
    /// </summary>
    /// <param name="toParent"></param>
    /// <param name="toChild"></param>
    /// <param name="closeact"></param>
    public void OpenSelectTray(UITray toParent, UITray toChild, Action closeact)
    {
        ChangeTray(toParent, this);
        this.SetFlameLink(toChild);
        OnOpenTray();
        PlayerController.Instance.ResetInput(10);
        AddCloseAct(closeact);
    }

    /// <summary>
    /// 合計フレーム数セット。
    /// </summary>
    public void SetNumFlame()
    {
        _numFlame = _objectFlame.Length;
        _nowFlameCursor = 0;
    }

    /// <summary>
    /// Init初期化処理。
    /// </summary>
    public virtual void OnOpenTray()
    {

    }

    /// <summary>
    /// 閉じるときの処理。closeActのInvokeより前
    /// </summary>
    public virtual void OnCloseTray()
    {

    }

    public void CloseTray()
    {
        EndProcessing();
    }

    // ---------------- 入力関係 ----------------------

    /// <summary>
    /// カーソルを動かした時
    /// </summary>
    /// <param name="input"></param>
    public virtual void CursorMove(Vector2 input, int numOfline = 1)
    {
        if (input.x == 0 && input.y == 0)
            return;

        int befidx = _nowFlameCursor;

        bool ischange = false;
        if (input.y > 0 || input.x < 0)
        {
            ChangeIdxFromCursorMove(-numOfline);
            ischange = true;
        }
        if (input.y < 0 || input.x > 0)
        {
            ChangeIdxFromCursorMove(numOfline);
            ischange = true;
        }

        if (_nowFlameCursor >= _numFlame)
            _nowFlameCursor = _nowFlameCursor % numOfline;
        if (_nowFlameCursor < 0)
        {
            _nowFlameCursor = (_numFlame + _nowFlameCursor) % _numFlame;
        }

        if (ischange)
        {
            if (befidx != _nowFlameCursor)
                LeaveAnime(befidx);
            CursorAnime(_nowFlameCursor);
            Map_Sound.Instance.CursorMoveSoundPlay();
        }
    }

    protected virtual void ChangeIdxFromCursorMove(int numOfline)
    {
        _nowFlameCursor += numOfline;
    }

    /// <summary>
    /// 決定キーを押した時
    /// </summary>
    /// <param name="isgoto"></param>
    /// <param name="act"></param>
    public virtual void GotoNextTray(bool isgoto, System.Action act = null)
    {
        if (!isgoto || !_isCanOpenTray)
            return;

        Map_Sound.Instance.DecisionSoundPlay();
        // 独立Tray
        if (_independence)
        {
            this._decideIdx = _nowFlameCursor;
            CloseTray();
            return;
        }

        _objectFlame[_nowFlameCursor].SetDecideIdx(this._nowFlameCursor);
        _objectFlame[_nowFlameCursor].OpenTray(this, act);
    }

    /// <summary>
    /// キャンセルキーを押した時
    /// </summary>
    /// <param name="isgoto"></param>
    public virtual void GotoBackTray(bool isgoto)
    {
        if (!isgoto)
            return;

        Map_Sound.Instance.CancelSoundPlay();
        // 独立Tray
        if (_independence)
        {
            this._decideIdx = -1;
            CloseTray();
            return;
        }

        this.CloseTray();
    }

    // ---------------- Start -------------------

    protected virtual void Start()
    {
        _anim = GetComponent<Animator>();
        SetNumFlame();
    }
}
