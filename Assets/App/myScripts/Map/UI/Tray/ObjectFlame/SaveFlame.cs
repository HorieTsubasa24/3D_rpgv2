﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SaveFlame : GenericFlame
{ 
    [SerializeField] TextMeshProUGUI _dungeonName;
    [SerializeField] TextMeshProUGUI _floorNum;
    [SerializeField] TextMeshProUGUI _F;
    [SerializeField] TextMeshProUGUI _playTime;
    [SerializeField] TextMeshProUGUI _playTimeNum;
    [SerializeField] TextMeshProUGUI _saveTime;
    [SerializeField] TextMeshProUGUI _saveTimeNum;

    /// <summary>
    /// セーブ、ロードする時に表示する情報をセットする。
    /// </summary>
    /// <param name="idx">Index.</param>
    public void SetViewData(SaveData saveData)
    {
        //var saveinfo = saveData.saveInfo;
        //_dungeonName.text = saveinfo.item.ItemName;
        //_saveTimeNum.text = saveinfo.item.ExplanatoryText;
    }
}
