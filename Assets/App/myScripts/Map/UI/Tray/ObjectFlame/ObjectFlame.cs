﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Tray要素の中にある。
/// メソッドはTray要素から呼び出す。
/// </summary>
[System.Serializable]
public abstract class ObjectFlame : MonoBehaviour
{
    /// <summary>
    /// アイテム、スキルの場合ここにチェックを付ける。
    /// </summary>
    public bool _isPicture = false;

    /// <summary>
    /// 要素が選ばれたらtrue、カーソルから外れたらfalse
    /// </summary>
    public bool _isSelect = false;

    /// <summary>
    /// OpenTrayで開くトレイ。
    /// </summary>
    [SerializeField]
    public UITray _gotoTray;

    /// <summary>
    /// アイテムの場合、画像を登録して表示。
    /// </summary>
    public PictureFlame _pictureFlame;

    /// <summary>
    /// カーソル物でもなんでも
    /// </summary>
    public Image _image;

    /// <summary>
    /// アニメーション群。カーソル拡大など。
    /// </summary>
    [SerializeField]
    public Animator _anim;

    public virtual void PlayAnimeOnCursor() {
        if (_anim != null)
            _anim.Play("MenubarSlideRight");
    }

    public virtual void PlayAnimeLeaveCursor() {
        if (_anim != null)
            _anim.Play("MenubarSlideLeft");
    }

    /// <summary>
    /// 前のトレイで選ばれたインデックスを次のトレイに代入する。。
    /// </summary>
    public void SetDecideIdx(int decideIdx) {
        this._gotoTray._decideIdx = decideIdx;
    }

    /// <summary>
    /// 親トレイから次のトレイを開くときに使用する。
    /// </summary>
    public void OpenTray(UITray nowCurrent, System.Action closeact) {
        Map_Sound.Instance.DecisionSoundPlay();
        this._gotoTray.OpenNewTray(nowCurrent, closeact);
    }

    /// <summary>
    /// 親トレイから次のトレイを開くときに使用する。
    /// </summary>
    public void CloseTray() {
        Map_Sound.Instance.CancelSoundPlay();
        this._gotoTray.CloseTray();
    }

    /// <summary>
    /// 子クラスで分岐させましょう。
    /// _isPictureのセット、画像のロード表示。
    /// </summary>
    /// <param name="o"></param>
    public abstract void SetData(object o);

    /// <summary>
    /// 要素の選択のハイライト表示。
    /// </summary>
    /// <param name="o"></param>
    public abstract void HiLightOnIconSelect();

    /// <summary>
    /// 要素の選択のハイライト表示解除。
    /// </summary>
    /// <param name="o"></param>
    public abstract void DisLightOnIconSelect();


    /// <summary>
    /// Tray系クラスから呼ばれる。
    /// アイコンにカーソルがあたったときの処理。
    /// </summary>
    public void OnCursorIcon() {
        _isSelect = true;
        HiLightOnIconSelect();
    }

    /// <summary>
    /// Tray系クラスから呼ばれる。
    /// アイコンからカーソルが離れたときの処理。
    /// </summary>
    public void LeaveCursorIcon() {
        _isSelect = false;
        DisLightOnIconSelect();
    }
    
}

public struct PictureFlame
{
    public Transform _trsFlameBox;
    public Image _imgFlameRect;
    public Image _imgIconImage;
    public Image _imgEffect;
    public Image _imgNumber;
}