﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ItemFlame : ObjectFlame
{
    public TextMeshProUGUI _text_num;   // アイテムの個数
    public Image _img_effect;   // アイテムの効果画像
    public Image _img_icon;    // アイテムの画像
    public Image _img_flame;    // アイテムの フレーム画像
    public Item item;           // アイテムデータ

    public Sprite _alphaIcon;
    protected Color _effectColor = new Color(1, 1, 1, 0.8f);
    protected Color _effectColorNull = new Color(1, 1, 1, 0);
    /// <summary>
    /// アイテムデータのセット。
    /// </summary>
    /// <param name="o"></param>
    public override void SetData(object o)
    {
        item = (Item)o;
        _img_icon.sprite = (item.IconSprite != null) ? item.IconSprite : _alphaIcon;
        if (item.IconEffectSprite != null) {
            _img_effect.sprite = item.IconEffectSprite;
            _img_effect.color = _effectColor;
            _text_num.text = item.ItemNum.ToString();
        } else {
            _img_effect.sprite = _alphaIcon;
            _img_effect.color = _effectColorNull;
            _text_num.text = "";
        }
    }

    public override void HiLightOnIconSelect() {
    }

    public override void DisLightOnIconSelect() {
    }

}
