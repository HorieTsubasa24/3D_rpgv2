﻿using System;
using System.Collections;
using UnityEngine;
using TMPro;
using Recipe;

/// <summary>
///                      here               CharaSelectUIをスポーン
/// 錬成画面 -> [[ いくつ錬成しますか ]] -> 誰に渡しますか。
/// 次のselectHandOverにDecideIndexを渡すためにある。
/// </summary>
public class SetRenseiNumTray : SelectTray
{
    public GameObject _pref_CharaSelectUI;

    public int _num = 0;
    public int _maxNum = 10;
    public TextMeshProUGUI _text_Num;
    public TextWin _textWin;

    // --------------- TextWin ---------------------

    /// <summary>
    /// メッセージ再生
    /// </summary>
    /// <param name="str"></param>
    private void TextPlay(string str) {
        _textWin.StartTextPlayNoSpan(str);
    }

    // ---------------- UITray ---------------------

    public override void LeaveAnime(int befidx) {
        _objectFlame[befidx].PlayAnimeLeaveCursor();
    }

    public override void CursorAnime(int nowidx) {
        Map_Sound.Instance.CursorMoveSoundPlay();
        StartCoroutine(DelayAnim(1, nowidx));
    }

    private IEnumerator DelayAnim(int delay, int nowidx) {
        yield return delay;
        _objectFlame[nowidx].PlayAnimeOnCursor();
    }

    protected override void ChangeIdxFromCursorMove(int numOfline) {
        if (numOfline > 0) {
            LeaveAnime(1);
            CursorAnime(1);
        } else {
            LeaveAnime(0);
            CursorAnime(0);
        }
    }

    private void ChangeNum(int number) {
        _num = Mathf.Clamp(_num + number, 0, _maxNum);
    }

    /// <summary>
    /// 数値の変更。
    /// </summary>
    /// <param name="input"></param>
    /// <param name="numOfline"></param>
    public override void CursorMove(Vector2 input, int numOfline = 1) {
        if (input.y > 0 || input.x < 0) {
            this.ChangeIdxFromCursorMove(-numOfline);
            ChangeNum(1);
        }
        if (input.y < 0 || input.x > 0) {
            this.ChangeIdxFromCursorMove(numOfline);
            ChangeNum(-1);
        }

        _text_Num.text = _num.ToString();
    }

    private UIWindowElements _charaSelectUI;
    public override void OnOpenTray() {
        plycnt = PlayerController.Instance;
        _charaSelectUI = null;
        _num = 1;
        _text_Num.text = _num.ToString();
        _maxNum = _parent._decideIdx;   // 錬成可能最大数
    }

    public override void OnCloseTray() {
        if (_decideIdx != -1) {
            _decideIdx = _num;
        }
        _charaSelectUI = null;
    }

    public override void GotoBackTray(bool isgoto) {
        if (!isgoto)
            return;
        
        // 独立Tray
        if (_independence) {
            this._decideIdx = -1;
            CloseTray();
            return;
        }

        this.CloseTray();
    }

    public override void GotoNextTray(bool isgoto, Action act = null) {
        if (!isgoto)
            return;
        
        if (_num <= 0) {
            base.GotoBackTray(true);
            plycnt.ResetInput(10);
            return;
        }

        // キャラクタ選択トレイ
        Map_Sound.Instance.DecisionSoundPlay();
        var ob = Instantiate(_pref_CharaSelectUI, transform.parent.parent);
        ob.GetComponent<CharaSelectMes>().SetCharaSetectMode(CharaSelectMes.Mode.HandOver);
        _charaSelectUI = ob.GetComponent<UIWindowElements>();
    }

    public PlayRecipeTray _playRecipeTray;
    private void CharaSelectPhese() {
        if (_charaSelectUI.isChoicing != false)
            return;

        // キャンセル
        if (_charaSelectUI.DecisionIndex == -1) {
            base.GotoBackTray(true);
            Destroy(_charaSelectUI.gameObject);
            return;
        }

        // 選択キャラ取得
        var selectChara = (PlayerCharaData.Name)_charaSelectUI.DecisionIndex;
        RecipeValue.HandoverChara = selectChara;
        // 所持数
        var numHaveItem = PlayerDatas.Instance.GetHaveItemNum(RecipeValue.NowSelectItem, selectChara);
        if (numHaveItem == -1) {
            numHaveItem = PlayerDatas.Instance.GetHaveItemNum(Item.GetNullItem(), selectChara);
        }

        // Handover可能

        if (numHaveItem > -1 && numHaveItem < 100) {
            RecipeValue.SetNumRecipe(numHaveItem);
            // Recipes_MatMessageTray ( ReadyRensei選択のトレイ )まで戻って、
            // 最終的にPlayRecipeTrayからクローズする。
            this.GotoBackTray(true);
            Destroy(_charaSelectUI.gameObject);
            _playRecipeTray.OpenNewTray(UITray._current, null);
            return;
        } else {
            // 錬成不可
            TextPlay("持ち物がいっぱいです！");
        }

        base.GotoBackTray(true);
        Destroy(_charaSelectUI.gameObject);
    }

    // Update is called once per frame
    void Update() {
        if (_charaSelectUI != null) {
            CharaSelectPhese();
            return;
        }

        if (!_isRunning)
            return;

        this.CursorMove(plycnt.joy_Axis_buf);
        this.GotoNextTray(plycnt.isFire1);
        base.GotoBackTray(plycnt.isFire2);
    }
}