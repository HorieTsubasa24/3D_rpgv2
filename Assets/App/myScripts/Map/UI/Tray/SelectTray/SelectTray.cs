﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 選択するトレイの親。サブクラスでUpdateなどの動きをつける。
///TODO:動的にジャンプ先のトレイを設定できるようにする。
/// </summary>
public class SelectTray : UITray
{
    protected PlayerController plycnt;
    public override void LeaveAnime(int befidx) {
        _objectFlame[befidx].PlayAnimeLeaveCursor();
    }

    public override void CursorAnime(int nowidx) {
        _objectFlame[nowidx].PlayAnimeOnCursor();
    }

    public override void OnOpenTray() {
        plycnt = PlayerController.Instance;
    }
}
