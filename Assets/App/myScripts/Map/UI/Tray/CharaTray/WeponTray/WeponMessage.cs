﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// 武器装備時のメッセージ取得系
/// </summary>
public class WeponMessage
{
    private List<string[]> stringTables;
    private readonly string[] arestaAbirityUp =
    {
        "<color=#FF00FF>魔力</color>上がるよ！",             // STR
        "<color=#00FFFF>素早く</color>動けるようになるよ！",   // AGI
        "<color=green>踏ん張り</color>が効くようになるよ！",   // VIT
        "<color=orange>ラッキー</color>になるよ！",          // LUK
        "<color=red>急所</color>に当たるようになるよ！",       // CRI
    };

    private readonly string[] arestaAbirityDown =
    {
        "<color=#FF00FF><alpha=#aa>魔力</color><alpha=#aa>下がるよ",
        "<color=#00FFFF><alpha=#aa>動き</color><alpha=#aa>が遅くなるよ！",
        "<color=green><alpha=#aa>踏ん張り</color><alpha=#aa>が弱くなるよ！",
        "<color=orange><alpha=#aa>不幸</color><alpha=#aa>になっちゃうよ",
        "<color=red><alpha=#aa>急所に</color><alpha=#aa>当たらなくなるよ",
    };

    private readonly string[] erinaAbirityUp =
    {
        "<color=#00FFAF>魔力</color>アップですわ！",
        "<color=#00FFFF>素早さ</color>が上がりますわ！",
        "<color=green>防御力</color>が上がりますわ！",
        "<color=orange>運</color>が上がりますわ！",
        "<color=red>急所</color>に当てやすくなりますわ！",
    };

    private readonly string[] erinaAbirityDown =
    {
        "<color=#00FFAF><alpha=#aa>魔力</color><alpha=#aa>が弱まりますわ",
        "<color=#00FFFF><alpha=#aa>素早さ</color><alpha=#aa>が下がりますわ",
        "<color=green><alpha=#aa>防御力</color><alpha=#aa>ダウンですわ",
        "<color=orange><alpha=#aa>運</color><alpha=#aa>が悪くなりますわ",
        "<color=red><alpha=#aa>急所率</color><alpha=#aa>が下がりますわ",
    };

    private readonly string[] petilAbirityUp =
    {
        "<color=yellow>破壊力</color>が上がるよ！",
        "<color=#00FFFF>体が軽く</color>なるよ！",
        "<color=green>ダメージを受けにくく</color>なるよ！",
        "<color=orange>見えない力</color>に助けられるよ！",
        "<color=red>敵を仕留めやすく</color>なるよ！",
    };

    private readonly string[] petilAbirityDown =
    {
        "<color=yellow><alpha=#aa>破壊力</color><alpha=#aa>が弱まるよ",
        "<color=#00FFFF><alpha=#aa>体が重く</color><alpha=#aa>なるよ",
        "<color=green><alpha=#aa>ダメージ</color><alpha=#aa>を受けやすくなるよ",
        "<color=orange><alpha=#aa>見えないチカラ</color>が働かなくなるよ<alpha=#aa>",
        "<color=red><alpha=#aa>敵を仕留めにくく</color><alpha=#aa>なるよ",
    };

    /*
    /// <summary>
    /// スキル、アイテムの効果
    /// </summary>
    public enum Effect
    {
        HPRecover, MPRecover, HPMPRecover,
        Attack, MPDown,
        STRUp, STRDown,
        AGIUp, AGIDown,
        INIUp, INIDown,
        VITUp, VITDown,
        LUCKUp, LUCKDown,
        CRIUp, CRIDown,
        Event,
    }
     */

    /// <summary>
    /// 武器装備メッセージ初期化
    /// </summary>
    public WeponMessage()
    {
        stringTables = new List<string[]>();
        stringTables.Add(arestaAbirityUp);
        stringTables.Add(arestaAbirityDown);
        stringTables.Add(erinaAbirityUp);
        stringTables.Add(erinaAbirityDown);
        stringTables.Add(petilAbirityUp);
        stringTables.Add(petilAbirityDown);
    }

    /// <summary>
    /// 能力変化時のメッセージ取得
    /// </summary>
    /// <returns></returns>
    public string GetAbirityMessage(int charaNo, TableValues.Effect[] effects)
    {
        string result = "";
        foreach (var e in effects)
        {
            if (e < TableValues.Effect.STRUp || e > TableValues.Effect.CRIDown)
                continue;

            int tableIdx = charaNo * 2 + (int)e >= 30 ? 1 : 0;
            string[] table = stringTables[tableIdx];
        }
        return result;
    }
}
