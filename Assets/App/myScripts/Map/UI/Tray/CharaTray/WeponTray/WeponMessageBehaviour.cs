﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// 武器メッセージの表示を行う
/// </summary>
public class WeponMessageBehaviour : MonoBehaviour
{
    // 能力変化メッセージ取得テーブル
    private WeponMessage weponMessage;
    [SerializeField]
    // 能力変化メッセージ表示テキスト
    private TextMeshProUGUI weponText;
    // 武器装備メッセージ再生用通常window

    /// <summary>
    /// 説明表示をクリアする
    /// </summary>
    public void Clear()
    {
        weponText.text = string.Empty;
    }

    /// <summary>
    /// 武器能力変化メッセージ表示
    /// </summary>
    /// <param name="charaNo"></param>
    /// <param name="effects"></param>
    public void ShowAbirityChangeMessage(int charaNo, TableValues.Effect[] effects)
    {
        if (weponMessage == null)
            weponMessage = new WeponMessage();

        weponText.text = weponMessage.GetAbirityMessage(charaNo, effects);
    }

    /// <summary>
    /// 武器装備時メッセージ表示
    /// </summary>
    public void ShowOnEquitMessage(PlayerCharaData chara)
    {
        var mes = chara.Wepon.weponStatus.equitMes.Replace("*Usr", chara.PlayerName).Replace("/", "\n");
        Debug.Log(mes);
        TextAreaStatic.StartStatusMes(mes);
    }
}
