﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// キャラクター要素のトレイ。ちょっと特殊。
/// CharaTray ←→ StatusTray ←→ ItemTray
///                         or SkilTray
/// と分かれる。
/// </summary>
public class CharaTray : UITray
{
    public static int _selectCharaIdx;
    [SerializeField]
    private StatusTray _statusTray;

    public TextMeshProUGUI _text_names;
    private string[] names = { "アレスタ", "エリナ", "ペティル" };
    public TextMeshProUGUI _text_hps;
    public TextMeshProUGUI _text_mps;
    public GameObject[] _ob_charaImgs;

    /// <summary>
    /// 決定されたキャラクターのデータをセットする。
    /// </summary>
    /// <param name="idx"></param>
    private void LoadStatusChara(int idx) {
        _selectCharaIdx = idx;
        _text_names.text = names[idx];

        for (int i = 0; i < _ob_charaImgs.Length; i++) {
            _ob_charaImgs[i].SetActive(false);
        }  _ob_charaImgs[idx].SetActive(true);

        _text_hps.text = CommonData.GetHpStr(idx);
        _text_mps.text = CommonData.GetMpStr(idx);
    }

    public override void LeaveAnime(int befidx) {
        _objectFlame[befidx].PlayAnimeLeaveCursor();
    }

    public override void CursorAnime(int nowidx) {
        _objectFlame[nowidx].PlayAnimeOnCursor();
    }

    // このトレイは経由するだけ。
    private void OnEnable() {
        FrameCenterController.Instance.HideFrame();
        _nowFlameCursor = _decideIdx;           // トレイ開くときに同じように代入される。
        LoadStatusChara(_decideIdx);

        _statusTray.gameObject.SetActive(true);
        _statusTray._coneTest[_statusTray._nowFlameCursor].color = Color.white;
        _statusTray.OpenNewTray(this, () => {
            FrameCenterController.Instance.ShowFrame();
            this.CloseTray();
        });


    }

}
