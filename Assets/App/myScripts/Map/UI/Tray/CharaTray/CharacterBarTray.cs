﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterBarTray : UITray
{
    private int _nowIndex;
    private float _nowPhese;
    private Color _floatColor;
    private readonly Color _defaultColor = new Color(1, 1, 1, 0.36f);
    private readonly static int CHARAFLAME = 3;
    public override void LeaveAnime(int befidx) {
        _objectFlame[befidx]._image.color = _defaultColor;//;.PlayAnimeLeaveCursor();
    }

    // カーソルの色アニメーション
    public override void CursorAnime(int nowidx) {
        _nowIndex = nowidx;
        _objectFlame[nowidx]._image.color = _defaultColor;
    }
    private void FloatColorUpdate() {
        _nowPhese = (_nowPhese > Mathf.PI) ? 0 : _nowPhese + 0.0664f;
        var sinv = Mathf.Sin(_nowPhese) / 2.2f + 0.5f;
        _floatColor = new Color(1, 1, 1, sinv);
        _objectFlame[_nowIndex]._image.color = _floatColor;
    }

    bool[] isJoines = new bool[CHARAFLAME];
    /// <summary>
    /// 仲間になっていないキャラを飛ばす。
    /// </summary>
    /// <param name="input"></param>
    /// <param name="numOfline"></param>
    public override void CursorMove(Vector2 input, int numOfline = 1) {
        if (input.x == 0 && input.y == 0)
            return;

        int befidx = _nowFlameCursor;
        bool ischange = false;
        int sign = 0;
        if (input.y > 0 || input.x < 0) {
            ChangeIdxFromCursorMove(-numOfline);
            sign = -1;
            ischange = true;
        }
        if (input.y < 0 || input.x > 0) {
            ChangeIdxFromCursorMove(numOfline);
            sign = +1;
            ischange = true;
        }

        if (_nowFlameCursor >= _numFlame)
            _nowFlameCursor = _nowFlameCursor % numOfline;
        if (_nowFlameCursor < 0) {
            _nowFlameCursor = (_numFlame + _nowFlameCursor) % _numFlame;
        }

        AddNoChara(sign, numOfline);

        if (ischange) {
            if (befidx != _nowFlameCursor)
                LeaveAnime(befidx);
            CursorAnime(_nowFlameCursor);
            Map_Sound.Instance.CursorMoveSoundPlay();
        }
    }

    private void AddNoChara(int sign, int numOfline) {
        int timeout = 100;
        while(timeout > 0) {
            if (isJoines[_nowFlameCursor] == true)
                return;
            else
                _nowFlameCursor += sign;

            if (_nowFlameCursor >= _numFlame)
                _nowFlameCursor = _nowFlameCursor % numOfline;

            if (_nowFlameCursor < 0) 
                _nowFlameCursor = (_numFlame + _nowFlameCursor) % _numFlame;

            timeout--;
        }
    }

    public override void GotoNextTray(bool isGoto, System.Action act = null) {
        if (isGoto)
            _objectFlame[_nowIndex]._image.color = Color.white;
        base.GotoNextTray(isGoto, act);
    }

    public override void GotoBackTray(bool isGoto) {
        if (isGoto) {
            _objectFlame[_nowIndex]._image.color = _defaultColor;
            base.GotoBackTray(isGoto);
        }
    }

    public override void OnOpenTray() {
        isJoines[0] = PlayerDatas.Instance.p_chara[0].isJoined;
        isJoines[1] = PlayerDatas.Instance.p_chara[1].isJoined;
        isJoines[2] = PlayerDatas.Instance.p_chara[2].isJoined;
        int befidx = _nowFlameCursor;
        AddNoChara(1, 1);
        int idx = 0;
        foreach (var f in _objectFlame) { LeaveAnime(idx); }
        CursorAnime(_nowFlameCursor);
    }

    public GameObject[] _charaElm = new GameObject[6];
    private void DisableChara() {
        int numchara = CHARAFLAME;
        for (int i = 0; i < PlayerDatas.Instance.p_chara.Length; i++) {
            bool isjoin = PlayerDatas.Instance.p_chara[i].isJoined;
            _charaElm[i].SetActive(isjoin);
            _charaElm[i + 3].SetActive(isjoin);
            numchara = (isjoin == false) ? numchara - 1 : numchara;
        }
    }

    /// <summary>
    /// CharacterBarのアニメから呼ばれる。
    /// </summary>
    public void DisableCharaFromAnim() {
        DisableChara();
    }

    private void Update() {
        if (!_isRunning) {
            return;
        }

        FloatColorUpdate();

        var plycny = PlayerController.Instance;
        this.CursorMove(plycny.joy_Axis_buf);
        this.GotoNextTray(plycny.isFire1);
        this.GotoBackTray(plycny.isFire2);
    }

}
