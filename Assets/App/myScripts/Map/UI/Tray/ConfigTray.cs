﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 設定がめんのt
/// </summary>
public class ConfigTray : UITray {
    // --------------- override -------------------

    public override void LeaveAnime(int befidx) {
        _objectFlame[befidx].PlayAnimeLeaveCursor();
    }

    public override void CursorAnime(int nowidx) {
        _objectFlame[nowidx].PlayAnimeOnCursor();
    }

    PlayerController plycnt;
    public override void OnOpenTray()
    {
        FrameCenterController.Instance.HideFrame();
        InitConfigView();
        plycnt = PlayerController.Instance;
        int idx = 0;
        foreach (var o in _objectFlame)
        {
            LeaveAnime(idx);
            idx++;
        }
        CursorAnime(_nowFlameCursor);
    }

    // -------------- ConfigTray ------------------

    [SerializeField] GameObject _isFrontViewOff;
    [SerializeField] Slider _bgm;
    [SerializeField] Slider _se;

    /// <summary>
    /// 設定値の表示をはじめに更新する。
    /// </summary>
    private void InitConfigView()
    {
        // UI表示
        int isView = FrontUIView.Instance.GetFrontUIView();
        _isFrontViewOff.SetActive(isView < 1);

        // bgm
        _bgm.value = Map_Audio.Instance.GetVolumeCache();

        // se
        _se.value = Map_Sound.Instance.GetVolumeCache();
    }

    /// <summary>
    /// Configの設定値を変更。暫定的にswitchで選択。
    /// </summary>
    /// <param name="inputx">Inputx.</param>
    private void ChangeConfigValue(Vector2 inputx)
    {
        if (inputx.x == 0)
            return;

        Map_Sound.Instance.CursorMoveSoundPlay();
        SetConfigValue(_nowFlameCursor, inputx);
    }

    /// <summary>
    /// 設定値変更。
    /// </summary>
    /// <param name="cursorIdx">Cursor index.</param>
    private void SetConfigValue(int cursorIdx, Vector2 inputx)
    {
        switch (cursorIdx)
        {
            case 0: // UI表示
                bool isView = FrontUIView.Instance.ReverseFrontUIView();
                _isFrontViewOff.SetActive(!isView);
                return;
            case 1: // BGM
                _bgm.value += Mathf.Sign(inputx.x); // SetConfigValueOnValueChanged(int idx)
                return;
            case 2: // SE
                _se.value += Mathf.Sign(inputx.x); // SetConfigValueOnValueChanged(int idx)
                return;
        }
    }

    /// <summary>
    /// Sliderの値変更から呼び出す。
    /// </summary>
    /// <param name="idx">Index.</param>
    public void SetConfigValueOnValueChanged(int idx)
    {
        switch (idx)
        {
            case 1: // BGM
                Map_Audio.Instance.SetVolume((int)_bgm.value);
                return;
            case 2: // SE
                Map_Sound.Instance.SetVolume((int)_se.value);
                return;
        }
    }

    // ConfigFlameからプロパティ値の操作をするため。
    [SerializeField] ConfigFlame[] _configFlame;

    /// <summary>
    /// 方向石フレームを表示
    /// </summary>
    public override void OnCloseTray()
    {
        FrameCenterController.Instance.ShowFrame();
    }

    // Update is called once per frame
    void Update () {
        if (!_isRunning)
            return;

        var inputx = new Vector2(plycnt.joy_Axis_buf.x, 0);
        var inputy = new Vector2(0, plycnt.joy_Axis_buf.y);
        base.CursorMove(inputy);
        ChangeConfigValue(inputx);
        //base.GotoNextTray(plycnt.isFire1);
        base.GotoBackTray(plycnt.isFire2);
	}
}
