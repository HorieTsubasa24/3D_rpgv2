﻿using UnityEngine;
using TMPro;
using System;
using IconScroll;
using TMPro;

namespace Recipe
{
    public static class RecipeValue {
        public static Item NowSelectItem { get; set; }
        public static Item ToRecipeItem { get; set; }
        public static PlayerCharaData.Name HandoverChara { get; set; }

        public static void SetNumRecipe(int num) {
            ToRecipeItem = NowSelectItem;
            ToRecipeItem.SetItemNum(num);
        }
    }

    public class RecipesTray : UITray
    {
        private readonly string[] KIND_TEXTS = { "HP回復", "MP回復", "HPMP回復", "能力アップ", "武器" };

        private RecipeItemGetter recipeItemGetter;

        private int nowSelectedIcon = 0;
        private int numIcon;
        private Item[] recipeItems;
        private Item selectedItem;

        [SerializeField]
        private TextMeshProUGUI kindText;

        public RecipeMode _mode;
        public int[] _mode_selectedIcon = new int[5];    // 各錬金での洗濯しているインデックス
        public IconScrollView itemScrollView;

        public TextMeshProUGUI _text_itemName;
        public TextMeshProUGUI _text_itemExplanatory;
        public RecipeFlame[] _itemFlame_Mats;

        [SerializeField]
        private MaterialTray materialTray;

        public override void LeaveAnime(int befidx) {

        }

        public override void CursorAnime(int nowidx) { }

        /// <summary>
        /// アイテムの種類変更時にゲームオブジェクトの有効化切り替え。
        /// </summary>
        private void OnChangeMode() {
            if (_mode <= RecipeMode.NONE)
                _mode = RecipeMode.WEPON;
            else if (_mode >= RecipeMode.NUM)
                _mode = RecipeMode.HP;

            kindText.text = KIND_TEXTS[(int)_mode];
            recipeItems = recipeItemGetter.RecipeItems[(int)_mode];
            numIcon = recipeItems.Length;
            itemScrollView.Init(recipeItems);
            nowSelectedIcon = _mode_selectedIcon[(int)_mode];
            itemScrollView.SelectCell(nowSelectedIcon);
            SetSelectitem();
        }

        /// <summary>
        /// 選択アイテムを設定
        /// </summary>
        private void SetSelectitem()
        {
            if (recipeItems == null || recipeItems.Length == 0)
                return;

            selectedItem = recipeItems[nowSelectedIcon];
            itemScrollView.SelectCell(nowSelectedIcon);
            LoadItemInfomationOnChangeCursor(selectedItem);
        }

        /// <summary>
        /// カーソル、モード変更時にアイテム情報を読み込み、右トレイに表示。
        /// </summary>
        private void LoadItemInfomationOnChangeCursor(Item itm)
        {
            _text_itemName.text = itm.ItemName;
            _text_itemExplanatory.text = itm.ExplanatoryText;
            var matIDs = itm.MaterialItemIDs;
            for (int i = 0; i < _itemFlame_Mats.Length; i++) {
                if (i < matIDs.Length) {
                    var mat = ItemTable.Instance.GetItemForId(matIDs[i]);
                    var num = PlayerDatas.Instance.GetHaveItemNum(mat);
                    mat.ItemNum = num;
                    _itemFlame_Mats[i].SetData(mat);
                } else {
                    _itemFlame_Mats[i].SetData(Item.GetNullItem());
                }
                _itemFlame_Mats[i]._anim.Play("Initial");
            }
        }

        public override void CursorMove(Vector2 input, int numOfLine = 1)
        {
            if (input.x == 0 && input.y == 0)
                return;

            if (input.x > 0)
            {
                ++_mode;
                OnChangeMode();
                Map_Sound.Instance.CursorMoveSoundPlay();
                return;
            }
            if (input.x < 0)
            {
                --_mode;
                OnChangeMode();
                Map_Sound.Instance.CursorMoveSoundPlay();
                return;
            }

            int index = nowSelectedIcon;
            if (input.y > 0)
            {
                index -= numOfLine;
            }
            if (input.y < 0)
            {
                index += numOfLine;
            }
            if (index >= numIcon || index < 0)
                return;
            nowSelectedIcon = index;
            _mode_selectedIcon[(int)_mode] = index;

            SetSelectitem();
            Map_Sound.Instance.CursorMoveSoundPlay();
        }

        /// <summary>
        /// トレーにLayoutGroupをセット。
        /// </summary>
        public override void OnOpenTray()
        {
            FrameCenterController.Instance.HideFrame();
            if (recipeItemGetter == null)
                recipeItemGetter = new RecipeItemGetter();

            OnChangeMode();
            selectedItem = recipeItems[nowSelectedIcon];
            LoadItemInfomationOnChangeCursor(selectedItem);
        }

        /// <summary>
        /// 方向石フレームを表示
        /// </summary>
        public override void OnCloseTray()
        {
            FrameCenterController.Instance.ShowFrame();
        }

        public override void GotoNextTray(bool isgoto, Action act = null) {
            if (!isgoto)
                return;

            Map_Sound.Instance.DecisionSoundPlay();
            materialTray._decideIdx = nowSelectedIcon;
            materialTray.OpenNewTray(this, act);
        }

        private void Update() {
            if (!_isRunning)
                return;

            var plycny = PlayerController.Instance;
            this.CursorMove(plycny.joy_Axis_buf);
            this.GotoNextTray(plycny.isFire1, () => {
                LoadItemInfomationOnChangeCursor(selectedItem);
            });
            GotoBackTray(plycny.isFire2);
        }
    }
}