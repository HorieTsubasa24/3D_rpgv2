﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 層とマップで音を変えられるようにする機構。
/// </summary>
public class Map_Audio : Singleton<Map_Audio>, IAudioSaveLoad
{
    private AudioSource audioSource;
    [SerializeField]
    private AudioClip[] audios;
    public int currentAudioIdx = 0;

    private readonly int[] numHierarchyFloors = { 0, 5, 7, 10, 12 };

    // Use this for initialization
    void Awake () {
        if (Instance != null)
        {
            gameObject.SetActive(false);
            return;
        }

        instance = this;
        DontDestroyOnLoad(this);
        audioSource = GetComponent<AudioSource>();
        StartCoroutine(DelayPlay());
    }

    private IEnumerator DelayPlay() {
        if (MapTest_Legacy.Instance == null)
            yield break;

        yield return 1;
        AudioPlay(MapTest_Legacy.Instance.GetMap_Floor(), MapTest_Legacy.Instance.GetMap_HierarchyNum());
    }

    public void AudioPlay(int audio_select, int hierarchy)
    {
        audioSource.clip = audios[audio_select + numHierarchyFloors[hierarchy - 1]];
        audioSource.Play();
        currentAudioIdx = audio_select;
    }

    AudioClip replay;
    /// <summary>
    /// イベントからオーディオ再生。
    /// </summary>
    /// <param name="audio">Audio.</param>
    public void AudioPlayEvent(AudioClip audio)
    {
        replay = audioSource.clip;
        audioSource.clip = audio;
        audioSource.Play();
    }

    /// <summary>
    /// イベント終了時に再生を元に戻す。
    /// </summary>
    public void ReplayBeforeAudio()
    {
        if (replay != null)
        {
            audioSource.clip = replay;
            audioSource.Play();
        }
        replay = null;
    }

    // --------------- セーブロード -------------------

    /// <summary>
    /// コンフィグ変更時、ロード時、ボリュームを変更。
    /// </summary>
    /// <param name="volume">Volume.</param>
    public void SetVolume(int volume)
    {
        audioSource.volume = volume * 0.01f;
    }

    /// <summary>
    /// セーブ時に現在のボリューム値を取得。
    /// </summary>
    /// <returns>The volume cache.</returns>
    public int GetVolumeCache()
    {
        return (int)(audioSource.volume * 100);
    }
}
