﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// HUDの表示に使いたい
/// </summary>
public class Map_WallView : MonoBehaviour {
#if UNITY_EDITOR
    public bool _toCompletedMapGenerate = false;
    private void LateUpdate()
    {
        if (_toCompletedMapGenerate == true)
        {
            GenerateCompleteMap();
            _toCompletedMapGenerate = false;
        }
    }

    private void GenerateCompleteMap()
    {
        var size = MapTest_Legacy.Instance.GetMap_Size();
        var fl = MapTest_Legacy.Instance.GetMap_Floor();
        var hr = MapTest_Legacy.Instance.GetMap_HierarchyNum();
        for (int i = 0; i < size.y; i++)
            for (int j = 0; j < size.x; j++)
                SetStepFloor(new Position(j, i, 0, fl, hr));
    }
#endif

    public static Map_WallView map_wallview;
    /// <summary>
    /// マップの表示をするか
    /// イベント時のみ非表示にするときはgameObject.SetActive(false)を適用する。
    /// </summary>
    private static bool isMapView = true;

    private Animator anim;
    [SerializeField]
    private Image maskImage;
    
    /// <summary>
    /// オプションの設定値によってマップ表示の振る舞いを変える。
    /// また、更新のときにも呼び出す。
    /// </summary>
    public static void MapObjectViewForIsMapView()
    {
        if (isMapView)
            ViewMapObject();
        else
            HideMapObject();
    }

    [SerializeField]
    private TMPro.TextMeshProUGUI text_floorNum;
    private Image[] imgs_Floor;
    // 一時的な消去
    public static void HideMapObject()
    {
        if (map_wallview)
        {
            map_wallview.anim.Play("MapArea_Remove", 0);
        }
    }
    
    /// <summary>
    /// 踏破リストから壁、床の踏破マップを更新する。
    /// </summary>
    private void SetViewDataForIsStep()
    {
        int hirearchy = cd.Hierarchy;
        int floor = cd.Floor;
        int width = MapTest_Legacy.Instance.GetMap_Size().x;
        byte[] stepmap = MapTest_Legacy.Instance.GetStepfloorsForFloor(hirearchy, floor);
        for (int i = 0; i < stepmap.Length; i++)
        {
            if (stepmap[i] != 0)
            {
                int x = i % width;
                int y = i / width;
                Position po = new Position(x, y, -1, floor, hirearchy);
                ViewStepWallAndFloor(po);
                SetStepFloor(po);
            }
        }
        Map_WallView.map_wallview.SetStepFloor(CommonData.Instance.IntPos);
    }

    // 一時的な表示
    private static void ViewMapObject()
    {
        if (map_wallview.map != null)
        {
            map_wallview.text_floorNum.text = "F" + map_wallview.map.GetMap_Floor().ToString();
        }
        else if (map_wallview.l_map != null)
        {
            map_wallview.text_floorNum.text = "F" + map_wallview.l_map.GetMap_Floor().ToString();
        }
        DG.Tweening.DOVirtual.DelayedCall(0.1f, () => map_wallview.SetViewDataForIsStep());
        Minimap.MinimapManager.Instance.UpdateVisible();
        map_wallview.anim.Play("MapArea_Appear", 0);
    }

    bool IsLock;
    /// <summary>
    /// 場所移動時の一時的な機能停止。
    /// </summary>
    /// <param name="toLock">If set to <c>true</c> to lock.</param>
    public static void ToLock(bool toLock)
    {
        map_wallview.IsLock = toLock;
        map_wallview.SetStepFloor(CommonData.Instance.IntPos);
    }

    /// <summary>
    /// 移動中マップ表示のオプションを変更。
    /// </summary>
    /// <param name="bl"></param>
    public void SetIsMapView(bool bl)
    {
        isMapView = bl;
        MapObjectViewForIsMapView();
    }

    private CommonData cd;
    [SerializeField]
    private MapTest map;
    [SerializeField]
    private MapTest_Legacy l_map;
    [SerializeField]
    private Image[] img_map;
    [SerializeField]
    private Image[] img_ws;
    [SerializeField]
    private Image[] img_hs;
    [SerializeField]
    private Image img_Pos;
    [SerializeField]
    private Sprite[] sps_Pos;

    // Use this for initialization
    void Start ()
    {
        cd = CommonData.Instance;
        anim = GetComponent<Animator>();
        map_wallview = this;
    }

	// Update is called once per frame
	void Update () {
        if (IsLock == true || WarpObject.isWarping == true || cd.isEndRead == false)
            return;

        if (cd.IntPos.Equals(cd.BeforeFlamePos) == false)
            SetStepFloor(cd.IntPos);
    }

    /// <summary>
    /// 踏破状態を記録
    /// </summary>
    void SetStepFloor(Position pos)
    {
        if (IsLock == true)
            return;

        if (map != null)
        {
            int n = (int)pos.X + pos.Y * (int)map.GetMap_Size().x;

            img_Pos.sprite = sps_Pos[CommonData.Instance.Posd];
            ChangeColorToOne(img_map[n], 20, img_Pos);

            int x = pos.X; int y = pos.Y; int floor = pos.Floor;
            map.SetIsStepfloors(x, y, floor, true);
        }
        else if (l_map != null)
        {
            int n = (int)pos.X + pos.Y * (int)l_map.GetMap_Size().x;

            img_Pos.sprite = sps_Pos[CommonData.Instance.Posd];
            ChangeColorToOne(img_map[n], 20, img_Pos);

            int x = pos.X; int y = pos.Y; int hierarchy = pos.Hierarchy; int floor = pos.Floor;
            l_map.SetIsStepfloors(x, y, hierarchy, floor, true);
        }
        ViewStepWallAndFloor(pos);
    }

    /// <summary>
    /// 踏破した床、壁の表示有効化
    /// </summary>
    void ViewStepWallAndFloor(Position po)
    {
        byte[] walls = new byte[4];

        for (int dir = 0; dir < walls.Length; dir++)
        {
            var position = new Position(po.X, po.Y, dir, po.Floor, po.Hierarchy);
            walls[dir] = (byte)l_map.GetWallKind(position, 0);
        }
        if (map != null)
        {
            if (walls[0] > 0) ChangeColorToOne(img_ws[po.X + po.Y * (int)map.GetMap_Size().x], walls[0]);
            if (walls[1] > 0) ChangeColorToOne(img_hs[po.X + 1 + po.Y * (int)(map.GetMap_Size().x + 1)], walls[1]);
            if (walls[2] > 0) ChangeColorToOne(img_ws[po.X + (po.Y + 1) * (int)map.GetMap_Size().x], walls[2]);
            if (walls[3] > 0) ChangeColorToOne(img_hs[po.X + po.Y * (int)(map.GetMap_Size().x + 1)], walls[3]);
        }
        else if (l_map != null)
        {
            if (walls[0] > 0) ChangeColorToOne(img_ws[po.X + po.Y * (int)l_map.GetMap_Size().x], walls[0]);
            if (walls[1] > 0) ChangeColorToOne(img_hs[po.X + 1 + po.Y * (int)(l_map.GetMap_Size().x + 1)], walls[1]);
            if (walls[2] > 0) ChangeColorToOne(img_ws[po.X + (po.Y + 1) * (int)l_map.GetMap_Size().x], walls[2]);
            if (walls[3] > 0) ChangeColorToOne(img_hs[po.X + po.Y * (int)(l_map.GetMap_Size().x + 1)], walls[3]);
        }

    }

    private void ChangeColorToOne(Image img, byte dat, Image Pos = null)
    {
        if (Pos != null) Pos.transform.position = img.transform.position;

        var col = img.color;
        float alphamax = 1.0f;
        // 床
        if (dat >= 20)
            alphamax = 0.35f;
        else if (dat > 1)
            col = new Color(0, 0, 0, 0);

        // 扉
        //if (dat == 2) col = new Color(1.0f, 0, 1.0f, img.color.a);

        col.a = alphamax;
        img.DOColor(col, 1.0f);
        //Pos.DOColor(col, 1.0f);
        
    }
}
