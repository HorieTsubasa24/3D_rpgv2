﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CharaSelectMes : MonoBehaviour {
    public enum Mode { NotSet, Use, HandOver, ThrowAway }
    public Mode _mode = Mode.NotSet;

    public int DecideValue { get { return _selectTray._decideIdx; } }
    public UITray _selectTray;
    public SelectHandoverTray _selectHandoverTray;
    public SelectThrowAwayTray _selectThrowAwayTray;

    public TextMeshProUGUI _text_Mes;
    public void SetCharaSetectMode(Mode mode) {
        _mode = mode;
        switch (mode) {
            case Mode.Use:
                _text_Mes.text = "誰に使いますか？";
                return;
            case Mode.HandOver:
                _text_Mes.text = "誰に渡しますか？";
                return;
            default:
                return;
        }
    }

    public void OpenSelectHandover(PlayerCharaData.Name give, Item item, System.Action act) {
        _selectTray = _selectHandoverTray;
        _selectHandoverTray.OpenNewTray(null, act, true);// 独立トレイ
        _selectHandoverTray.InitHandoverStatus(give, item);
        _selectHandoverTray.gameObject.SetActive(true);
    }

    public void OpenSelectThrowAway(Item item, System.Action act) {
        _selectTray = _selectThrowAwayTray;
        _selectThrowAwayTray.OpenNewTray(null, act, true);// 独立トレイ
        _selectThrowAwayTray.InitThrowAwayStatus(item);
        _selectThrowAwayTray.gameObject.SetActive(true);
    }

    public void CloseTray() {
        _selectTray.gameObject.SetActive(false);
    }
}
