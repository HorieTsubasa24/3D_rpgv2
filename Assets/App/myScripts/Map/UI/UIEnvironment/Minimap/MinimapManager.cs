﻿using System.Collections.Generic;
using UnityEngine;
using Minimap;
using UnityEngine.UI;
using MapVariable;

namespace Minimap
{
    /// <summary>
    /// ミニマップの表示をコントロール
    /// </summary>
    public class MinimapManager : Singleton<MinimapManager>
    {
        private const int MASS_SIZE = 128;
        private const int RECT_SIZE = 500;
        private const float VIEW_SPAN = -10f;
        private List<MinimapMass> masses = new List<MinimapMass>();

        [SerializeField]
        private GameObject minimapMassPrefab;

        [SerializeField]
        private Transform spawnMass;

        [SerializeField]
        private Camera minimapCamera;
        private Transform minimapCameraTrs;

        [SerializeField]
        private Transform playerTrs;

        [SerializeField]
        private RawImage renderImage;
        private RenderTexture renderTexture;

        [SerializeField]
        private Transform arrowTrs;

        private Vector2Int mapSize;

        private bool isLock;

        /// <summary>
        /// 起動時のインスタンス代入
        /// </summary>
        private void OnEnable()
        {
            instance = this;
        }

        /// <summary>
        /// マップ表示物をロード
        /// </summary>
        public void MapLoad()
        {
            for (int i = 0; i < masses.Count; i++)
            {
                Destroy(masses[i].gameObject);
            }
            masses.Clear();

            var wallv = MapTest_Legacy.Instance.WallDatas1;
            var wallh = MapTest_Legacy.Instance.WallDatas2;
            var floors = MapTest_Legacy.Instance.FloorDatas1;

            mapSize = MapTest_Legacy.Instance.GetMap_Size();

            // クリープ　フリポンズ　ってバンドが女子高生ビッチに人気だってさ by macdo

            int nowFloor = MapTest_Legacy.Instance.GetMap_Floor();
            for (int y = 0; y < mapSize.y; y++)
            {
                for (int x = 0; x < mapSize.x; x++)
                {
                    GameObject obj = Instantiate(minimapMassPrefab, new Vector3(x * MASS_SIZE, -y * MASS_SIZE), Quaternion.identity, spawnMass);
                    MinimapMass mass = obj.GetComponent<MinimapMass>();
                    var up = wallh[x + mapSize.x * y];
                    var right = wallv[x + 1 + (mapSize.x + 1) * y];
                    var down = wallh[x + mapSize.x * (y + 1)];
                    var left = wallv[x + (mapSize.x + 1) * y];
                    var floor = floors[x + mapSize.x * y];

                    mass.SetMassData(up, right, down, left, floor);

                    int iconId = MapVariableManager.Instance.CurrentMapIconMap[nowFloor - 1][y * mapSize.x + x];
                    mass.SetEventIcon(iconId);
                    masses.Add(mass);
                }
            }

            var nowPos = CommonData.Instance.IntPos;

            renderTexture = new RenderTexture(RECT_SIZE, RECT_SIZE, 0);

            minimapCameraTrs = minimapCamera.transform;
            minimapCamera.targetTexture = renderTexture;
            minimapCamera.orthographicSize = RECT_SIZE;
            minimapCameraTrs.position = new Vector3(MASS_SIZE * nowPos.X, -MASS_SIZE * nowPos.Y, VIEW_SPAN);

            renderImage.texture = renderTexture;
            var rect = renderImage.GetComponent<RectTransform>();
            rect.sizeDelta = new Vector2(RECT_SIZE, RECT_SIZE);

        }
        
        /// <summary>
        /// ミニマップ矢印向きを変更する
        /// </summary>
        /// <param name="dir"></param>
        public void ChangeArrow(int dir)
        {
            arrowTrs.rotation = Quaternion.Euler(0, 0, dir * 90);
        }

        /// <summary>
        /// 踏破データから表示を変更する
        /// </summary>
        public void UpdateVisible()
        {
            StepFloor(CommonData.Instance.IntPos);
            var dat = MapTest_Legacy.Instance.GetStepfloorsForFloor(CommonData.Instance.Hierarchy, CommonData.Instance.Floor);
            for (int i = 0; i < masses.Count; i++)
            {
                if (dat[i] > 0)
                {
                    masses[i].ToActive(true);
                }
                else
                {
                    masses[i].ToActive(false);
                }
            }
        }

        /// <summary>
        /// マスを踏破処理する
        /// </summary>
        /// <param name="pos"></param>
        public void StepFloor(Position pos)
        {
            int index = (int)pos.X + pos.Y * (int)MapTest_Legacy.Instance.GetMap_Size().x;
            int x = pos.X; int y = pos.Y; int hierarchy = pos.Hierarchy; int floor = pos.Floor;
            MapTest_Legacy.Instance.SetIsStepfloors(x, y, hierarchy, floor, true);
            masses[index].ToActive(true, true);
        }

        /// <summary>
        /// オプションの設定値によってマップ表示の振る舞いを変える。
        /// また、更新のときにも呼び出す。
        /// </summary>
        public void MapObjectViewForIsMapView()
        {
            //if (isMapView)
            //    ViewMapObject();
            //else
            //    HideMapObject();
        }


        //public static void HideMapObject()
        //{
        //    if (map_wallview)
        //    {
        //        map_wallview.anim.Play("MapArea_Remove", 0);
        //    }
        //}

        /// <summary>
        /// 移動マップ表示のオプションを変更。
        /// </summary>
        /// <param name="bl"></param>
        //public void SetIsMapView(bool bl)
        //{
        //    isMapView = bl;
        //    MapObjectViewForIsMapView();
        //}

            
        private void Update()
        {
            Vector3 vec = playerTrs.position;
            // プレイヤー位置からみてミニマップ位置同期
            var newPos = new Vector3(MASS_SIZE * vec.x, MASS_SIZE * vec.z);// + basePos;;
            minimapCameraTrs.position = newPos;
            arrowTrs.rotation = Quaternion.Euler(0, 0, -playerTrs.rotation.eulerAngles.y);
        }
    }
}