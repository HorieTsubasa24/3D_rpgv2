﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using Recipe;

/// <summary>
/// マップメニューの開始ポイント
/// </summary>
public class MapUI : Singleton<MapUI>
{
    private bool isBattle;
    public bool _isOpen = false;
    private PlayerController plycnt;

    private const float ANIMATION_IDLE_WAIT = 4.0f;
    private float waitIdle;
    private bool isAnimated;

    // -------------- トレー類 -------------------

    public MenuBarTray _menuBarTray;
    public CharacterBarTray _characterBarTray;
    public CharaTray _charaTray;
    public EventTray _eventTray;
    public RecipesTray recipesTray;
    public SaveTray _saveTray;
    public SelectTray _selectTray;

    // --------------- その他表示関係 ------------

    // *** 背景イメージ
    public Image img_backGround;
    [SerializeField]

    // *** テキスト
    private GameObject ob_TextWin;
    [SerializeField]
    private TextMeshProUGUI text_TextWin;
    [SerializeField]
    private TextWin textWin;
    
    // ----------- メニューを開く ------------------

    /// <summary>
    /// メニューを開ける状態なら開く
    /// </summary>
    private void CheckMenuOpen() {
        if (CameraMove.Instance.Move_Arrow == CameraMove.Arrow.NONE && plycnt.isMenuAllow == true && plycnt.isFire2 && UITray._current == null) {
            Debug.Log("CheckMenuOpen");
            plycnt.SetInputDelay(20);
            OpenMenu();
        }    
    }
    
    private void OpenMenu() {
        OnOpenMenu();

        _characterBarTray.gameObject.SetActive(true);   // アニメーションも走る
        _menuBarTray.OpenNewTray(null, () => {
            MenuClose();
        });
        _menuBarTray.InitCursor();
    }

    private void OnOpenMenu()
    {
        ShowFrames();
        isAnimated = true;

        _isOpen = true;
        img_backGround.DOFade(0.65f, 0.8f);
        Map_Sound.Instance.DecisionSoundPlay();
        // 動作禁止
        PlayerController.Menu_InputConfigSet();
    }

    // ----------- メニューを閉じる -----------------

    private void MenuClose() {
        _menuBarTray.PlayRemoveAnim();
        _characterBarTray.PlayRemoveAnim();   // 表示だけ
        UITray._current = null;

        OnMenuClose();
    }

    private void OnMenuClose()
    {
        HideFrames();
        isAnimated = false;
        waitIdle = Time.time + ANIMATION_IDLE_WAIT;

        img_backGround.DOFade(0, 0.8f);
        // 歩き許可
        PlayerController.Map_InputConfigSet();
        Map_Sound.Instance.CancelSoundPlay();
        MapComponentsContainar.Instance.DeleteAll();
        plycnt.SetInputDelay(20);
        _isOpen = false;
    }

    // --------------- フレームの表示 ----------------------

    /// <summary>
    /// マップフレームの表示
    /// </summary>
    private void ShowFrames()
    {
        FrameLeftController.Instance.SetMoney(CommonData.Instance.Gold);
        FrameLeftController.Instance.ShowFrame();
        FrameCenterController.Instance.ShowFrame();
        int hierarchy = CommonData.Instance.Hierarchy;
        FrameRightController.Instance.SetDungeonFloor(CommonData.Instance.Floor, CommonData.Instance.GetFloorName(hierarchy));
        FrameRightController.Instance.SetDungeonName(CommonData.Instance.GetDungeonName(hierarchy));
        FrameRightController.Instance.ShowFrame();
    }

    /// <summary>
    /// マップフレームの非表示
    /// </summary>
    private void HideFrames()
    {
        FrameLeftController.Instance.HideFrame();
        FrameCenterController.Instance.HideFrame();
        FrameRightController.Instance.HideFrame();
    }

    /// <summary>
    /// バトル時、バトル終了時の処理
    /// </summary>
    /// <param name="battle"></param>
    public void Battle(bool battle)
    {
        isBattle = battle;
        if (isBattle)
        {
            HideFrames();
        }
        else
        {

        }
    }

    // ----------------- Start() Update() ----------------

    private void Start() {
        waitIdle = Time.time + ANIMATION_IDLE_WAIT;
        instance = this;
        plycnt = PlayerController.Instance;
        ShowFrames();
        isAnimated = true;
    }

    private void Update() {
        if (_isOpen || isBattle)
            return;

        if (!isAnimated && Time.time > waitIdle)
        {
            ShowFrames();
            FrontUIView.Instance.DisplayAnime(true);
            isAnimated = true;
        }
        CheckMenuOpen();    // メニューオープン

        if (!_isOpen && PlayerController.Instance.IsButtonAndAxisIsNoZero)
        {
            HideFrames();
            isAnimated = false;
            waitIdle = Time.time + ANIMATION_IDLE_WAIT;
        }
    }
}