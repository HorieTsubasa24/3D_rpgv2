﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShopViewItemPropaty : MonoBehaviour {
    public TextMeshProUGUI playermoney;
    PlayerCharaData player;
    int befMoney;

    public TextMeshProUGUI money;

    public UIWindowElements shopWindow;
    ItemElement befSelectItemElement;
    ItemElement nowSelectItemElement;

    private void Start()
    {
        nowSelectItemElement = shopWindow.nowItemElement;
        befSelectItemElement = null;

        player = PlayerDatas.Instance.p_chara[0];
        befMoney = 0;
        playermoney.text = StringBuild.GetStringFromArgs(player.G.ToString(), "G");
    }

    // Update is called once per frame
    void Update () {
		if (nowSelectItemElement != befSelectItemElement)
        {
            befSelectItemElement = nowSelectItemElement;
            money.text = StringBuild.GetStringFromArgs(nowSelectItemElement.item.Gold.ToString(), "G");
        }

        if (player.G != befMoney)
        {
            befMoney = player.G;
            playermoney.text = StringBuild.GetStringFromArgs(player.G.ToString(), "G");
        }
    }
}
