﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Threading.Tasks;

public class SimpleCircleLayoutGroup : UIBehaviour, ILayoutGroup 
{
    public bool _isRotating = false;
    public float radius = 100;
    // アングルはUIをオープンしたときに0 ~ 360の間に修正する
    public int Index;                // 設定したインデックス。これを見て
    public int _nowCurrentIdx;       // 現在のインデックス
    private float _partOfAngle;      // インデックス1つごとの角度
    private float _PreviousAngle;    // 前の角度
    public float _nowAngle;          // 現在の角度
    public float _offsetAngle;       // 計算ごとにかかる角度
    public float _baseOffsetAngle;   // いつもかかる回転角
    public float _xScale = 1.0f;　   // 縦横半径の伸び縮み
    public float _yScale = 1.0f;　   // 縦横半径の伸び縮み
    public float _sinScaler = 1.0f;  // 変更すると、角度が制限、拡大される。
    public float _cosScaler = 1.0f;  // 変更すると、角度が制限、拡大される。
    public Vector2 _offSet = new Vector2(0.0f, 0.0f); // x, y座標のオフセット


    public bool d = false;
    public int didx = 0;

    protected override void OnEnable() {
        Arrange();
    }

    protected override void Start()
    {
        base.Start();
        _partOfAngle = -360.0f / (transform.childCount - 1);
    }

    private void Update()
    {
        // 角度差があれば回転
        if (_PreviousAngle != _nowAngle)
        {
            _PreviousAngle = _nowAngle;
            Arrange();
        }

        // デバッグ
        if (d == true)
        {
            d = false;
            RotateElements(didx);
        }
    }

    public void IncIdx() {
        RotateElements(_nowCurrentIdx + 1);
    }

    public void DecIdx() {
        RotateElements(_nowCurrentIdx - 1);
    }

    public void SetIdx(int idx) {
        Index = idx;
        RotateElements(idx);
    }

    /// <summary>
    /// アングルの変更
    /// </summary>
    /// <param name="angle"></param>
    public void SetOffsetAngle(float angle)
    {
        _nowAngle = angle;
    }

    private Task _task;
    public void RotateElements(int idx)
    {
        if (idx == _nowCurrentIdx || _isRotating == true)
            return;

        _isRotating = true;
        if (Mathf.Abs(_nowCurrentIdx - idx) != 1) {
            SetOffsetAngle(_nowCurrentIdx * _partOfAngle);
        }

        //var smoothDelta60 = Time.deltaTime * 60;
        var childcount = transform.childCount  - 1;
        if (_task == null)
        {
            var cnt = 20 / Time.smoothDeltaTime / 60;
            var partAddDeg = _partOfAngle * (idx - _nowCurrentIdx) / cnt;
            _task = Task.Run(async () =>
            {
                // ちょっとずつ回転
                for (int i = 1; i <= cnt; i++)
                {
                    await Task.Delay(7);
                    SetOffsetAngle(_nowAngle + 2 * partAddDeg * Mathf.Sqrt(i / cnt));
                }

                // 角度固定
                _nowCurrentIdx = idx;
                SetOffsetAngle(_nowCurrentIdx * _partOfAngle);

                // インデックス調整
                if (idx == childcount) {
                    _nowCurrentIdx = 0;
                }
                if (idx == -1) {
                    _nowCurrentIdx = childcount - 1;
                }
                _task = null;
                _isRotating = false;

                return;
            });
        }
    }

#if UNITY_EDITOR
    protected override void OnValidate ()
	{
		base.OnValidate ();
		Arrange ();
	}
#endif

    // 要素数が変わると自動的に呼ばれるコールバック
    #region ILayoutController implementation
    public void SetLayoutHorizontal (){}
	public void SetLayoutVertical ()
	{
		Arrange ();
	}
	#endregion

	void Arrange()
	{
        float splitAngle = 360.0f / (transform.childCount - 1);
		var rect = transform as RectTransform;

		for (int elementId = 0; elementId < transform.childCount; elementId++) {
			var child = transform.GetChild (elementId) as RectTransform;
			float currentAngle = (splitAngle * elementId + _nowAngle + _offsetAngle) % 360;
            if (currentAngle < 0)
                currentAngle = 360 + currentAngle;
            
            // radiusが回転行列の(x, y)
            var pos = new Vector2(
                Mathf.Cos((_sinScaler * currentAngle + _baseOffsetAngle) * Mathf.Deg2Rad) * _xScale,
                Mathf.Sin((_cosScaler * currentAngle + _baseOffsetAngle) * Mathf.Deg2Rad) * _yScale) * radius;
            //if (currentAngle > 71 && currentAngle < 248)
            //    pos.x -= (currentAngle - 71) * 10;
            
            child.anchoredPosition = pos + _offSet;

        }
	}
}