﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceStatus : MonoBehaviour {
    [SerializeField]
    protected GameObject[] ob_Face;
    [SerializeField]
    private int PlayerID = 0;        // 0 ~ 2
    private PlayerCharaData pd;

    // Use this for initialization
    virtual public void Start () {
        pd = PlayerDatas.Instance.p_chara[PlayerID];
        CharacterFaceSet(pd.M_HP, pd.HP);
    }
	
	// Update is called once per frame
	virtual public void Update () {
        CharacterFaceSet(pd.M_HP, pd.HP);
    }


    protected int CharacterFaceSet(float m_hp, float hp)
    {
        float hppermax = hp / m_hp;

        // マップ画面時
        if (ob_Face.Length > 0)
        {
            foreach (var a in ob_Face)
                a.SetActive(false);

            if (hp <= 0)
                ob_Face[4].SetActive(true);
            else if (hppermax <= 0.25f)
                ob_Face[3].SetActive(true);
            else if (hppermax <= 0.55f)
                ob_Face[2].SetActive(true);
            else if (hppermax <= 0.8f)
                ob_Face[1].SetActive(true);
            else
                ob_Face[0].SetActive(true);
        }

        // バトル時、マップ時共通
        if (hp <= 0)
            return 0;
        else if (hppermax <= 0.25f)
            return 1;
        else if (hppermax <= 0.55f)
            return 2;
        else if (hppermax <= 0.8f)
            return 3;
        else
            return 4;

    }
}
