﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Battle;

[System.Serializable]
public class MagicElement : MonoBehaviour
{
    public bool isBattle;
    private PlayerCharaData pd;
    private Battle_Status bs;

    [SerializeField]
    private int charaIdx;

    public void SetCharaIdx(int n) { charaIdx = n; }

    private Image img_Magic;
    private ParticleSystem part_Effect;
    private float emissionLevel = 1.0f; // 0 ~ 1.0f;揺れ幅
    private Color amplitube = new Color(0.8f, 0.8f, 0.8f, 1);
    private readonly Color baseColor = new Color(0.2f, 0.2f, 0.2f, 1);
    private Color nowcolor = new Color(0.2f, 0.2f, 0.2f, 1);

	// Use this for initialization
	void Start () {
        if (isBattle)
        {
            //TODO:ずさん
            var bss = FindObjectsOfType<Battle_Status>();
            if (charaIdx == 0)
                bs = bss.First(x => x.CharaName == "アレスタ");
            else if (charaIdx == 1)
                bs = bss.First(x => x.CharaName == "エリナ");
            else if (charaIdx == 2)
                bs = bss.First(x => x.CharaName == "ペティル");
            else
                throw new System.Exception("MPエレメント用Battle_Status読み込み失敗");
        }
        else
            pd = PlayerDatas.Instance.p_chara[charaIdx];

        img_Magic = GetComponent<Image>();
        part_Effect = GetComponent<ParticleSystem>();
    }
	
	// Update is called once per frame
	void Update () {
        emissionLevel = GetEmissionLevel();
        nowcolor = baseColor + amplitube * emissionLevel;// 色 : 基本色 + 振幅の半分 * -1.0 ~ 1.0f * 0 ~ 1.0f
        nowcolor.a = 1;

        ResetParticleNum();
        img_Magic.color = nowcolor;
    }

    private bool isChange = false;
    private float GetEmissionLevel()
    {
        float m_mp;
        float mp;
        float mppermax;
        if (isBattle)
        {
            m_mp = bs.M_Mp;
            mp = bs.Mp;
        }
        else
        {
            m_mp = pd.M_MP;
            mp = pd.MP;
        }
        mppermax = mp / m_mp;

        if (mppermax != emissionLevel)
            isChange = true;

        return mppermax;
    }

    private void ResetParticleNum()
    {
        if (!isChange)
            return;

        ParticleSystem.EmissionModule EmObj = part_Effect.emission;
        EmObj.rateOverTime = new ParticleSystem.MinMaxCurve(emissionLevel * 10);
    }
}
