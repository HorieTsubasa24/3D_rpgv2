﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;
//using DG.Tweening;
//using TMPro;

//public class Map_UI : MonoBehaviour
//{
//    private static Map_UI _mapUI;
//    /*~***********************************
//    *                                    *
//    *            外部クラス	             *
//    *                                    *
//    /~***********************************/
//    private CommonData cd;
//    private PlayerController plycnt;
//    [SerializeField]
//    private Map_Sound sound;
//    [SerializeField]
//    private GameObject ob_TextWin;
//    [SerializeField]
//    private TextMeshProUGUI text_TextWin;
//    [SerializeField]
//    private MapToBattle iv_sys;
//    [SerializeField]
//    private SkillBook sklbook;
//    [SerializeField]
//    private ItemBook itembook;
//    /*~***********************************
//    *                                    *
//    *    このオブジェクトのクラス        *
//    *                                    *
//    /~***********************************/
//    private Animator anim_Map_UI;
//    [SerializeField]
//    private TextWin textWin;
//    /*~***********************************
//    *                                    *
//    *              カーソル物            *
//    *                                    *
//    /~***********************************/
//    [SerializeField]
//    private Image img_backGround;
//    // アレスタ - magic, item, skill, エリナ - magic, skill, ペテル - magic, skill
//    // マップ
//    private readonly string[] animStateName = { "Aresta_magic", "Aresta_item", "Aresta_skill", "Erina_magic", "Erina_item", "Erina_skill", "Petil_magic", "Petil_item", "Petil_skill", "saveload"};
//    // ast astexp ern ernexp ptl ptlexp
//    [SerializeField]
//    private GameObject[] obs_Chara = new GameObject[6];
//    // magic item skill magic item skill magic item skill saveload
//    [SerializeField]
//    private GameObject[] ob_btns = new GameObject[10];
//    private Button[] btns = new Button[10];
//    /*~***********************************
//    *                                    *
//    *                変数類              *
//    *                                    *
//    /~***********************************/
//    readonly int WaitCountFlame = 280;
//    public int WaitCount;

//    bool isOpenTheMenu = false;
    
//    public static void StartEvent()
//    {
//        _mapUI.CharacterHide();
//    }


//    // Use this for initialization
//    void Start () {
//        _mapUI = this;
//        cd = CommonData.Instance;
//        plycnt = PlayerController.Instance;
//        anim_Map_UI = GetComponent<Animator>();
//        WaitCount = WaitCountFlame;
//        ClickEventSet();
//        // 自動的に実行される
//        // anim_Map_UI.Play("Map_UI_OutOfCanvas", 0);
//    }

//    private void DecWait()
//    {
//        if (menustck == 0 && CursorWaitCount > 0)
//            CursorWaitCount--;

//        if (menustck == 0 && ClickWaitCount > 0)
//            ClickWaitCount--;
//    }

//    private void EndUIAct()
//    {
//        if (endUI.isChoicing == false)
//        {
//            if (endUI.DecisionIndex == 0)
//            {
//                #if UNITY_EDITOR
//                    UnityEditor.EditorApplication.isPlaying = false;
//                #elif UNITY_STANDALONE
//                    Application.Quit();
//                #endif
//                return;
//            }
//            else
//            {
//                isGameEndMode = false;
//                Destroy(endUI.gameObject);
//                endUI = null;
//                TextAreaClose();
//                if (!isOpenTheMenu)
//                {
//                    PlayerController.Map_InputConfigSet();
//                    MapComponentsContainar.DeleteAll();
//                }
//            }
//        }
//    }

//    // Update is called once per frame
//    void Update () {
//        if (isGameEndMode)
//        {
//            EndUIAct();
//            return;
//        }

//        // メニューを開いていない
//        if (isOpenTheMenu == false)
//        {
//            CharaViewJudgeForMenuOpen();
//            if (plycnt.joy_Axis_buf.x != 0 || plycnt.joy_Axis_buf.y != 0)
//            {
//                WaitCount = WaitCountFlame;
//                CharacterHide();
//                TextAreaClose();
//            }

//            // メニュー禁止
//            if (plycnt.isMenuAllow == false)
//                return;

//            if (WaitCount >= 0)
//                WaitCount--;
//            if (CursorWaitCount > 0) CursorWaitCount--;
//            KeyJudgeForMenuOpen();
//        }
//        // OpenTheMenu == true
//        else
//        {
//            // スキル、アイテムを開いている。
//            if (menustck == 0)
//                MenuRoutine();
//            DecWait();
//        }
//    }
    
//    public int MenuCursorLocation = 0;
//    readonly int CursorWait = 7;
//    int CursorWaitCount = 20;
//    [SerializeField]
//    private int menustck = 0;
//    /// <summary>
//    /// メニュー
//    /// </summary>
//    void MenuRoutine()
//    {
//        if (anim_Map_UI.GetCurrentAnimatorStateInfo(0).IsName("Map_UI_Appear") == true)
//            return;
//        else
//            anim_Map_UI.Play(animStateName[MenuCursorLocation], 0);

//        var x = plycnt.Horizontal;
//        var y = plycnt.Virtical;
//        var enter = plycnt.isFire1;
//        var cancel = plycnt.isFire2;

//        CursorMove(x, y);
//        CursorButtonEvent(enter, cancel);
        
//    }
    
//    void CursorMove(float x, float y)
//    {
//        if (CursorWaitCount == CursorWait - 1) anim_Map_UI.Play(animStateName[MenuCursorLocation], 0);
//        if (CursorWaitCount == 0)
//        {
//            if (x != 0 || y != 0)
//            {
//                CursorWaitCount = CursorWait;
//                Map_Sound.Instance.CursorMoveSoundPlay();
//                TextAreaClose();
//            }

//            if (x > 0)
//                MenuCursorLocation++;
//            else if (x < 0)
//                MenuCursorLocation--;

//            if (y > 0)
//                MenuCursorLocation--;
//            else if (y < 0)
//                MenuCursorLocation++;

//            CursorMoveForCursorLimit();
//        }
//    }

//    /// <summary>
//    /// カーソルの限界点による移動
//    /// </summary>
//    void CursorMoveForCursorLimit()
//    {
//        if (MenuCursorLocation > animStateName.Length - 1) MenuCursorLocation = 0;
//        else if (MenuCursorLocation < 0) MenuCursorLocation = animStateName.Length - 1;

//        // 誰もいない
//        if (CommonData.Instance.Aresta.isJoined == false && CommonData.Instance.Erina.isJoined == false && CommonData.Instance.Petil.isJoined == false)
//        {
//            MenuCursorLocation = 9;
//            return;
//        }

//        // int[] ns = { 0, 3, 6 };

//        // アレスタがいなくなることは無いが一応
//        if (CommonData.Instance.Aresta.isJoined == false)
//        {
//            // 9 -> 0
//            if (MenuCursorLocation == 0)
//            {
//                if (CommonData.Instance.Erina.isJoined == true) MenuCursorLocation = 3;
//                else if (CommonData.Instance.Petil.isJoined == true) MenuCursorLocation = 6;
//            }
//            // 3 -> 2
//            else if (MenuCursorLocation == 2)
//            {
//                if (CommonData.Instance.Petil.isJoined == true) MenuCursorLocation = 9;
//                else if (CommonData.Instance.Erina.isJoined == true) MenuCursorLocation = 5;
//            }
//        }

//        // エリナ不在
//        if (CommonData.Instance.Erina.isJoined == false)
//        {
//            // 2 -> 3
//            if (MenuCursorLocation == 3)
//            {
//                if (CommonData.Instance.Petil.isJoined == true) MenuCursorLocation = 6;
//                else if (CommonData.Instance.Aresta.isJoined == true) MenuCursorLocation = 9;
//            }
//            // 6 -> 5
//            else if (MenuCursorLocation == 5)
//            {
//                if (CommonData.Instance.Aresta.isJoined == true) MenuCursorLocation = 2;
//                else if (CommonData.Instance.Petil.isJoined == true) MenuCursorLocation = 9;
//            }
//        }

//        // ペテル不在
//        if (CommonData.Instance.Petil.isJoined == false)
//        {
//            // 5 -> 6
//            if (MenuCursorLocation == 6)
//            {
//                MenuCursorLocation = 9;
//                //if (CommonData.Instance.Aresta.isJoined == true) MenuCursorLocation = 0;
//                //else if (CommonData.Instance.Erina.isJoined == true) MenuCursorLocation = 3;
//            }
//            // 9 -> 8
//            else if (MenuCursorLocation == 8)
//            {
//                if (CommonData.Instance.Erina.isJoined == true) MenuCursorLocation = 5;
//                else if (CommonData.Instance.Aresta.isJoined == true) MenuCursorLocation = 2;
//            }
//        }


//    }


//    /************************************************
//     *
//     *          ボタン、クリック時のイベント
//     * 
//     * *********************************************/
//    /// <summary>
//    /// 決定、キャンセル。
//    /// </summary>
//    /// <param name="ent"></param>
//    /// <param name="cnc"></param>
//    void CursorButtonEvent(bool ent, bool cnc)
//    {
//        if (CursorWaitCount > 0 || ClickWaitCount > 0 || menustck != 0)
//            return;
//        if (cnc == true)
//        {
//            MenuClose();
//            return;
//        }
//        if (ent == true)
//        {
//            ClickEvent(MenuCursorLocation);
//        }
//    }

//    void ClickEventSet()
//    {
//        // 歩き禁止
//        plycnt.isMoveAllow = false;

//        for (int i = 0; i < ob_btns.Length; i++)
//            btns[i] = ob_btns[i].GetComponent<Button>();

//        for (int i = 0; i < btns.Length; i++)
//        {
//            int ii = 0;
//            ii += i;
//            try {
//                btns[ii].onClick.AddListener(() => ClickEvent(ii));
//            }
//            catch
//            {
//                throw new System.Exception("UIボタンイベント追加失敗。");
//            }
//         }
//    }

//    int ClickWait = 60;
//    int ClickWaitCount = 0;
//    /// <summary>
//    /// クリック時、ボタン決定時のイベント
//    /// </summary>
//    /// <param name="n"></param>
//    void ClickEvent(int n)
//    {
//        if (menustck != 0)
//            return;


//        MenuCursorLocation = n;
//        switch (n)
//        {
//            // Aresuta
//            // magic
//            case 0:
//                PlayMagicMessage(0);
//                return;
//            // item
//            case 1:
//                ItemButtonEventFromUI(0);
//                return;
//            // skill
//            case 2:
//                SkillButtonEventFromUI(0);
//                return;
//            // Erina
//            // magic
//            case 3:
//                PlayMagicMessage(1);
//                return;
//            // item
//            case 4:
//                ItemButtonEventFromUI(1);
//                return;
//            // skill
//            case 5:
//                SkillButtonEventFromUI(1);
//                return;
//            // Petil
//            // magic
//            case 6:
//                PlayMagicMessage(2);
//                return;
//            // item
//            case 7:
//                ItemButtonEventFromUI(2);
//                return;
//            // skill
//            case 8:
//                SkillButtonEventFromUI(2);
//                return;
//            // skill
//            case 9:
//                GameEnd();
//                return;
//            default:
//                return;
//        }

//    }

//    private void PlayMagicMessage(int n)
//    {
//        if (menustck != 0)
//            return;

//        string str = BattleControl.GetMpStr(PlayerDatas.Instance.p_chara[n].MP, PlayerDatas.Instance.p_chara[n].M_MP, PlayerDatas.Instance.p_chara[n].PlayerName);
//        Map_Sound.Instance.DecisionSoundPlay();
//        TextAreaOpen();
//        StartStatusMes(str);
//    }

//    private void PlayVitalMessage(int n)
//    {
//        if (menustck != 0)
//            return;

//        string str = BattleControl.GetHpStr(PlayerDatas.Instance.p_chara[n].HP, PlayerDatas.Instance.p_chara[n].M_HP, PlayerDatas.Instance.p_chara[n].PlayerName, TableValues.Tgt.Player);
//        Map_Sound.Instance.DecisionSoundPlay();
//        TextAreaOpen();
//        StartStatusMes(str);
//    }

//    public bool isGameEndMode = false;
//    public GameObject pref_Choice;
//    private UIWindowElements endUI;
//    public void GameEnd()
//    {
//        if (isGameEndMode)
//            return;

//        PlayerController.Map_EventConfigSet();
//        isGameEndMode = true;
//        StartMesOnClick("*ゲームを終了しますか？");
//        endUI = UIWindowElements.LoadUIWindow(pref_Choice, endUI, transform);
//    }

//    /// <summary>
//    /// OnClick用のメッセージ再生
//    /// </summary>
//    /// <param name="str"></param>
//    public void StartMesOnClick(string str)
//    {
//        Map_Sound.Instance.DecisionSoundPlay();
//        TextAreaOpen();
//        textWin.StartTextPlayNoSpan(str);
//    }
    
//    /// <summary>
//    /// ステータス用のメッセージ再生
//    /// </summary>
//    /// <param name="str"></param>
//    private void StartStatusMes(string str)
//    {
//        textWin.StartTextPlayNoSpan(str);
//    }

//    void TextAreaOpen()
//    {
//        ob_TextWin.SetActive(true);
//    }

//    void TextAreaClose()
//    {
//        ob_TextWin.SetActive(false);
//    }

//    /// <summary>
//    /// カウント0によるキャラクター表示
//    /// </summary>
//    void CharaViewJudgeForMenuOpen()
//    {
//        if ((WaitCount == 0 && plycnt.IsButtonAndAxisIsNoZero == false))
//        {
//            CharacterView();
//        }
//    }

//    /// <summary>
//    /// キーによるメニューオープンチェック
//    /// </summary>
//    void KeyJudgeForMenuOpen()
//    {
//        if (CursorWaitCount == 0 && plycnt.IsButtonAndAxisIsNoZero == true)
//        {
//            if (plycnt.isFire2 == true)
//                MenuOpen();
//            else
//                CharacterHide();

//            WaitCount = WaitCountFlame;
//        }
//    }

//    /// <summary>
//    /// 仲間にいるかで表示を変更
//    /// </summary>
//    void CharaShowOrHideByJoined()
//    {
//        if (CommonData.Instance.Aresta.isJoined == true)
//        {
//            obs_Chara[0].SetActive(true);
//            obs_Chara[1].SetActive(true);
//        }
//        else
//        {
//            obs_Chara[0].SetActive(false);
//            obs_Chara[1].SetActive(false);
//        }

//        if (CommonData.Instance.Erina.isJoined == true)
//        {
//            obs_Chara[2].SetActive(true);
//            obs_Chara[3].SetActive(true);
//        }
//        else
//        {
//            obs_Chara[2].SetActive(false);
//            obs_Chara[3].SetActive(false);
//        }

//        if (CommonData.Instance.Petil.isJoined == true)
//        {
//            obs_Chara[4].SetActive(true);
//            obs_Chara[5].SetActive(true);
//        }
//        else
//        {
//            obs_Chara[4].SetActive(false);
//            obs_Chara[5].SetActive(false);
//        }
//    }

//    /// <summary>
//    /// 左からキャラクター表示
//    /// </summary>
//    /// <returns></returns>
//    bool CharacterView()
//    {
//        if (anim_Map_UI.GetCurrentAnimatorStateInfo(0).IsName("Empty") == false)
//        {
//            CharaShowOrHideByJoined();
//            anim_Map_UI.Play("Map_UI_Appear", 0);
//            return false;
//        }
//        return true;
//    }

//    /// <summary>
//    /// キャラクターを左に隠す
//    /// </summary>
//    void CharacterHide()
//    {
//        if (anim_Map_UI.GetCurrentAnimatorStateInfo(0).IsName("Map_UI_OutOfCanvas") == false && 
//            anim_Map_UI.GetCurrentAnimatorStateInfo(0).IsName("Map_UI_Remove") == false &&
//            anim_Map_UI.GetCurrentAnimatorStateInfo(0).IsName("Empty_Rem") == false)
//            anim_Map_UI.Play("Map_UI_Remove", 0);
//    }

//    /// <summary>
//    /// メニューを開く
//    /// </summary>
//    void MenuOpen()
//    {
//        img_backGround.DOFade(0.65f, 0.8f);
//        CharacterView();
//        WaitCount = WaitCountFlame;
//        CursorWaitCount = CursorWait;

//        Map_Sound.Instance.DecisionSoundPlay();
//        isOpenTheMenu = true;
//        // 動作禁止
//        PlayerController.Menu_InputConfigSet();
//    }

//    /// <summary>
//    /// メニューを終了する。
//    /// </summary>
//    void MenuClose()
//    {
//        img_backGround.DOFade(0, 0.8f);
//        // 歩き許可
//        PlayerController.Map_InputConfigSet();

//        CharacterHide();
//        TextAreaClose();
//        CursorWaitCount = CursorWait;
//        Map_Sound.Instance.CancelSoundPlay();
//        isOpenTheMenu = false;
//        menustck = 0;
//        MapComponentsContainar.DeleteAll();
//    }
//    /// <summary>
//    /// スキルボタン
//    ///TODO:まだまだ
//    /// </summary>
//    void SkillButtonEventFromUI(int n)
//    {
//        if (menustck != 0)
//            return;

//        Map_Sound.Instance.DecisionSoundPlay();
//        ClickWaitCount = ClickWait;
//        sklbook.SetSkill(n);
//        sklbook.ShowBook();
//        ob_btns[0].GetComponent<ParticleSystem>().Stop();
//        ob_btns[3].GetComponent<ParticleSystem>().Stop();
//        ob_btns[6].GetComponent<ParticleSystem>().Stop();
//        sklbook.SetCallBack(() => {
//            ob_btns[0].GetComponent<ParticleSystem>().Play();
//            ob_btns[3].GetComponent<ParticleSystem>().Play();
//            ob_btns[6].GetComponent<ParticleSystem>().Play();
//            menustck = 0;
//            ClickWaitCount = 2 * ClickWait / 3;
//            return;
//        });
//        menustck = 1;
//        return;
//    }

//    /// <summary>
//    /// アイテムボタン
//    ///TODO:まだまだ
//    /// </summary>
//    void ItemButtonEventFromUI(int n)
//    {
//        if (menustck != 0)
//            return;

//        Map_Sound.Instance.DecisionSoundPlay();
//        ClickWaitCount = ClickWait;
//        itembook.SetItem(PlayerDatas.Instance.p_chara[n]);
//        itembook.ShowBook();
//        ob_btns[0].GetComponent<ParticleSystem>().Stop();
//        ob_btns[3].GetComponent<ParticleSystem>().Stop();
//        ob_btns[6].GetComponent<ParticleSystem>().Stop();
//        itembook.SetCallBack(() => {
//            ob_btns[0].GetComponent<ParticleSystem>().Play();
//            ob_btns[3].GetComponent<ParticleSystem>().Play();
//            ob_btns[6].GetComponent<ParticleSystem>().Play();
//            menustck = 0;
//            ClickWaitCount = 2 * ClickWait / 3;
//            return;
//        });
//        menustck = 1;
//        return;
//    }

//    /// <summary>
//    /// 外部処理前処理
//    /// </summary>
//    void BeforeExternalProcessing(Book b)
//    {
//        //TODO:複数の処理をできるようにする。
//        b.SetCallBack(() => {
//            menustck = 0;
//            ClickWaitCount = ClickWait;
//            return;
//        });
//    }
    
//}
