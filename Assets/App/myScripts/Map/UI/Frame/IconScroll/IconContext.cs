﻿using System;

namespace IconScroll
{
    public class IconContext
    {
        public int SelectedIndex = -1;
        public Action<int> OnCellClicked;
    }
}