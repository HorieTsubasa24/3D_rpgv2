﻿
/// <summary>
/// 命令解析→開くフォーム選択クラス
/// </summary>
public static class InstructionAnalysis
{
    public static readonly string[] Words = { "playcond", "eventtag", "flag", "val", "menm", "position", "FadeoutBGM", "If", "Else", "Endif", "FullCare", "CharaDelete", "CharaView",
        "Choice", "Branch", "EndChoice", "Effect", "Exp", "Flag", "ScreenFlash", "HPMP", "Item", "LV", "Member", "Money",
        "Note", "ScreenShake", "Skill", "Text", "Variable", "ScreenTone" ,"PlayBGM", "PlaySE", "SubT", "SubN",
        "CharaName", "NoWriteLine", "Battle", "Call", "Camp", "Break", "LabelJump", "Label", "ValueEntry", "MovePlace", "Shop", "Condition", "Delete", "Wait",
        "CharaFront", "CharaBack", "InputText", "GameMethod"};

    /// <summary>
    /// 命令IDサーチ 返り値にID
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static int SeachWord(string str) {
        int idxof = 0;
        int instid = 0;
        for (int i = 0; i < Words.Length; i++) {
            idxof = str.IndexOf(Words[i]);
            if (idxof == 0) {
                instid = i;
                return instid;
            }
        }
        return -1;
    }

    /// <summary>
    /// 命令IDサーチ 返り値に文字列
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static string SeachWordAsString(string str) {
        str = CollectionTool.RemoveChar(str, '\t');

        int idxof = 0;
        int instid = 0;
        for (int i = 0; i < Words.Length; i++) {
            idxof = str.IndexOf(Words[i]);
            if (idxof == 0) {
                instid = i;
                return Words[instid];
            }
        }
        return null;
    }

    /// <summary>
    /// 文字列引数の取得
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    private static string GetStrArg(ref string str) {
        if (str.Length < 2) return null;

        // '\"' '\"' ',' delete
        int a = str.IndexOf('\"', 1);
        // 文字列引数
        string strone = "";
        if (a != -1) {
            strone = str.Substring(0, a + 1);
            str = str.Remove(0, strone.Length);
            // , 消去
            if (str.Length > 0) str = str.Remove(0, 1);
            // \" \" 削除
            strone = strone.Remove(0, 1);
            strone = strone.Remove(strone.Length - 1, 1);
        }
        return strone;
    }

    /// <summary>
    /// int引数配列の取得
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    private static int[] GetIntArgs(string str) {
        // int引数配列の生成
        string[] stargs = str.Split(',');
        int[] Args = new int[stargs.Length];
        for (int i = 0; i < stargs.Length; i++)
            if (stargs[i].Length > 0)
                Args[i] = int.Parse(stargs[i]);

        return Args;
    }

    /// <summary>
    /// 取得する命令の情報群。
    /// </summary>
    public struct InstructionVariable
    {
        /// <summary>
        /// 命令Id。
        /// </summary>
        public int InstID;

        /// <summary>
        /// 命令名。
        /// </summary>
        public string InstName;

        /// <summary>
        /// 文字の引数。
        /// </summary>
        public string ArgStr;

        /// <summary>
        /// 数値の引数。
        /// </summary>
        public int[] Args;

        public InstructionVariable(int id, string i_name, string s_args, int[] args) {
            InstID = id;
            InstName = i_name;
            ArgStr = s_args;
            Args = args;
        }
    }

    /// <summary>
    /// 命令の構造体を取得。
    /// </summary>
    /// <param name="str">命令ソース(1行)</param>
    /// <returns></returns>
    public static InstructionVariable? GetInstructionNameAndArgs(string str) {
        // 先頭の'\t'Remove
        str = CollectionTool.RemoveChar(str, '\t');
        // 後ろの'　'Remove
        str = CollectionTool.RemoveChar(str, '　');
        if (str.Length == 0) return null;


        int instid = SeachWord(str);
        if (instid == -1) return null;

        // ()のない命令はここでリターン
        if (str[str.Length - 1] == ':')
            return new InstructionVariable(instid, Words[instid], null, null);

        // "instruct(", ")" delete 
        str = str.Remove(0, Words[instid].Length + 1);
        str = str.Remove(str.Length - 1, 1);

        // 文字列引数
        string BefStrs = GetStrArg(ref str);

        // int引数配列の生成
        int[] BefArgs = GetIntArgs(str);

        return new InstructionVariable(instid, Words[instid], BefStrs, BefArgs);
    }


    /// <summary>
    /// イベントリスト表示用の文字列の取得
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static string GetViewStrs(string str) {
        // 先頭の'\t'Remove
        str = CollectionTool.RemoveChar(str, '\t');
        // 後ろの'　'Remove
        str = CollectionTool.RemoveChar(str, '　');
        if (str.Length <= 2) return null;

        int instid = SeachWord(str);
        if (instid == -1) return null;

        // "instruct(", ")" delete 
        str = str.Remove(0, Words[instid].Length + 1);
        // Endifなどの巻き込み防止
        if (str.Length > 0)
            str = str.Remove(str.Length - 1, 1);

        // 文字列引数
        string strone = GetStrArg(ref str);

        // int引数配列の生成
        int[] Args = GetIntArgs(str);

        return GetSelectStrs(instid, strone, Args);
    }

    /// <summary>
    /// 解析した文字列から個々の表示用文字列取得。
    /// </summary>
    /// <param name="idx"></param>
    /// <param name="strone"></param>
    /// <param name="Args"></param>
    /// <returns></returns>
    private static string GetSelectStrs(int idx, string strone, int[] Args) {
        string retstr = "";
        switch (Words[idx]) {
            default:
                retstr = Words[idx];
                break;
            case "FadeoutBGM":
                retstr = "BGMのフェードアウト";
                break;
            case "If":
                retstr = "条件分岐";
                break;
            case "Else":
                retstr = "条件以外の場合";
                break;
            case "Endif":
                retstr = "条件分岐終了";
                break;
            case "FullCare":
                retstr = "全回復";
                break;
            case "CharaDelete":
                retstr = "キャラクター消去";
                break;
            case "CharaView":
                retstr = "キャラクター表示";
                break;
            case "Choice":
                retstr = "選択肢の処理";
                break;
            case "Branch":
                retstr = "選択肢";
                break;
            case "EndChoice":
                retstr = "選択肢処理の終了";
                break;
            case "Effect":
                retstr = "マップエフェクト";
                break;
            case "Exp":
                retstr = "経験値の増減";
                break;
            case "Flag":
                retstr = "フラグの操作";
                break;
            case "ScreenFlash":
                retstr = "画面のフラッシュ";
                break;
            case "HPMP":
                retstr = "HP,MPの増減";
                break;
            case "Item":
                retstr = "アイテムの増減";
                break;
            case "LV":
                retstr = "レベルの増減";
                break;
            case "Member":
                retstr = "メンバーの入れ替え";
                break;
            case "Money":
                retstr = "所持金の増減";
                break;
            case "Note":
                retstr = "//";
                break;
            case "ScreenShake":
                retstr = "画面のシェイク";
                break;
            case "Skill":
                retstr = "スキルの増減";
                break;
            case "Text":
                retstr = "文字列の表示";
                break;
            case "Variable":
                retstr = "変数の操作";
                break;
            case "ScreenTone":
                retstr = "画面の色調操作";
                break;
            case "PlayBGM":
                retstr = "BGMの再生";
                break;
            case "PlaySE":
                retstr = "効果音の再生";
                break;
            case "SubT":
                retstr = "　　　　　　";
                break;
            case "SubN":
                retstr = "//";
                break;
            case "CharaName":
                retstr = "キャラ名の表示";
                break;
            case "NoWriteLine":
                break;
            case "Battle":
                retstr = "戦闘の処理";
                break;
            case "Call":
                retstr = "他イベントの呼び出し";
                break;
            case "Camp":
                retstr = "拠点を開く";
                break;
            case "Break":
                retstr = "イベント処理の中断";
                break;
            case "LabelJump":
                retstr = "指定ラベルに移動";
                break;
            case "Label":
                retstr = "ラベルの設置";
                break;
            case "ValueEntry":
                retstr = "数値入力の処理";
                break;
            case "MovePlace":
                retstr = "場所移動";
                break;
            case "Shop":
                retstr = "お店の処理";
                break;
            case "Condition":
                retstr = "状態の変更";
                break;
            case "Delete":
                retstr = "イベントの一時削除";
                break;
            case "Wait":
                retstr = "ウェイト : ";
                break;
            case "CharaFront":
                retstr = "キャラを最前面に移動";
                break;
            case "CharaBack":
                retstr = "キャラを再背面に移動";
                break;
            case "InputText":
                retstr = "テキストの入力";
                break;
            case "GameMethod":
                retstr = "ゲーム内メソッドを呼び出す。";
                break;
        }
        return retstr;
    }
}
