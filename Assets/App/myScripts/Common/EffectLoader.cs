﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectLoader : MonoBehaviour {

	// Use this for initialization
	void Start () {
        BundleCaches.LoadEffects(SkillTable.Instance);
    }
}
