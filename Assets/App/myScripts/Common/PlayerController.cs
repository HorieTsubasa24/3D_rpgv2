﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// コントローラー装置。入力をキャッシュに保存している。押しっぱなしでの連続入力に対応。
/// 高頻度の使用の場合は各クラス内にPlayerController plycnt;を宣言しています。
/// </summary>
public class PlayerController : Singleton<PlayerController> {
    /*-----------------------------------
	 * 									*
	 * 				入力許可				*
	 * 									*
	-----------------------------------*/
    /// <summary>
    /// 入力許可
    /// </summary>
    public bool isInputAllow;
    /// <summary>
    /// キャンセルキー メニュー開閉ボタン使用可能
    /// </summary>
    public bool isMenuAllow;
    /// <summary>
    /// 移動ボタン使用可能
    /// </summary>
    public bool isMoveAllow;
    /// <summary>
    /// 文字列表示中
    /// 外部のメソッドで変化する。
    /// </summary>
    public bool isPlayString;
    /// <summary>
    /// イベントを調べられるかどうか。
    /// </summary>
    public bool isEventAllow;
    /// <summary>
    /// trueでキーダウンで読み込み
    /// falseで押しっぱなしで読み込み
    /// </summary>
    public bool isKeyDownMode = true;
    /// <summary>
    /// ディレイ
    /// </summary>
    [SerializeField]
    private int delay = 0;
    /// <summary>
    /// 軸ディレイ
    /// </summary>
    [SerializeField]
    private int axisdelay = 0;

    [SerializeField]
    private readonly int delayflame = 3;
    [SerializeField]
    private readonly int axisdelayflame = 6;
    [SerializeField]
    private bool isFirstAxis = true;
    [SerializeField]
    private bool isRepeat;
    [SerializeField]
    private readonly int axisinitflame = 10;

    public static int FirsrKeyWait { get { return Instance.axisinitflame; } }
    public static int RepeatKeyWait { get { return Instance.axisdelayflame; } }
    public static bool IsRepeat { get { return Instance.isRepeat; } }

    // ジョイスティックの入力バッファ
    public bool[] joy_buttons_buf = new bool[3];
    public bool joy_buttons_leftPad;
    public bool joy_buttons_rightPad;
    public Vector2 joy_Axis_buf;

    // -------------------- 各モードごとの入力許可設定 -------------------------

    public static void Map_EventConfigSet()
    {
        TextWin.isAllowSkip = true;
        PlayerController.Instance.isInputAllow = true;
        PlayerController.Instance.isEventAllow = false;
        PlayerController.Instance.isMenuAllow = false;
        PlayerController.Instance.isMoveAllow = false;
        PlayerController.Instance.isPlayString = false;
        PlayerController.Instance.isKeyDownMode = true;
    }

    /// <summary>
    /// マップ上待機状態時の入力に設定
    /// </summary>
    /// <param name="isMovePlace"></param>
    public static void Map_InputConfigSet(bool isMovePlace = false)
    {
        TextWin.isAllowSkip = true;
        RunEventOnTrigger.isEventOnCollide = false; // 接触判定の一時オフ
        PlayerController.Instance.isInputAllow = true;
        PlayerController.Instance.isEventAllow = true;
        PlayerController.Instance.isMenuAllow = !isMovePlace;
        PlayerController.Instance.isMoveAllow = true;
        PlayerController.Instance.isPlayString = false;
        PlayerController.Instance.isKeyDownMode = true;
    }

    public static void Battle_InputConfigSet()
    {
        TextWin.isAllowSkip = true;
        PlayerController.Instance.isInputAllow = true;
        PlayerController.Instance.isEventAllow = false;
        PlayerController.Instance.isMenuAllow = false;
        PlayerController.Instance.isMoveAllow = true;
        PlayerController.Instance.isPlayString = false;
        PlayerController.Instance.isKeyDownMode = true;
    }

    public static void Menu_InputConfigSet()
    {
        TextWin.isAllowSkip = true;
        PlayerController.Instance.isInputAllow = true;
        PlayerController.Instance.isEventAllow = false;
        PlayerController.Instance.isMenuAllow = true;
        PlayerController.Instance.isMoveAllow = false;
        PlayerController.Instance.isPlayString = false;
        PlayerController.Instance.isKeyDownMode = true;
    }

    // -------------------- 入力待ちモード -------------------------

    public bool _isInputWaiting;
    public System.Action _waitAction;
    public static void GotoInputWaitMode(System.Action act) {
        PlayerController.Instance.ResetInput(10);
        PlayerController.Instance._isInputWaiting = true;
        PlayerController.Instance._waitAction = act;
    }

    public void CheckWaitingAndRun() {
        ButtonInputSet();
        AxisInputSet();
        if (IsButtonAndAxisIsNoZero) {
            _isInputWaiting = false;
            _waitAction.Invoke();
            _waitAction = null;
            PlayerController.Instance.ResetInput(10);
            return;
        }
        return;
    }

    // -------------------------------------------------------------

    private void ButtonInputSet()
    {
        for (int i = 0; i < joy_buttons_buf.Length; i++)
        {
            if (isKeyDownMode)
                joy_buttons_buf[i] = Input.GetButtonDown("Fire" + (1 + i).ToString());
            else
                joy_buttons_buf[i] = Input.GetButton("Fire" + (1 + i).ToString());
        }
        if (isKeyDownMode)
        {
            joy_buttons_leftPad = Input.GetButtonDown("LeftPad");
            joy_buttons_rightPad = Input.GetButtonDown("RightPad");
        }
        else
        {
            joy_buttons_leftPad = Input.GetButton("LeftPad");
            joy_buttons_rightPad = Input.GetButton("RightPad");
        }
    }

    private bool IsAxisZero()
    {
        var a = Input.GetAxis("Horizontal");
        var b = Input.GetAxis("Vertical");
        return (a == 0 && b == 0);
    }

    private bool IsButtonZero()
    {
        var a = Input.GetAxis("Horizontal");
        var b = Input.GetAxis("Vertical");
        return (a == 0 && b == 0);
    }

    private void AxisInputSet()
    {
        if (isKeyDownMode == true)
        {
            if (axisdelay > 0)
            {
                joy_Axis_buf.x = 0.0f;
                joy_Axis_buf.y = 0.0f;
                return;
            }
        }
        AxisInputValGet();
    }

    private void AxisInputValGet()
    {
        joy_Axis_buf.x = Input.GetAxis("Horizontal");
        joy_Axis_buf.y = Input.GetAxis("Vertical");

        if (joy_Axis_buf.x == 0 && joy_Axis_buf.y == 0)
            return;

        if (isFirstAxis == true)
        {
            axisdelay = (int)(axisinitflame / 60.0f / Time.smoothDeltaTime);
            //Debug.Log(axisdelay);
            isFirstAxis = false;
        }
        else
        {
            isRepeat = true;
            axisdelay = (int)(axisdelayflame / 60.0f / Time.smoothDeltaTime);
        }
    }

    /// <summary>
    /// ボタンの読込方法
    /// </summary>
    /// <param name="b"></param>
    public void SetKeyDownMode(bool b)
    {
        isKeyDownMode = b;
    }

    /// <summary>
    /// 連続で入力の防止
    /// </summary>
    public void SetInputDelay() {
        this.delay = (int)(delayflame / 60.0f / Time.smoothDeltaTime);
        for (int i = 0; i < joy_buttons_buf.Length; i++)
            joy_buttons_buf[i] = false;
        joy_Axis_buf.x = 0.0f;
        joy_Axis_buf.y = 0.0f;
    }

    /// <summary>
    /// メニュー等連続入力禁止
    /// </summary>
	public void SetInputDelay(int dly) {
        this.delay = (int)(dly / 60.0f / Time.smoothDeltaTime);
    }


    public void ResetInput(int dly = -1) {
        if (dly == -1)
            dly = delayflame;

        SetInputDelay(dly);
        SetAxisDelay();

        for (int i = 0; i < joy_buttons_buf.Length; i++)
            joy_buttons_buf[i] = false;
        joy_Axis_buf.x = 0.0f;
        joy_Axis_buf.y = 0.0f;
    }

    /// <summary>
    /// 連続で入力の防止
    /// </summary>
    private void SetAxisDelay()
    {
        axisdelay = (int)(axisdelayflame / 60.0f / Time.smoothDeltaTime);
    }

    /// <summary>
    /// 動作全ロック
    /// </summary>
    public void ControlLock()
    {
        isMoveAllow = false;
        isMenuAllow = false;
    }
    
    public void SetAllow(bool a)
	{
		isInputAllow = a;
	}

	public bool isFire1
	{
		get { return (isInputAllow == false) ? false : joy_buttons_buf[0]; }
	}
	public bool isFire2
	{
		get { return (isInputAllow == false) ? false : joy_buttons_buf[1]; }
	}
	public bool isFire3
	{
		get { return (isInputAllow == false) ? false : joy_buttons_buf[2]; }
	}
	public float Horizontal
	{
		get { return (isInputAllow == false) ? 0.0f : joy_Axis_buf.x; }
	}
	public float Virtical
	{
		get { return (isInputAllow == false) ? 0.0f : joy_Axis_buf.y;}
	}
    /// <summary>
    /// ボタンや方向キーに触れているか
    /// </summary>
    public bool IsButtonAndAxisIsNoZero
    {
        get
        {
            return (isFire1 || isFire2 || isFire3 || (Horizontal != 0.0f) || (Virtical != 0.0f));
        }
    }

    private void Awake()
    {
        if (Instance != null)
        {
            gameObject.SetActive(false);
            return;
        }

        instance = this;
    }

    // Use this for initialization
    void Start() {
        for (int i = 0; i < joy_buttons_buf.Length; i++)
            joy_buttons_buf[i] = false;
        joy_Axis_buf = new Vector2();
        isInputAllow = true;
        isPlayString = false;
    }

    // Update is called once per frame
    void LateUpdate() {
        if (_isInputWaiting) {
            if (axisdelay > 0)
                axisdelay--;
            if (delay > 0) {
                delay--;
                return;
            }
            CheckWaitingAndRun();
            return;
        }

        if (IsAxisZero()) {
            isFirstAxis = true;
            axisdelay = 0;
            isRepeat = false;
        }


        if (axisdelay > 0)
            axisdelay--;
        if (delay > 0) {
            delay--;
            return;
        }

        ButtonInputSet();
        AxisInputSet();
    }

}
