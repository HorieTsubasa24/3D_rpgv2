﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

/// <summary>
/// 同じゲームオブジェクトにMapEventがアタッチされているので、
/// EVBranchから命令をスタートさせることができる。
/// </summary>
[System.Serializable]
public class ShopObject : MapComponent
{
    public static bool isShoping = true;

    private PlayerController plycnt;
    private TextWin textWindow;
    [SerializeField]
    private GameObject pref_ShopBuySellUI;
    [SerializeField]
    private GameObject pref_ViewItemProperty;
    [SerializeField]
    private GameObject pref_Item;
    [SerializeField]
    private GameObject pref_Menu;
    private UIWindowElements menu;
    [SerializeField]
    private GameObject pref_CharaSelectUI;
    private UIWindowElements charaSelectUI;
    [SerializeField]
    private GameObject pref_ShopSelectUI;
    private UIWindowElements shopSelectUI;
    [SerializeField]
    private GameObject pref_YesNoUI;
    private UIWindowElements yesNoUI;
    
    [SerializeField] // デバッグ用
    private string[] strArgs;

    private Tweener tweener;

    // --------------------メッセージ-------------------------
    //
    // お店の人が話す言葉
    private string[] talkStrs = new string[3];
    // 買う、売る、帰る
    private string[] selectMenuStrs = new string[3];
    // 買う売るメッセージ
    private string[] buyMessageStrs = new string[5];
    private string[] sellMessageStrs = new string[5];

    private void SetConvertStrArgs(string[] strs)
    {
        int n = 0;
        for (int i = 0; i <= 2; i++, n++)
            talkStrs[i] = strs[n];
        for (int i = 0; i <= 2; i++, n++)
            selectMenuStrs[i] = strs[n];
        for (int i = 0; i <= 4; i++, n++)
            buyMessageStrs[i] = strs[n];
        for (int i = 0; i <= 4; i++, n++)
            sellMessageStrs[i] = strs[n];
    }
    
    /// <summary>
    /// 店で販売するものリスト。
    /// </summary>
    [SerializeField]
    private int[] buyArgs;
    
    /// <summary>
    /// 今の段階を表すフェーズ
    /// </summary>
    public enum Phase {
        firsttalk, secondtalk, endshop,
        selectbuy, decisionbuy, charaselectbuy, canselbuy, buycompleated,
        charaselectsell, selectsell, decisionsell, canselsell, sellcompleated,
    };

    public Phase phase = Phase.firsttalk;

    private UIWindowElements LoadUIWindowShop(GameObject pref, UIWindowElements ui)
    {
        return UIWindowElements.LoadUIWindow(pref, ui, transform);
    }

    /// <summary>
    /// IDから販売リスト、売却リストを作成する。
    /// </summary>
    /// <param name="vs"></param>
    private Item[] GetItemData(int[] vs)
    {
        var its = new Item[vs.Length];
        for(int i = 0; i < vs.Length; i++)
        {
            // 引数+1のアイテムID
            its[i] = ItemTable.Instance.GetItemForId(vs[i] + 1);
        }
        return its;
    }

    private Item[] items;
    /// <summary>
    /// アイテムデータのセット
    /// </summary>
    /// <param name="jb"></param>
    private void SetItemDataToItemList()
    {
        shopSelectUI.DeleteItemList();
        if (phase == Phase.selectbuy)
        {
            items = GetItemData(buyArgs);
            shopSelectUI.AddItemElements(items);
        }
        else //phase = Phase.charaselectsell;
        {
            charaSelectUI = UIWindowElements.LoadUIWindow(pref_CharaSelectUI, charaSelectUI, transform.parent);

            TextWin textWin = (TextWin)MapComponentsContainar.Instance.GetOneComponent(Name.TextWin);
            if (textWin != null)
                textWin.HideAnimation(false);
            string[] str = { "誰が持ち物を売りますか？" };
            charaSelectUI.InitTexts(str);
            // アイテムリスト選択を無効化して逃がす
            shopSelectUI.isChoicing = false;
        }

    }

    /// <summary>
    /// アイテムリストをフェードインしていく。
    /// </summary>
    private void ItemListFadeIn()
    {
        imgs_ItemListUI = shopSelectUI.gameObject.GetComponentsInChildren<Image>();
        foreach (var a in imgs_ItemListUI)
        {
            Color befcol = new Color(a.color.r, a.color.g, a.color.b, a.color.a);
            a.color = new Color(a.color.r, a.color.g, a.color.b, 0);
            a.DOColor(befcol, 0.6f);
        }
        foreach (var b in imgs_ViewItemProperty)
        {
            Color befcol = new Color(b.color.r, b.color.g, b.color.b, b.color.a);
            b.color = new Color(b.color.r, b.color.g, b.color.b, 0);
            b.DOColor(befcol, 0.6f);
        }
    }
    
    /// <summary>
    /// キャラ移動のウェイト
    /// </summary>
    /// <param name="sign"></param>
    private void ToWaitProcessing(int sign)
    {
        isWait = true;
        elements = FindObjectsOfType<UIWindowElements>();
        foreach (var a in elements)
            a.enabled = false;
        // キャラID:0のキャラクターを右に移動。
        MapCharacter c = FindObjectOfType<MapCharacter>();
        if (tweener != null && tweener.IsActive())
            tweener.Kill();
        tweener = c.transform.DOLocalMoveX(c.transform.localPosition.x + Mathf.Sign(sign) * 0.08f, 0.6f).OnComplete(() =>
        {
            foreach (var a in elements)
                a.enabled = true;
            isWait = false;
        });
    }

    private GameObject spwn_ViewItemProperty;
    private Image[] imgs_ItemListUI;
    private Image[] imgs_ViewItemProperty;
    /// <summary>
    /// 買う時、売るときにアイテムリスト、説明リストを表示する。
    /// </summary>
    private void SetInitItemList()
    {
        ToWaitProcessing(1);
        // アイテムリスト追加
        if (shopSelectUI == null)
        {
            shopSelectUI = LoadUIWindowShop(pref_ShopSelectUI, shopSelectUI);
        }
        else // アイテムリストがすでにあるときは有効化
        {
            shopSelectUI.ShowObject();
        }
        // アイテムの説明欄
        if (spwn_ViewItemProperty == null)
        {
            spwn_ViewItemProperty = Instantiate(pref_ViewItemProperty, transform);
            imgs_ViewItemProperty = spwn_ViewItemProperty.GetComponentsInChildren<Image>();
        }
        else
            spwn_ViewItemProperty.SetActive(true);

        // フェードイン
        ItemListFadeIn();

        // 買う、売るの初期化
        SetItemDataToItemList();
    }

    /// <summary>
    /// 買う、売るのときアイテムリスト、説明リストを開く。
    /// </summary>
    private void EnableInitItemList()
    {
        shopSelectUI = LoadUIWindowShop(pref_ShopSelectUI, shopSelectUI);
        spwn_ViewItemProperty.SetActive(true);
    }

    /// <summary>
    /// 買う、売るをやめてアイテムリスト、説明リストを閉じる。(オブジェクトは残る)
    /// </summary>
    private void DisableInitItemList()
    {
        shopSelectUI.HideObject(true);
        spwn_ViewItemProperty.SetActive(false);
    }

    // Use this for initialization
    public override void Awake ()
    {
        plycnt = PlayerController.Instance;
        plycnt.SetKeyDownMode(true);
        base.comname = Name.Shop;
        base.AddContainerIndex();
        menu = Instantiate(pref_Menu, transform).GetComponent<UIWindowElements>();
        isShoping = true;
    }

    private void TalkProcessing()
    {
        if (phase == Phase.endshop)
        {
            if (!TextWin.isMesPlaying && plycnt.isFire1)
            {
                isShoping = false;
                gameObject.SetActive(false);
            }
        }
        
        if (phase == Phase.firsttalk || phase == Phase.secondtalk)
        {
            if (menu.isChoicing == false)
            {
                switch (menu.DecisionIndex)
                {
                    case 0:
                        PlayMenuText(buyMessageStrs[0]);
                        phase = Phase.selectbuy;
                        SetInitItemList();
                        break;
                    //case 1:
                    //    PlayMenuText(sellMessageStrs[0]);
                    //    phase = Phase.charaselectsell;
                    //    SetInitItemList();
                    //    break;
                    default:
                    //case 2:
                        PlayMenuTextNormal(talkStrs[2]);
                        phase = Phase.endshop;
                        break;
                }
            }
        }
    }

    private UIWindowElements[] elements;
    private void ReturnTopProcess()
    {
        ToWaitProcessing(-1);

        DisableInitItemList();
        menu.ShowObject();
        PlayMenuText(talkStrs[1]);
    }

    private void BuyProcessing()
    {
        // 買う
        if (phase == Phase.selectbuy)
        {
            if (shopSelectUI.isChoicing == false)
            {
                if (shopSelectUI.DecisionIndex != -1)
                {
                    PlayMenuText(buyMessageStrs[1]);
                    phase = Phase.decisionbuy;
                    yesNoUI = LoadUIWindowShop(pref_YesNoUI, yesNoUI);
                }
                else
                {
                    ReturnTopProcess();
                    phase = Phase.secondtalk;
                }
            }
        }
        // 買う決定
        if (phase == Phase.decisionbuy)
        {
            if (yesNoUI.isChoicing == false)
            {
                if (yesNoUI.DecisionIndex == 0)
                {
                    var gold = CommonData.Instance.Gold;
                    if (gold < shopSelectUI.nowItemElement.item.Gold)
                    {
                        PlayMenuText("お金が足りません。");
                        shopSelectUI.isChoicing = true;
                        phase = Phase.selectbuy;
                        return;
                    }

                    TextWin textWin = (TextWin)MapComponentsContainar.Instance.GetOneComponent(Name.TextWin);
                    if (textWin != null)
                        textWin.HideAnimation(false);
                    charaSelectUI = UIWindowElements.LoadUIWindow(pref_CharaSelectUI, charaSelectUI, transform.parent);
                    string[] str = { "誰に渡しますか？" };
                    charaSelectUI.InitTexts(str);
                    phase = Phase.charaselectbuy;
                }
                else
                {
                    PlayMenuText(buyMessageStrs[3]);
                    // EnableInitItemList();
                    shopSelectUI.ShowObject();
                    phase = Phase.selectbuy;
                }
            }
        }
        // 買うキャラ選択
        if (phase == Phase.charaselectbuy)
        {
            if (charaSelectUI.isChoicing == false)
            {
                if (charaSelectUI.DecisionIndex == -1)
                {
                    phase = Phase.selectbuy;
                }
                else
                {
                    var player = PlayerDatas.Instance.p_chara[charaSelectUI.DecisionIndex];
                    if (player.GetCharaHaveItemSpace(items[shopSelectUI.DecisionIndex]))
                    {
                        player.AddItem(items[shopSelectUI.DecisionIndex]);
                        CommonData.Instance.Gold -= shopSelectUI.nowItemElement.item.Gold;
                        phase = Phase.buycompleated;
                        return;
                    }
                    else
                    {
                        PlayMenuText("もちものがいっぱい！");
                        shopSelectUI.isChoicing = true;
                        phase = Phase.selectbuy;
                        return;
                    }
                }
            }
        }
        // 買う完了
        if (phase == Phase.buycompleated)
        {
            PlayMenuText(buyMessageStrs[2]);
            // EnableInitItemList();
            shopSelectUI.ShowObject();
            phase = Phase.selectbuy;
        }
    }

    private void SellProcessing()
    {
        // 売るキャラ選択
        if (phase == Phase.charaselectsell)
        {
            if (charaSelectUI.isChoicing == false)
            {
                if (charaSelectUI.DecisionIndex != -1)
                {
                    EnableInitItemList();
                    phase = Phase.selectsell;
                }
                else
                {
                    ReturnTopProcess();
                    phase = Phase.secondtalk;
                }
            }
        }
        // 売る
        if (phase == Phase.selectsell)
        {
            if (shopSelectUI.isChoicing == false)
            {
                if (shopSelectUI.DecisionIndex != -1)
                {
                    PlayMenuText(sellMessageStrs[1]);
                    yesNoUI = LoadUIWindowShop(pref_YesNoUI, yesNoUI);
                    phase = Phase.decisionsell;
                }
                else
                {
                    ReturnTopProcess();
                    phase = Phase.secondtalk;
                }
            }
        }
        // 売る決定
        if (phase == Phase.decisionsell)
        {
            if (yesNoUI.isChoicing == false)
            {
                if (yesNoUI.DecisionIndex == 0)
                    phase = Phase.sellcompleated;
                else
                {
                    PlayMenuText(sellMessageStrs[3]);
                    EnableInitItemList();
                    phase = Phase.selectsell;
                }
            }
        }
        // 売る完了
        if (phase == Phase.sellcompleated)
        {
            PlayMenuText(buyMessageStrs[2]);
            shopSelectUI.ShowObject();
            phase = Phase.selectsell;
        }
    }

    private bool isWait = false;
    private void Update()
    {
        if (isShoping == false || isWait == true)
            return;

        TalkProcessing();
        BuyProcessing();
        SellProcessing();
    }

    private void SetupSelectBranch(ref string[] vs)
    {
        menu.InitTexts(selectMenuStrs);
    }

    private string GetTalkStr(string str)
    {
        var outstr = "";
        for (int i = 0; i < str.Length; i++)
        {
            if (i < str.Length - 1 && str[i] == '\\' && str[i + 1] == 'n')
            {
                outstr += '\n';
                i++;
            }
            else
                outstr += str[i];
        }
        return outstr;
    }

    private void PlayMenuText(string str)
    {
        var outstr = GetTalkStr(str);
        textWindow.SetViewSpan(1);
        textWindow.StartTextPlayNoSpan(outstr, null, false);
    }

    private void PlayMenuTextNormal(string str)
    {
        var outstr = GetTalkStr(str);
        textWindow.SetViewSpan(TextWin.ViewSpanDefault);
        textWindow.StartTextPlay(outstr);
    }

    /// <summary>
    /// EVShopから呼び出す。
    /// strs = メッセージ args = アイテムID
    /// </summary>
    public void RunInit(string[] strs, int[] args, TextWin tw)
    {
        textWindow = tw;

        SetConvertStrArgs(strs);

        strArgs = strs;
        buyArgs = args;
        ShopObject.isShoping = true;
        SetupSelectBranch(ref selectMenuStrs);
        PlayMenuText(talkStrs[0]);
    }

}
