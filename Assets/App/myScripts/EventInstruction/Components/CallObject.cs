﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallObject : MapComponent
{
    public bool isDoing = false;

    // Inspecter表示用。
    [SerializeField]
    private Instruction[] instructions;

    // Use this for initialization
    public override void Awake()
    {
        base.comname = Name.Call;
        base.AddContainerIndex();
    }


    /// <summary>
    /// EVCallから呼び出す。
    /// </summary>
    /// <param name="inst"></param>
    public void StartInstruction(Instruction[] inst, string[] sources)
    {
        instructions = inst;
        var evt = GetComponent<MapEvent>();
        evt.evSources = sources;
        evt.RunEventFromComponents(inst);
        name = "CallObject:" + evt.evSources[0];
        // EventRun()で実行する時に追加される。
        //MapEvent.childEvent.Add(gameObject);
    }
}
