﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// 購入、売却するアイテム。
/// </summary>
public class ItemElement : MonoBehaviour {
    public TextMeshProUGUI text;
    public Image icon;
    public Item item;
}
