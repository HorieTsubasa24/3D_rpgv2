﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// 同じゲームオブジェクトにMapEventがアタッチされているので、
/// EVIfから命令をスタートさせることができる。
/// </summary>
public class IfObject : MapComponent
{
    public bool isDoing = false;

    // Inspecter表示用。
    [SerializeField]
    private Instruction[] instructions;
    
    // Use this for initialization
    public override void Awake ()
    {
        base.comname = Name.If;
        base.AddContainerIndex();
    }
    

    /// <summary>
    /// EVIfから呼び出す。
    /// </summary>
    /// <param name="inst"></param>
    public void StartInstruction(Instruction[] inst, string[] sources)
    {
        instructions = inst;
        var evt = GetComponent<MapEvent>();
        evt.evSources = sources;
        evt.RunEventFromComponents(inst);
        name = "IfObject:" + evt.evSources[0];
        // EventRun()で実行する時に追加される。
        //MapEvent.childEvent.Add(gameObject);
    }
}
