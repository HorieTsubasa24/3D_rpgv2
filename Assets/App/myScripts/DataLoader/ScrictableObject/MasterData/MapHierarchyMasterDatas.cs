﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// 層ごとに1つ作成する。
/// </summary>
[Serializable]
[CreateAssetMenu]
public class MapHierarchyMasterDatas : ScriptableObject, IDisposable
{
    public GameObject[] _pref_Walls = new GameObject[16];
    public GameObject[] _pref_Tiles = new GameObject[16];
    public GameObject[] _pref_Space = new GameObject[16];
    public GameObject _pref_CornerR;
    public GameObject _pref_CornerL;

    public void Dispose() {

    }
}
