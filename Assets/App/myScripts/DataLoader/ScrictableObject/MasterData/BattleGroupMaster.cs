﻿using System.Linq;
using UnityEngine;

namespace Battle
{
    /// <summary>
    /// 敵グループのマスターデータ
    /// </summary>
    [CreateAssetMenu(menuName = "Utility/Create Battle Group Data")]
    public class BattleGroupMaster : ScriptableObject
    {
        public BattlePrefabSource[] BattleGroups { get { return battleGroups.Concat(playerGroups).ToArray(); } }
        public BattlePrefabSource[] battleGroups;
        public BattlePrefabSource[] playerGroups;
    }
}