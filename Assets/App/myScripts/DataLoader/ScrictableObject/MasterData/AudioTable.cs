﻿using System;
using UnityEngine;
using Audio;

/// <summary>
/// Resources用のサウンドデータマスターデータ
/// </summary>
[Serializable]
[CreateAssetMenu(menuName = "Utility/Create Audio Table")]
public class AudioTable : ScriptableObject
{
    public AudioData[] AudioDatas;
}
