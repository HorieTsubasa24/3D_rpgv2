﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// データ読み出しの時セットされる。
/// </summary>
[CreateAssetMenu]
[System.Serializable]
public class IconDatas : ScriptableObject
{
    [SerializeField]
    private Sprite defaultIcon;
    [SerializeField]
    private IconStruct[] itemIcons = new IconStruct[0];
    [SerializeField]
    private IconStruct[] skillIcons = new IconStruct[0];
    [SerializeField]
    private IconStruct[] kindIcons = new IconStruct[0];

    public void ItemReSize(int itemNum) {
        itemIcons = new IconStruct[itemNum];
    }

    public void SetItemStruct(int idx, int id, string str) {
        itemIcons[idx]._id = id;
        itemIcons[idx]._name = str;
    }

    public Sprite GetItemIcon(int id)
    {
        for (int i = 0; i < itemIcons.Length; i++) {
            if (itemIcons[i]._id == id) {
                return itemIcons[i]._sp_Icon;
            }
        }
        return defaultIcon;
    }

    public Sprite GetSkillIcon(int id) {
        for (int i = 0; i < skillIcons.Length; i++) {
            if (skillIcons[i]._id == id) {
                return skillIcons[i]._sp_Icon;
            }
        }
        return defaultIcon;
    }

    public Sprite GetEffectIcon(TableValues.Effect effect) {
        string effectName = effect.ToString();
        for (int i = 0; i < kindIcons.Length; i++) {
            if (kindIcons[i]._name == effectName) {
                return kindIcons[i]._sp_Icon;
            }
        }
        return defaultIcon;
    }


    [ContextMenu("LoadDatasFromCSV")]
    void LoadDatasFromCSV() {
        //var icondatas = Resources.Load("IconDatasMaster", typeof(IconDatas)) as IconDatas;
        var icondatas = BundleCaches.LoadIconDatas();

        var filepath = Application.streamingAssetsPath + "/Tables/ItemTablev2.csv";
        var allline = File.ReadAllLines(filepath, System.Text.Encoding.UTF8);
        var datalength = allline.Length - 2;
        icondatas.ItemReSize(datalength);

        for (int i = 2; i < allline.Length; i++) {
            var strs = allline[i].Split(',');
            var id = int.Parse(strs[0]);
            var name = strs[1];
            icondatas.SetItemStruct(i - 2, id, name);
        }
    }
}

[System.Serializable]
public struct IconStruct
{
    public string _name;
    public int _id;
    public Sprite _sp_Icon;
}

