﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Audio
{
    /// <summary>
    /// オーディオデータ
    /// </summary>
    [Serializable]
    public class AudioData
    {
        public string Name;
        public AudioClip Clip;

        /// <summary>
        /// オーディオ名とオーディオを設定
        /// </summary>
        /// <param name="audioName"></param>
        /// <param name="audioClip"></param>
        public AudioData(string audioName, AudioClip audioClip)
        {
            Name = audioName;
            Clip = audioClip;
        }
    }

    /// <summary>
    /// オーディオデータを保持、再生など
    /// </summary>
    public class AudioManager : SingletonForData<AudioManager>
    {
        private List<AudioData> audioDatas;

        /// <summary>
        /// オーディオデータを設定する
        /// </summary>
        /// <param name="datas"></param>
        public void Init(AudioData[] datas)
        {
            audioDatas = datas.ToList();
        }

        /// <summary>
        /// オーディオクリップを取得する
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public AudioClip GetAudio(string name)
        {
            var data = audioDatas.FirstOrDefault(audioData => audioData.Name == name);
            return data.Clip;
        }
    }
}