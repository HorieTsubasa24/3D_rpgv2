﻿using System;
using UnityEngine;
/*
　・キャラクターデータ
　　所持アイテム
　　所持キャラ
　　所持武器
　・音量設定
　・所持金
　・フラグ
　・変数
　・フロア
　・現在位置
　・迷宮名　プレイ時間　記録時間
*/
[System.Serializable]
public class SaveData : ISerializationCallbackReceiver, IDisposable
{
    public bool IsNewData = true;

    public SaveInfo saveInfo;
    public SavePosition savePosition;
    public SaveEvent saveEvent;
    public SaveVariableFlag saveVariableFlag;
    public SaveMoney saveMoney;
    public SaveVolume saveVolume;
    public SaveCharacterPossessions saveCharacterPossessions;
    public SaveCharaStatuses saveCharaStatuses;
    public SaveConfig saveConfig;

    public SaveData() { }

    ~SaveData()
    {
        Dispose();
    }

    /// <summary>
    /// セーブする前にセーブするデータを読み込む。
    /// </summary>
    public void SetCaches()
    {
        saveInfo = new SaveInfo();
        savePosition = new SavePosition();
        saveEvent = new SaveEvent();
        saveVariableFlag = new SaveVariableFlag();
        saveMoney = new SaveMoney();
        saveVolume = new SaveVolume();
        saveCharacterPossessions = new SaveCharacterPossessions();
        saveCharaStatuses = new SaveCharaStatuses();
        saveConfig = new SaveConfig();

        saveInfo.SetCaches();
        savePosition.SetCaches();
        saveEvent.SetCaches();
        saveVariableFlag.SetCaches();
        saveMoney.SetCaches();
        saveVolume.SetCaches();
        saveCharacterPossessions.SetCaches();
        saveCharaStatuses.SetCaches();
        saveConfig.SetCaches();

        IsNewData = false;
    }

    /// <summary>
    /// Jsonからクラス化した後に各データを読み込む。
    /// </summary>
    public void LoadCaches()
    {
        saveInfo.SetDataFromCaches();
        savePosition.SetDataFromCaches();
        saveEvent.SetDataFromCaches();
        saveVariableFlag.SetDataFromCaches();
        saveMoney.SetDataFromCaches();
        saveVolume.SetDataFromCaches();
        saveCharacterPossessions.SetDataFromCaches();
        saveCharaStatuses.SetDataFromCaches();
        saveConfig.SetDataFromCaches();

    }

    public void Dispose()
    {
        saveInfo = null;
        savePosition = null;
        saveEvent = null;
        saveVariableFlag = null;
        saveMoney = null;
        saveVolume = null;
        saveCharacterPossessions = null;
        saveCharaStatuses = null;
        saveConfig = null;
    }

    public void OnBeforeSerialize()
    {
        Debug.Log("データセーブ");
    }

    public void OnAfterDeserialize()
    {
        Debug.Log("データロード");
    }
}
