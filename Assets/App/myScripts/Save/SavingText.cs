﻿using System.IO;
using UnityEngine;

public class SavingText<T> : ISavingText<T> where T : new()
{ 
    private const string PATH_SAVE_DATA_FILE = "/save/saveData";
    private const string SAVE_DATA_EXTENTION = ".json";

    public string FilePath { get; set; }

    /// <summary>
    /// ファイルパスと暗号化インスタンスを取得する。
    /// </summary>
    void ISavingText<T>.InitSetting(int idx)
    {
        FilePath = Application.streamingAssetsPath + PATH_SAVE_DATA_FILE + idx.ToString() + SAVE_DATA_EXTENTION;
    }

    /// <summary>
    /// Jsonテキストにセーブする。
    /// </summary>
    /// <param name="saveData">Save data.</param>
    void ISavingText<T>.Save(T saveData)
    {
        var json = JsonUtility.ToJson(saveData);
        File.WriteAllText(FilePath, json);
    }

    /// <summary>
    /// パスからロードしてJsonからクラスに変換して返す。
    /// </summary>
    /// <returns>The aving text< save data>. load.</returns>
    T ISavingText<T>.Load()
    {
        if (!File.Exists(FilePath))
        {
            Debug.Log("初期セーブデータ取得");
            return new T();
        }
        string json = File.ReadAllText(FilePath, System.Text.Encoding.UTF8);
        return JsonUtility.FromJson<T>(json);
    }
}
