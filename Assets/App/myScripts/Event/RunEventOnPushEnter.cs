﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// RunEventOnTriggerとペアで使用する。
/// </summary>
public class RunEventOnPushEnter : MonoBehaviour {
    public bool isRunning = true;
    public static bool isCollide = false;
    private MapEvent mapEvent;
    private PlayerController plycnt;
    private Animator anim;

    [SerializeField]
    private RunEventConditionChacker runEventConditionChacker;

    private const float DELAYTIME = 0.5f;
    private float waitTime;

    private void Start()
    {
        isRunning = true;
        anim = GetComponentInParent<Animator>();
        mapEvent = GetComponentInParent<MapEvent>();
        plycnt = PlayerController.Instance;
    }

    /// <summary>
    /// 有効化無効化設定
    /// </summary>
    /// <param name=""></param>
    public void SetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
    }
    
    private void Update()
    {   
        if (plycnt.isEventAllow && plycnt.isFire1 && Map_EventAnalysisExecution.Instance.IsEventRunning == false && CameraMove.Instance.Move_Arrow == CameraMove.Arrow.NONE && runEventConditionChacker.CheckEventCondition())
        {
            mapEvent.RunEvent();
            // anim.Play("Fadeout", 0);
            EventSign.Instance.Fadeout();
            this.isRunning = false;
            waitTime = Time.time + DELAYTIME;
            SetActive(false);
        }
    }
}
