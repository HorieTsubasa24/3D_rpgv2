﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// ラベルの位置の命令までジャンプする。
/// ラベルが見つからないときは例外が飛ぶ。
/// </summary>
public class EVLabelJump : Instruction
{
    private int _no;
    public override ControlReq Run()
    {
        MapEvent.Label lbl;
        try
        {
            lbl = MapEvent._labels.First(l => l._labelNo == _no);
        }
        catch
        {
            throw new System.Exception("EVLabelJumpで設置ラベルが見つかりませんでした。");
        }
        lbl._doJump = true;
        return ControlReq.LabelJump;
    }

    public EVLabelJump(int[] args)
    {
        _no = args[0];
    }
    
}
