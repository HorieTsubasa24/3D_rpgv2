﻿using UnityEngine;
using Audio;

/// <summary>
/// イベントBGMを再生する。
/// リソースの管理方法に若干困っている。
/// </summary>
public class EVBGM : Instruction
{
    private string clipName;
    public EVBGM(string str, int[] Args)
    {
        // TODO:Argsの構文解析
        //str = str.Replace('\\', '/');
        //var strs = str.Split('.');
        //clip = Resources.Load(strs[0], typeof(AudioClip)) as AudioClip;

        // 例:"bgm\chiisanagakutai.mp3"
        var strs = str.Split('\\');
        clipName = strs[strs.Length - 1].Split('.')[0];
    }

    public override ControlReq Run()
    {
        var clip = AudioManager.Instance.GetAudio(clipName);
        Map_Audio.Instance.AudioPlayEvent(clip);
        return ControlReq.None;
    }

}
