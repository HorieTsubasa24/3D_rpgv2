﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 何もしない命令。
/// </summary>
public class EVNull : Instruction
{
    public override ControlReq Run()
    {
        return ControlReq.None;
    }
}
