﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 立ち絵の対象キャラを再背面に移動。
/// </summary>
public class EVCharaBack : Instruction
{
    private int charaid;
    private GameObject charaView;
    public override ControlReq Run(){
        init();
        var cview = charaView.GetComponent<CharaView>();
        cview.SortToBack(charaid);

        return ControlReq.None;
    }

    public EVCharaBack(int[] args) {
        charaid = args[0];
    }

    private void init() {
        var ob = GameObject.FindGameObjectWithTag("CharaView");
        if (ob == null) // nullでスポーンしてもは機能しない
            charaView = GameObject.Instantiate(ev_pref.evcharaview[0], com_parent);
        else
            charaView = ob;
    }

}
