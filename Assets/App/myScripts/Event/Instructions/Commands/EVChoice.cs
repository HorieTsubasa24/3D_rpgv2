﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 選択肢ウィンドウを表示させ、さらに選択を待って
/// 決定されたら指定のEVBranchジャンプ。
/// </summary>
public class EVChoice : Instruction
{
    [SerializeField]
    private GameObject ob_choice;
    [SerializeField]
    private string[] branchs;
    [SerializeField]
    private ChoiceObject.Mode cancelbr = ChoiceObject.Mode.b1;

    public EVChoice(string str, int[] Args)
    {
        branchs = str.Split('/');
        cancelbr = (ChoiceObject.Mode)Args[0];
    }

    private void init()
    {
        ob_choice = GameObject.Instantiate(ev_pref.evchoiceobject[0], com_parent);
        ob_choice.GetComponent<ChoiceObject>().RunInit(branchs, cancelbr);
    }

    public override ControlReq Run()
    {
        init();
        return ControlReq.Choice;
    }

}
