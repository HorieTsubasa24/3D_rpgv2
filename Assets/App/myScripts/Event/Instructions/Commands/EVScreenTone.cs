﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

/// <summary>
/// 画面の色調を操作する。
/// TODO:画面のフラッシュはこれで代用できる。
/// </summary>
public class EVScreenTone : Instruction
{
    bool iswait;
    float waittime;
    Color color;
    public EVScreenTone(int[] Args)
    {
        // default color = 31, 31, 31, 0
        color = new Color(Args[0] / 31f, Args[1] / 31f, Args[2] / 31f, Args[3] / 31f);
        waittime = Args[4] * 0.1f;
        iswait = (Args[5] == 1);
    }

    // wait対応しましょうか
    public override ControlReq Run()
    {
        var sp = GameObject.Find("BlackMask").GetComponent<Image>();
        sp.DOColor(color, waittime);
        if (iswait)
        {
            MapEvent.SetWaitAndMethod(waittime);
            return ControlReq.Wait;
        }
        else
            return ControlReq.None;
    }

}
