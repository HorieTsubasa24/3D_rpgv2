﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// ラベルを識別子にして命令をジャンプさせることができる。
/// 無限ループにならないようにしたい。
/// </summary>
public class EVLabel : Instruction
{
    private int _no;

    public override ControlReq Run()
    {
        bool _isAlreadyRegist = MapEvent._labels.Any((l) => l._labelNo == _no);
        if (!_isAlreadyRegist)
            MapEvent._labels.Add(new MapEvent.Label(_no));
        return ControlReq.Label;
        // Labelの番号のみ登録しておく
        // private EventCont InstructionOperation(Instruction.ControlReq req)
        // でmapEvent, idxを設定する。
    }

    public EVLabel(int[] args)
    {
        _no = args[0];
    }
    
}
