﻿using UnityEngine;
using SE;

/// <summary>
/// 効果音を再生する。
/// リソースの管理方法に難がある。
/// </summary>
public class EVSE : Instruction
{
    private string clipName;
    public EVSE(string str, int[] Args)
    {
        // TODO:Argsの構文解析
        //str = str.Replace('\\', '/');
        //var strs = str.Split('.');
        //clip = Resources.Load(strs[0], typeof(AudioClip)) as AudioClip;

        // 例:"se\se_old_pack00\step13a.mp3"
        var strs = str.Split('\\');
        clipName = strs[strs.Length - 1].Split('.')[0];
    }

    public override ControlReq Run()
    {
        var clip = SEManager.Instance.GetSE(clipName);
        Map_Sound.Instance.SoundPlay(clip);
        return ControlReq.None;
    }
    
}
