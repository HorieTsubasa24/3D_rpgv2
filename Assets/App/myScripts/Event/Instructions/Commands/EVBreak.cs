﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// イベントの強制終了。
/// 接触型で条件を満たす場合は再度イベントが開始される。
/// </summary>
public class EVBreak : Instruction
{
    public override ControlReq Run()
    {
        return ControlReq.Break;
    }
    
    private void init()
    {
    }

    private void showChara(CharaView cview)
    {
    }

}
