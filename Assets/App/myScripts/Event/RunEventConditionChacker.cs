﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/*
 * 
    // イベントの実行方法
    public enum PlayCond { Investigation = 0, Collision, Auto }
    public PlayCond playCond;

    // 出現条件フラグ
    public bool checkFlag;
    public int flagNo;
    public bool isFlagCondTrue;

    // 出現条件変数
    public bool checkVariable;
    public int valNo;
    public int valLimit;
    public enum Condition { Eq, More, OneOrLess, Greater, Less, Other }
    public Condition condition;

    // 出現条件メンバー
    public bool checkMen;
    public int menNo;

    // 出現フロアとポジション
    public int floor;
    public int posX;
    public int posY;

 * 
 */

/// <summary>
/// イベントが実行可能かを監視し、実行可能な条件になったらEventOnConditionをEnableにする。
/// イベントが実行不可能な条件になったらEventOnConditionをDisableにする。
/// 大分類: フラグ 変数 メンバー
/// </summary>
public class RunEventConditionChacker : MonoBehaviour
{
    private Appear evAppear;
    [SerializeField]
    private GameObject viewObject;

    // 出現条件フラグ
    private bool checkFlag;
    private int flagNo;
    private bool isFlagCondTrue;

    // 出現条件変数
    private bool checkVariable;
    private int valNo;
    private int valLimit;
    private Appear.Condition condition;

    // 出現条件メンバー
    private bool checkMen;
    private int menNo;

    // 出現フロアとポジション
    public int floor;
    public int posX;
    public int posY;

    // Use this for initialization
    void Start()
    {
        evAppear = GetComponent<MapEvent>().appearConditions;

        checkFlag = evAppear.checkFlag;
        flagNo = evAppear.flagNo;
        isFlagCondTrue = evAppear.isFlagCondTrue;

        checkVariable = evAppear.checkVariable;
        valNo = evAppear.valNo;
        valLimit = evAppear.valLimit;
        condition = evAppear.condition;

        checkMen = evAppear.checkMen;
        menNo = evAppear.menNo;

        floor = evAppear.floor;
        posX = evAppear.posX;
        posY = evAppear.posY;
        
        // デモイベント以外
        //if (eventRunner != null)
        //    _nowEnable = eventRunner.enabled;

        // フロア0で不活性化コモンイベント
        if (floor == 0)
        {
            viewObject.SetActive(false);
            this.enabled = false;
        }
    }

    /// <summary>
    /// イベント条件をチェック
    /// </summary>
    /// <returns></returns>
    public bool CheckEventCondition()
    {
        // フロア違い
        if (floor != CommonData.Instance.IntPos.Floor)
        {
            return false;
        }

        // フラグ条件
        if (checkFlag)
        {
            var f = FlagObject.Instance.GetFlag(flagNo);
            if (f != isFlagCondTrue)
                return false;
        }

        // 変数条件
        if (checkVariable)
        {
            var v = VariableObject.Instance.GetVal(valNo);
            switch (condition)
            {
                case Appear.Condition.Eq:
                    if (v == valLimit)
                        break;
                    else
                        return false;
                case Appear.Condition.More:
                    if (v >= valLimit)
                        break;
                    else
                        return false;
                case Appear.Condition.Less:
                    if (v <= valLimit)
                        break;
                    else
                        return false;
                case Appear.Condition.Greater:
                    if (v > valLimit)
                        break;
                    else
                        return false;
                case Appear.Condition.OneOrLess:
                    if (v < valLimit)
                        break;
                    else
                        return false;
                case Appear.Condition.Other:
                    if (v != valLimit)
                        break;
                    else
                        return false;
            }
        }

        // メンバー条件
        if (checkMen)
        {
            var c = PlayerDatas.Instance.p_chara.Any(cd => (int)cd.name == menNo);
            if (c == false)
                return false;
        }

        return true;
    }
    
    void Update()
    {
        bool isTrue = CheckEventCondition();
        if (viewObject.activeSelf != isTrue)
        {
            viewObject.SetActive(isTrue);
        }
    }
}
