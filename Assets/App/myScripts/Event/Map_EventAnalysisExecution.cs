﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// マップのイベントデータの保持。ロード。解析。
/// ソース読み出しはmap(MapTest)の情報によって読み出す。
/// MapToBattleから各イベントを呼び出す。(TODO:MapToBattleのUpdateでオーバーヘッドが多く発生する。)
/// </summary>
[SerializeField]
public class Map_EventAnalysisExecution : Singleton<Map_EventAnalysisExecution>
{
    public static bool _isEndRead;
    public bool IsEventRunning
    {
        get
        {
            foreach (var e in events)
            {
                if (e.isRunning)
                    return true;
            }
            return false;
        }
    }
    /*~----------------------------------/
    :                                    :
    :        外部のクラス                  :
    :                                    :
    /~----------------------------------*/
    // マップ
    public static MapTest map;
    public static MapTest_Legacy l_map;

    // マップイベント命令用のプレハブデータ。
    public static EventInstructionDatas EvPref;

    private const string FIRST_HIERARCHY = "EVH1.dat";
    private const string DEMO_EVENT_FILE = "EV_DEMO.dat";

    /// <summary>
    /// コンポーネント格納先。
    /// </summary>
    [SerializeField] Transform com_parent;
    public static Transform ComParent { get { return instance.com_parent; } private set { instance.com_parent = value; } }

    /// <summary>
    /// overlayコンポーネント格納先。
    /// </summary>
    [SerializeField] Transform com_parent_overlay;
    public static Transform ComParentOverlay { get { return instance.com_parent_overlay; } private set { instance.com_parent_overlay = value; } }

    /// <summary>
    /// マップイベントを操作して実行可能なものを実行する。
    /// </summary>
    /// <returns>イベントがある</returns>
    public bool MapEventExcection()
    {
        if (CommonData.Instance.BeforePosition.XYF == CommonData.Instance.IntPos.XYF)
        {
            return false;
        }

        bool run = false;
        bool showSign = false;
        foreach (var ev in instance.events)
        {
            if (ev == null)
            {
                // Inspector上ではMissingになるので必要あればループ回す前にどこかで要素を削除する
                continue;
            }

            bool sign = false;
            (run, sign) = ev.OnCollision();
            showSign |= sign;
            if (run)
            {
                EventSign.Instance.Fadeout();
                return true;
            }
        }

        if (!showSign)
            EventSign.Instance.Fadeout();
        
        return showSign;
    }

    // イベントごとに格納。(中にイベントコンテンツ)
    [SerializeField]
    public List<MapEvent> events = new List<MapEvent>();

    // イベント元ソース
    [SerializeField]
    public List<string> eventsource = new List<string>();

    private void Awake()
    {
        if (Instance != null)
        {
            gameObject.SetActive(false);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this);
    }

    // ソース読み出し
    private void Start()
    {
        //EvPref = Resources.Load("EventPrefMaster") as EventInstructionDatas;
        EvPref = BundleCaches.LoadEventInstructionDatas();
        ComParent = com_parent;
        ComParentOverlay = com_parent_overlay;
        LoadMapEvents();
        _isEndRead = true;
    }

    /// <summary>
    /// マップイベントを読み込む
    /// </summary>
    public void LoadMapEvents()
    {
        instance.DeleteEvents();
        instance.ReadHierarchyEvent();
        instance.EventConversion();
    }

    /// <summary>
    /// デモイベントを読み込む
    /// </summary>
    public void LoadDemoEvents()
    {
        instance.DeleteEvents();
        instance.ReadDemoEvent();
        instance.EventConversion(true);
    }

    /// <summary>
    /// シーン上のイベントを削除
    /// </summary>
    private void DeleteEvents()
    {
        foreach (var e in events)
        {
            if (e != null && e.gameObject != null)
                Destroy(e.gameObject);
        }
        events.Clear();
    }

    /// <summary>
    /// 階層イベントを読み込む
    /// </summary>
    private void ReadHierarchyEvent()
    {
        if (map == null)
            map = GetComponent<MapTest>();
        if (l_map == null)
            l_map = GetComponent<MapTest_Legacy>();

        if (map != null)
            ReadEventFile(FIRST_HIERARCHY);
        else if (l_map != null)
            ReadEventFile(l_map.GetMap_EventFileName());
    }

    /// <summary>
    /// デモイベントを読み込む
    /// </summary>
    private void ReadDemoEvent()
    {
        ReadEventFile(DEMO_EVENT_FILE);
    }

    /// <summary>
    /// イベントファイルをMapTestから階層、フロアを指定して読み出し。デフォルトで名称不明の迷宮 -> MapTest
    /// </summary>
    public void ReadEventFile(string eventFileName)
    {
        eventsource.Clear();
        string[] evSources = CollectionTool.ReadFile(Application.streamingAssetsPath + "/Events/" + eventFileName);
        eventsource.AddRange(evSources);
    }

    /// <summary>
    /// イベントをコンテンツごとに変換。
    /// </summary>
    private void EventConversion(bool isDemo = false)
    {
        if (eventsource.Count == 0)
            return;

        int numEvent = CollectionTool.GetEventNum(eventsource);

        int i = 0;
        for (i = 1; i <= numEvent; i++)
        {
            EventLoad(i, numEvent, isDemo);
        }
        MapEventExcection();
    }
    
    [SerializeField]
    private GameObject pref_EventObj;
    [SerializeField]
    private Transform trs_event;

    /// <summary>
    /// イベントのロード。
    /// Branchコンポーネントからも呼ぶ。
    /// </summary>
    public void EventLoad(int eventLine, int numEvent, bool isDemo)
    {
        string[] strs = eventsource[eventLine].Split('{');
        string eventName = strs[0];
        string lblname = CollectionTool.RemoveChar(strs[1], '}');
        
        List<string> AppStrs = CollectionTool.GetAppearCondData(numEvent, eventsource.ToArray(), lblname);
        List<string> EvcStrs = CollectionTool.GetEventContentsData(numEvent, eventsource.ToArray(), lblname);
        if (AppStrs == null || EvcStrs == null)
            return;

		try
		{
			// Event Instantiate
			var evob = Instantiate(pref_EventObj, trs_event);
			evob.name = eventName;
			events.Add(evob.GetComponent<MapEvent>());
			events[events.Count - 1].MapEventDataset(int.Parse(lblname.Substring(2, 4)), eventName, AppStrs, EvcStrs);

			if (isDemo)
            {
                evob.GetComponent<RotationToEventSign>().enabled = false;
            }
		}
		catch
		{
			Debug.LogError(eventName);
            throw;
		}
    }
}
