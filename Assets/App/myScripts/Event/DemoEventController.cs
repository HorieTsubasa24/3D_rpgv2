﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// デモイベントの実行コントロール
/// </summary>
public class DemoEventController : Singleton<DemoEventController>
{
    private int eventNo;
    private const int DEMO_VARIABLE_NO = 1001;
    private const string DEMO_SCENE = "DemoEvent_Develop";
    private const string MAP_SCENE = "Map_EventDevelop";
    private const string WORLD_SCENE = "World_Develop";
    private string currentScene;

    [SerializeField]
    private int testRunEventNo;

    private void Awake()
    {
        if (Instance != null)
        {
            gameObject.SetActive(false);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this);
    }

    /// <summary>
    /// テスト実行
    /// </summary>
    [ContextMenu("デモイベントテスト実行")]
    public void RunTestDemo()
    {
        RunDemoEvent(testRunEventNo);
    }

    /// <summary>
    /// デモイベントを実行する
    /// </summary>
    /// <param name="eventNo"></param>
    public void RunDemoEvent(int eventNo)
    {
        GotoDemo(() =>
        {
            var events = FindObjectsOfType<MapEvent>();
            foreach (var e in events)
            {
                int valNo = e.appearConditions.valNo;
                int valValue = e.appearConditions.valLimit;
                if (valNo == DEMO_VARIABLE_NO && valValue == eventNo)
                {
                    e.RunEvent();
                    return;
                }
            }
        });
    }

    /// <summary>
    /// シーン変更処理
    /// </summary>
    /// <param name="sceneName"></param>
    /// <param name="callback"></param>
    /// <returns></returns>
    private IEnumerator ChangeScene(string sceneName, Action callback)
    {
        if (currentScene != sceneName)
        {
            yield return SceneManager.LoadSceneAsync(sceneName);
            currentScene = sceneName;
        }
        callback();
    }

    /// <summary>
    /// デモイベントシーンに移動する
    /// </summary>
    /// <param name="callback"></param>
    private void GotoDemo(Action callback = null)
    {
        StartCoroutine(ChangeScene(DEMO_SCENE, () =>
        {
            Map_EventAnalysisExecution.Instance.LoadDemoEvents();
            MapTest_Legacy.Instance.ResetDatas();

            if (callback != null)
                callback();
        }));
    }

    /// <summary>
    /// マップ画面に戻る
    /// </summary>
    /// <param name="callback"></param>
    private void GotoMap(Action callback = null)
    {
        StartCoroutine(ChangeScene(MAP_SCENE, () =>
        {
            MapTest_Legacy.MapLoad(CommonData.Instance.Hierarchy, CommonData.Instance.Floor);
            Map_EventAnalysisExecution.Instance.LoadMapEvents();
            PlayerController.Map_InputConfigSet();

            if (callback != null)
                callback();
        }));
    }

    /// <summary>
    /// ワールドマップに戻る
    /// TODO:ワールドマップ作成する
    /// </summary>
    /// <param name="callback"></param>
    public void GotoWorld(Action callback = null)
    {
        StartCoroutine(ChangeScene(WORLD_SCENE, () =>
        {
            if (callback != null)
                callback();
        }));
    }
}
