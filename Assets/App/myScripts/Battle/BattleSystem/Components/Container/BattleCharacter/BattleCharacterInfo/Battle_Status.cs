﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace Battle
{
    /// <summary>
    /// 戦闘不能になっても消されない。
    /// HP0以下で判断する。
    /// </summary>
    public class Battle_Status : MonoBehaviour
    {
        /*~----------------------------------/
        :                                    :
        :           メニューバー             :
        :                                    :
        /~----------------------------------*/
        [SerializeField]
        private GameObject ob_Menu;
        //[SerializeField]
        //private Battle_MenuButton Menu;
        //[SerializeField]
        //private ItemBook itemBook;
        //[SerializeField]
        //private SkillBook skillBook;

        [SerializeField]
        private GameObject ob_Enemy;
        [SerializeField]
        private Text Player_Name;


        /*~----------------------------------/
        :                                    :
        :              能力値                :
        :                                    :
        /~----------------------------------*/
        // 全部エディターで入力
        public string CharaName;
        public int Lv;
        public int Hp;
        public int M_Hp;
        public int Mp;
        public int M_Mp;
        // 基礎能力
        public int Str;
        public int Agi;
        public int Int;
        public int Vit;
        public int Luk;
        public int Cri;
        // 武器ステータス
        public int w_Str;
        public int w_Agi;
        public int w_Int;
        public int w_Vit;
        public int w_Luk;
        public int w_Cri;
        public int STR { get { return Str + w_Str; } }
        public int AGI { get { return Agi + w_Agi; } }
        public int INT { get { return Int + w_Int; } }
        public int VIT { get { return Vit + w_Vit; } }
        public int LUK { get { return Luk + w_Luk; } }
        public int CRI { get { return Cri + w_Cri; } }
        public int Fatigue;
        public TableValues.Attribute Atr;

        // 主人公、敵に適用
        public int Exp;
        // アレスタ、敵に適用
        public int G;

        // バトル中のステータス
        public int B_Str;
        public int B_Agi;
        public int B_Int;
        public int B_Vit;
        public int B_Luk;
        public int B_Cri;
        public StatusTable.Abnormality B_Abn;


        /*~----------------------------------/
        :                                    :
        :        その他必要参照等            :
        :                                    :
        /~----------------------------------*/
        /// <summary>
        /// エリナのダミーオブジェクトか
        /// </summary>
        public bool isDammy;

        /// <summary>
        /// 自分がプレイヤーか敵か
        /// </summary>
        public PLYorENM PlyOrEnm;

        public Animator anim;

        public string GroupName;

        /// <summary>
        /// オブジェクトのID
        /// </summary>
        public int ObjectID;

        /// <summary>
        /// 行動優先度
        /// </summary>
        public int B_Priority;

        /// <summary>
        /// 次の行動の種類
        /// </summary>
        public Act NextAct;

        /// <summary>
        /// 選択した魔法やスキル
        /// </summary>
        public int SelectedActNum;

        /// <summary>
        /// 敵、敵全体、プレイヤー、プレイヤー全体、自分自身か
        /// </summary>
        public TableValues.Tgt TargetObject;

        /// <summary>
        /// 選択した敵、プレーヤー
        /// </summary>
        public int SelectedTgtNo;

        /// <summary>
        /// 選択した敵、プレーヤーの名前
        /// </summary>
        public string SelectedTgtName;

        /// <summary>
        /// 選択した敵の情報。バトルでは敵全体を選択し、GetTarget()で対象を取得する。
        /// </summary>
        public List<Battle_Status> EnmStatus;

        /// <summary>
        /// ターンのはじめに選択したスキル
        /// </summary>
        public Skill SelectedSkl;

        /// <summary>
        /// ターンのはじめに選択したアイテム
        /// </summary>
        public Item SelectedItem;

        /// <summary>
        /// ターンでMPを消費したか。消費した後MPを再度消費しないようにする。
        /// </summary>
        public bool UseMagic;

        /// <summary>
        /// スキル、アイテムを使用したときのセリフ
        /// </summary>
        public string[] TalkText = new string[5];

        /// <summary>
        /// ばたんきゅ~後か
        /// </summary>
        public bool PlayedEndMessage;

        /// <summary>
        /// 屍アタックされた回数 アイテムをドロップすると0に戻る
        /// </summary>
        private int SikabaneCount = 0;

        /// <summary>
        /// 行動
        /// </summary>
        public enum Act
        {
            Skill, Item, RunAway, PowerUp, Status, none = 0xff
        }

        /// <summay>
        /// 対象
        /// </summary>
        public enum Target
        {
            Player, Players, Enemy, Enemys, Myself
        }

        public enum PLYorENM
        {
            PLY = 0, ENM = 1
        }

        private const int MAX_RECOVER_ITEMID = 203;

        /// <summary>
        /// マップ上でスキル回復などでエミュレーション用
        /// </summary>
        /// <param name="id"></param>
        public void StatusInit(int id, PLYorENM PlOrEn)
        {
            PlyOrEnm = PlOrEn;
            if (PlyOrEnm == PLYorENM.ENM)
                GroupName = CommonData.Instance.BattleEnemyGroupName;
            else // Player
                GroupName = "こっち";
            ObjectID = id;

            B_Str = STR;
            B_Agi = AGI;
            B_Int = INT;
            B_Vit = VIT;
            B_Luk = LUK;
            B_Cri = CRI;
            B_Abn = StatusTable.Abnormality.Normal;

            Fatigue = 0;

            //Enemy_Name.text = Status.CharaName;
        }

        /// <summary>
        /// ターンごとに呼ばれる処理。毒のダメージなど。
        /// </summary>
        public void Turn()
        {
            UseMagic = false;   // ターンでMPを消費したか。
        }

        /// <summary>
        /// メニュー用。単体選択用。
        /// </summary>
        /// <param name="bs">Bs.</param>
        /// <param name="tgt_bss">Tgt bss.</param>
        /// <param name="idx">Index.</param>
        public void SetEnemyStatus(Battle_Status bs, List<Battle_Status> tgt_bss, int idx)
        {
            bs.EnmStatus.Clear();
            bs.SelectedTgtNo = idx;
            bs.EnmStatus.Add(tgt_bss[idx]);
        }

        /// <summary>
        /// バトル用。単体選択用。単体用でもバトルでは敵全て代入。
        /// </summary>
        /// <param name="bs">Bs.</param>
        /// <param name="tgt_bss">Tgt bss.</param>
        /// <param name="idx">Index.</param>
        public void SetEnemyStatus(Battle_Status bs, List<BattleCharacter> tgt_bss, int idx)
        {
            bs.EnmStatus.Clear();
            bs.SelectedTgtNo = idx;
            for (int i = 0; i < tgt_bss.Count; i++)
                bs.EnmStatus.Add(tgt_bss[i].BattleStatus);
        }



        /// <summary>
        /// 対象のステータスセット全体
        /// </summary>
        public void SetEnemiesStatus(Battle_Status bs, List<BattleCharacter> tgt_bss)
        {
            bs.EnmStatus.Clear();
            bs.SelectedTgtNo = -1;
            for (int i = 0; i < tgt_bss.Count; i++)
                bs.EnmStatus.Add(tgt_bss[i].BattleStatus);
        }

        /// <summary>
        /// 対象のステータスセット全体
        /// </summary>
        public void SetEnemiesStatus(Battle_Status bs, List<Battle_Status> tgt_bss)
        {
            bs.EnmStatus.Clear();
            bs.SelectedTgtNo = -1;
            for (int i = 0; i < tgt_bss.Count; i++)
                bs.EnmStatus.Add(tgt_bss[i]);
        }

        public void SetSkill(Skill skill)
        {
            NextAct = Act.Skill;
            SelectedSkl = skill;
        }


        public void SetItem(Item item)
        {
            NextAct = Act.Item;
            SelectedItem = item;
        }

        /// <summary>
        /// 次に選択された行動により、スキル、アイテムの対応する効果を取得する。
        /// </summary>
        /// <returns>The effects.</returns>
        public IEnumerable<TableValues.Effect> GetEffects()
        {
            switch(NextAct)
            {
                case Act.Item:
                    return SelectedItem.effects;
                case Act.Skill:
                    return SelectedSkl.effects;
                case Act.PowerUp://TODO:とりあえずアイテム用にPowerUpとしておく。
                    return SelectedItem.effects;
                case Act.RunAway:
                    return new TableValues.Effect[] { TableValues.Effect.VITDown };
                case Act.Status:
                case Act.none:
                default:
                    return null;
            }
        }

        /// <summary>
        /// TableValues.Effectによりターゲットがどちら側の陣営かを取得する。
        /// </summary>
        /// <returns>The targets enum.</returns>
        public PLYorENM GetTargetsEnum()
        {
            PLYorENM result;
            var effects = GetEffects().ToArray();
            if (effects == null || effects.Count() == 0)
                result = PLYorENM.PLY;

            var effect = effects[0];
            switch(effect)
            {
                case TableValues.Effect.HPRecover:
                case TableValues.Effect.MPRecover: // アイテム使用後MPの程度を確かめる
                case TableValues.Effect.HPMPRecover:
                case TableValues.Effect.AGIUp:
                case TableValues.Effect.CRIUp:
                case TableValues.Effect.LUCKUp:
                case TableValues.Effect.STRUp:
                case TableValues.Effect.VITUp:
                case TableValues.Effect.Event:
                    result = PLYorENM.PLY;
                    break;
                case TableValues.Effect.Attack: // 選択した(bs)敵へのダメージ
                case TableValues.Effect.AGIDown:
                case TableValues.Effect.CRIDown:
                case TableValues.Effect.LUCKDown:
                case TableValues.Effect.MPDown:
                case TableValues.Effect.STRDown:
                case TableValues.Effect.VITDown:
                    result = PLYorENM.ENM;
                    break;
                default:
                    throw new IndexOutOfRangeException("TableValues.Effectの範囲外です");
            }
            if (PlyOrEnm == PLYorENM.ENM)
            {
                result = (PLYorENM)(((int)result ^ 1) & 1); 
            }
            return result;
        }

        /// <summary>
        /// TableValues.Tgtによりターゲートになる対象を全て取得する。
        /// </summary>
        /// <returns>The targets.</returns>
        public IEnumerable<Battle_Status> GetTargets()
        {
            if (EnmStatus.Count == 0)
                return new List<Battle_Status>();

            if (TargetObject == TableValues.Tgt.Me) {
                return new Battle_Status[] { this };
            }

            if (TargetObject == TableValues.Tgt.Player || TargetObject == TableValues.Tgt.Enemy)
            {
                List<Battle_Status> targets = new List<Battle_Status>();
                targets.Add(EnmStatus[SelectedTgtNo]);
                return targets;
            }
            else
            {
                return EnmStatus;
            }
        }

        /// <summary>
        /// 効果量計算。
        /// </summary>
        /// <returns>The calculate.</returns>
        /// <param name="targetStatus">Target status.</param>
        public int Calculate(Battle_Status targetStatus)
        {
            //Ftaiatari(Battle_Status b_st, Battle_Status b_st_tgt)
            ActMethod.act act = null;
            switch (NextAct)
            {
                case Act.Item:
                    act = SelectedItem.itm_method;
                    break;
                case Act.Skill:
                    act = SelectedSkl.skl_method;
                    break;
                case Act.PowerUp://TODO:とりあえずアイテム用にPowerUpとしておく。
                    act = SelectedItem.itm_method;
                    break;
                case Act.Status:
                case Act.RunAway:
                case Act.none:
                default:
                    break;
            }
            if (act == null)
                return 0;

            return act.Invoke(this, targetStatus);
        }

        /// <summary>
        /// 次の行動から再生するアニメーションの名前を取得する。
        /// </summary>
        /// <returns>The animation name.</returns>
        public string GetAnimationName()
        {
            switch (NextAct)
            {
                case Act.Item:
                    return SelectedItem.ItemName;
                case Act.Skill:
                    return SelectedSkl.SkillName;
                case Act.PowerUp://TODO:とりあえずアイテム用にPowerUpとしておく。
                    return SelectedItem.ItemName;
                case Act.Status:
                case Act.RunAway:
                case Act.none:
                default:
                    return null;
            }
        }

        /// <summary>
        /// 次の行動から再生するエフェクトの名前を取得する。
        /// </summary>
        /// <returns>The animation name.</returns>
        public string GetEffectName()
        {
            switch (NextAct)
            {
                case Act.Item:
                    return SelectedItem.EffectName;
                case Act.Skill:
                    return SelectedSkl.EffectName;
                case Act.PowerUp://TODO:とりあえずアイテム用にPowerUpとしておく。
                    return SelectedItem.EffectName;
                case Act.Status:
                case Act.RunAway:
                case Act.none:
                default:
                    return null;
            }
        }

        /// <summary>
        /// 次の行動から再生するエフェクトのタイプを取得する。
        /// </summary>
        /// <returns>The animation name.</returns>
        public TableValues.ViewType GetEffectType()
        {
            switch (NextAct)
            {
                case Act.Item:
                    return TableValues.ViewType.Single;
                case Act.Skill:
                    return SelectedSkl.ViewType;
                case Act.PowerUp://TODO:とりあえずアイテム用にPowerUpとしておく。
                    return TableValues.ViewType.Single;
                case Act.Status:
                case Act.RunAway:
                case Act.none:
                default:
                    return TableValues.ViewType.Single;
            }
        }

        /// <summary>
        /// CommonDataのアイテムを減らす
        /// </summary>
        public void DecSelectedItem()
        {
            // 消費アイテムでないならreturn
            if (PlyOrEnm == PLYorENM.ENM || SelectedItem.ItemID > MAX_RECOVER_ITEMID)
                return;

            PlayerDatas.Instance.p_chara[ObjectID].DecItem(SelectedItem, 1, true);
            SelectedItem.ItemID = 0;
        }
        
        /// <summary>
        /// 屍アタック回数を加算
        /// </summary>
        public void IncDieAttackCount()
        {
            if (PlayedEndMessage)
                SikabaneCount++;
        }

        /// <summary>
        /// 屍アタック回数をセット
        /// </summary>
        /// <param name="count"></param>
        public void SetDieAttackCount(int count)
        {
            SikabaneCount = count;
        }

        /// <summary>
        /// 屍アタック回数を取得
        /// </summary>
        /// <returns></returns>
        public int GetSikabaneCount()
        {
            if (Hp <= 0)
                return SikabaneCount;
            else
                return -1;
        }

        private void Awake()
        {
            EnmStatus = new List<Battle_Status>();
            anim = GetComponent<Animator>();
        }

    }
}