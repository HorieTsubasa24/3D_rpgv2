﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Battle
{
    /// <summary>
    /// バトル時のプレイヤー
    /// </summary>
    public class PlayerCharacter : BattleCharacter
    {
        // public GameObject BehaviourObject { get; set; }
        // public Battle_Status BattleStatus { get; set; }  
        public RectTransform RectTransformPlayer { get; set; }
        public PlayerActSelecter PlayerActSelecter { get; set; }
        public PlayerCharaData.Name PlayerCharaName { get; set; }
        public Battle_FaceStatus BattleFaceStatus { get; set; }
        public Map_Exp ExpSlider { get; set; }
        
        public PlayerCharacter() : base()
        {
        }
    }
}