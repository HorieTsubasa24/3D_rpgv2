﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Battle
{
    /// <summary>
    /// 廃止。
    /// </summary>
    [System.Serializable]
    public class Battle_PlayerAct : MonoBehaviour
    {
        private CommonData cd;
        /*~----------------------------------/
        :                                    :
        :           外部のクラス             :
        :                                    :
        /~----------------------------------*/
        // テーブル
        private SkillTable skl_table;
        private ItemTable itm_table;

        /*~----------------------------------/
        :                                    :
        :              クラス                :
        :                                    :
        /~----------------------------------*/
        // 乱数
        private static System.Random rand = new System.Random();
        /*~----------------------------------/
        :                                    :
        :         外部から設定する変数       :
        :                                    :
        /~----------------------------------*/
        /// <summary>
        /// キャラの名前
        /// </summary>
        [SerializeField]
        private string Name;

        public string GetName() { return Name; }

        /// <summary>
        /// 何番目のキャラ(1 ~ 3)
        /// </summary>
        [SerializeField]
        private int Number;
        
        /*~----------------------------------------------------------/
        :                                                            :
        :       コモンのテーブルから読み込んで設定設定する変数       :
        :                                                            :
        /~----------------------------------------------------------*/
        /// <summary>
        /// プレイヤーのスキル、最大で10~
        /// SkillNamesでcdObjectのテーブルから読み込む
        /// </summary>
        [SerializeField]
        private List<Skill> skills;
        [SerializeField]
        private List<Item> items;

        private void Start()
        {
            cd = CommonData.Instance;
            skl_table = SkillTable.Instance;
            itm_table = ItemTable.Instance;

            SkillLoadFromCommonData();
            /*
            SkillLoadFromUnityList()
            */
        }
        
        void SkillLoadFromCommonData()
        {
            for (int i = 0; i < PlayerDatas.Instance.p_chara[Number - 1].Ply_skl.Length; i++)
            {
                var skl = PlayerDatas.Instance.p_chara[Number - 1].Ply_skl[i];
                if (skl.skl_method != null) skills.Add(skl);
                else break;
            }
            for (int i = 0; i < PlayerDatas.Instance.p_chara[Number - 1].Ply_item.Length; i++)
            {
                var itm = PlayerDatas.Instance.p_chara[Number - 1].Ply_item[i];
                if (itm.itm_method != null) items.Add(itm);
                else break;
            }
        }

        public List<Item> GetItems()
        {
            return items;
        }

        public void DecItem(Item itm, int n = 1)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].ItemID == itm.ItemID)
                {
                    var buf = items[i];
                    buf.ItemNum = buf.ItemNum - n;
                    if (buf.ItemNum > 0)
                        items[i] = buf;
                    else
                        items.RemoveAt(i);
                }
            }
        }

        public List<Skill> GetSkills()
        {
            return skills;
        }
    }
}
