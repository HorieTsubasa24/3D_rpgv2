﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Battle
{
    /// <summary>
    /// 敵のスキルとスキルの思考ルーチン
    /// ここでスキルを選択する
    /// </summary>
    [System.Serializable]
    public class Battle_EnemyAct : MonoBehaviour
    {
        /*~----------------------------------/
        :                                    :
        :           外部のクラス             :
        :                                    :
        /~----------------------------------*/
        // テーブル
        private SkillTable skl_table;

        /*~----------------------------------/
        :                                    :
        :              クラス                :
        :                                    :
        /~----------------------------------*/
        // 乱数
        private static System.Random rand = new System.Random();
        /*~----------------------------------/
        :                                    :
        :         外部から設定する変数       :
        :                                    :
        /~----------------------------------*/
        /// <summary>
        /// キャラの名前
        /// </summary>
        [SerializeField]
        private ActaiID Name;

        /// <summary>
        /// スキルデータを読み込むための名前(SkillTable.SkillID)
        /// </summary>
        [SerializeField]
        private SkillTable.SkillID[] SkillNames;

        /*~----------------------------------------------------------/
        :                                                            :
        :       コモンのテーブルから読み込んで設定設定する変数       :
        :                                                            :
        /~----------------------------------------------------------*/
        /// <summary>
        /// 敵のスキル、最大で4~5
        /// SkillNamesでcdObjectのテーブルから読み込む
        /// </summary>
        [SerializeField]
        private Skill[] skills;

        /// <summary>
        /// スキルAIの関数、行動ルーチン
        /// </summary>
        [SerializeField]
        private actai skl_ai_method;

        /// <summary>
        /// 実行スキル、実行対象をセットする。初期化の時にロードされたメソッド(敵キャラクターごと)でスキルの選択、対象の選択が行える。
        /// </summary>
        public Battle_Status AIMethodRun(BattleCharacter bs, List<PlayerCharacter> bsplylst, List<EnemyCharacter> bsenmlst, Battle_EnemyAct bea)
        {
            List<BattleCharacter> players = new List<BattleCharacter>(bsplylst);
            List<BattleCharacter> enemies = new List<BattleCharacter>(bsenmlst);
            return skl_ai_method.Invoke(bs, players, enemies, bea);
        }

        // ---------------- 初期化 ---------------------

        /// <summary>
        /// 敵キャラのaiを取得。Start()で呼び出される。
        /// </summary>
        /// <returns></returns>
        static actai GetSkillAiMethod(ActaiID ID)
        {
            return actais[(int)ID];
        }

        /// <summary>
        /// スキルをセッティング。
        /// </summary>
        private void Start()
        {
            skills = new Skill[SkillNames.Length];

            skl_ai_method = GetSkillAiMethod(Name);
            skl_table = SkillTable.Instance;
            for (int i = 0; i < SkillNames.Length; i++)
            {
                skills[i] = skl_table.GetSkillFromEffectName(SkillNames[i]);
            }
        }

        // ----------------------- 敵AI ----------------------------

        /// <summary>
        /// 攻撃のターゲットを選択し、攻撃目標のステータスをEnmStatusに適用して返す。
        /// </summary>
        /// <returns>The act attack.</returns>
        /// <param name="skl">使用するスキル</param>
        /// <param name="b_st">使用者のステータス</param>
        /// <param name="b_st_us">Enemy側のStatuses</param>
        /// <param name="b_st_tgt">Player側のStatuses</param>
        /// <param name="idx">選択するオブジェクトNo.</param>
        public static Battle_Status GetActAttack(Skill skl, BattleCharacter b_st, List<BattleCharacter> b_st_us, List<BattleCharacter> b_st_tgt, int idx)
        {
            b_st.BattleStatus.SelectedSkl = skl;                     // 選択したスキルを適用。あとでInvokeできるようにしている。
            b_st.BattleStatus.NextAct = Battle_Status.Act.Skill;     // スキル行動を適用。
            b_st.BattleStatus.TargetObject = skl.Target;
            return SetEnmStatuses(b_st, b_st_us, b_st_tgt, idx);  // EnmStatusに値を代入する。
        }

        static int GetRandomSkill(IEnumerable<Skill> skills)
        {
            var randvalue = rand.Next(skills.Count());
            return randvalue;
        }

        static int GetRandomTgt(List<BattleCharacter> btlcharas)
        {
            int num = 0;
            foreach (var t in btlcharas)
            {
                if (t.BattleStatus.Hp > 0)
                    num++;
            }
            if (num == 0)
                return -1;

            return GetRandomTgtLoop(btlcharas);
        }
        static int GetRandomTgtLoop(List<BattleCharacter> btlcharas)
        {
            var randvalue = rand.Next(btlcharas.Count);
            if (btlcharas[randvalue].BattleStatus.Hp <= 0)
                return GetRandomTgtLoop(btlcharas);

            return randvalue;
        }

        /// <summary>
        /// 攻撃目標のステータスをEnmStatusに追加していく。
        /// </summary>
        static Battle_Status SetEnmStatuses(BattleCharacter bs, List<BattleCharacter> us_bs, List<BattleCharacter> tgt_bs, int idx)
        {
            var status = bs.BattleStatus;
            if (idx == -1)
            {
                status.EnmStatus.Clear();
                return status;
            }

            if (status.TargetObject == TableValues.Tgt.Me)
                status.SetEnemyStatus(status, us_bs, status.ObjectID);
            else if (status.TargetObject == TableValues.Tgt.Player)
                status.SetEnemyStatus(status, tgt_bs, idx);
            else if (status.TargetObject == TableValues.Tgt.PlayerAll)
                status.SetEnemiesStatus(status, tgt_bs);
            else if (status.TargetObject == TableValues.Tgt.Enemy)
                status.SetEnemyStatus(status, us_bs, idx);
            else //if (bs.TargetObject == TableValues.Tgt.EnemyAll)
                status.SetEnemiesStatus(status, us_bs);

            return status;
        }

        /// <summary>
        /// 敵キャラの行動関数
        /// </summary>
        public delegate Battle_Status actai(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> players, Battle_EnemyAct skl);

        /// <summary>
        /// 敵キャラのID
        /// </summary>
        public enum ActaiID
        {
            Wolf = 0, FlowerSlime, Yashigani, Harpy, Alraune, Mashmellz, Dragonbaby, Dragonkids, DogGateKeeper,
            Automata, Dragon, Yale, DarkElf, Sphinx, StoneGolem, BlossomSlime, IronGolem, Dullahan, Mimic1, Mimic2, Mimic3,
            Fary, Frat
        }

        // デリゲートメソッド
        static actai[] actais = {
            Wolf, FlowerSlime, Yashigani, Harpy, Alraune, Mashmellz, Dragonbaby, Dragonkids, DogGateKeeper,
            Automata, Dragon, Yale, DarkElf, Sphinx, StoneGolem, BlossomSlime, IronGolem, Dullahan, Mimic1, Mimic2, Mimic3,
            Fary, Frat
        };

        /// <summary>
        /// おおかみおとこ
        /// </summary>
        /// <returns></returns>
        public static Battle_Status Wolf(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            print("Wolf");
            var n = GetRandomTgt(enemies); //rand.Next(enemies.Count);
            var w = GetRandomSkill(skl.skills); //rand.Next(skl.skills.Length);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// フラワースライム
        /// </summary>
        /// <returns></returns>
        public static Battle_Status FlowerSlime(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// やしがにん
        /// </summary>
        /// <returns></returns>
        public static Battle_Status Yashigani(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// ハーピー
        /// </summary>
        /// <returns></returns>
        public static Battle_Status Harpy(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// アルラウネ
        /// </summary>
        /// <returns></returns>
        public static Battle_Status Alraune(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// マッシュメルツ
        /// </summary>
        /// <returns></returns>
        public static Battle_Status Mashmellz(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// ドラゴンベイビー
        /// </summary>
        /// <returns></returns>
        public static Battle_Status Dragonbaby(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// ドラゴンキッズ
        /// </summary>
        /// <returns></returns>
        public static Battle_Status Dragonkids(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// 犬門番
        /// </summary>
        /// <returns></returns>
        public static Battle_Status DogGateKeeper(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// オートマタ
        /// </summary>
        /// <returns></returns>
        public static Battle_Status Automata(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// ドラゴン
        /// </summary>
        /// <returns></returns>
        public static Battle_Status Dragon(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// エアレー
        /// </summary>
        /// <returns></returns>
        public static Battle_Status Yale(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// ダークエルフ
        /// </summary>
        /// <returns></returns>
        public static Battle_Status DarkElf(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// スフィンクス
        /// </summary>
        /// <returns></returns>
        public static Battle_Status Sphinx(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// スフィンクス
        /// </summary>
        /// <returns></returns>
        public static Battle_Status StoneGolem(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// ブロッサムスライム
        /// </summary>
        /// <returns></returns>
        public static Battle_Status BlossomSlime(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// アイアンゴーレム
        /// </summary>
        /// <returns></returns>
        public static Battle_Status IronGolem(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// デュラハン
        /// </summary>
        /// <returns></returns>
        public static Battle_Status Dullahan(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// ボロボロミミック
        /// </summary>
        /// <returns></returns>
        public static Battle_Status Mimic1(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// そこそこミミック
        /// </summary>
        /// <returns></returns>
        public static Battle_Status Mimic2(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// PIKAPIKAミミック
        /// </summary>
        /// <returns></returns>
        public static Battle_Status Mimic3(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }


        /// <summary>
        /// フェアリー
        /// </summary>
        /// <returns></returns>
        public static Battle_Status Fary(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

        /// <summary>
        /// フレイムラット
        /// </summary>
        /// <returns></returns>
        public static Battle_Status Frat(BattleCharacter enemy, List<BattleCharacter> enemies, List<BattleCharacter> us,Battle_EnemyAct skl)
        {
            var n = GetRandomTgt(enemies);
            var w = GetRandomSkill(skl.skills);
            enemy.BattleStatus = GetActAttack(skl.skills[w], enemy, enemies, us, n);

            return enemy.BattleStatus;
        }

    }
}