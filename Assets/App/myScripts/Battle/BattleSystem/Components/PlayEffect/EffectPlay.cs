﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EffectPlay : EffekseerEmitter
{
    public Camera _useCamera;

    public abstract void CalculatePos();
    public abstract EffekseerEmitter PlayEffect(string effectName);
}
