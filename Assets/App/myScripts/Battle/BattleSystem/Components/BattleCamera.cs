﻿using System;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

namespace Battle
{
    public class BattleCamera
    {
        public Camera _camera;

        public BattleCamera(Camera cam)
        {
            _camera = cam;
        }

        // ----------------- モード -----------------------

        private readonly int INITSIZE = 4;
        public bool IsZooming { get; private set; } = false;
        private readonly int DEFAULTSIZE = 10;
        public bool IsIdling { get; private set; } = false;

        // ----------------- method ----------------------

        public enum Phese { BTLINIT, IDLE }
        public void SetOrthographicSize(Phese phese)
        {
            switch(phese)
            {
                case Phese.BTLINIT:
                    _camera.orthographicSize = INITSIZE;
                    break;
                case Phese.IDLE:
                    _camera.orthographicSize = DEFAULTSIZE;
                    break;
            }
        }

        public void AppearShader(MeshRenderer noiseMeshMap, MeshRenderer noiseMeshBattle, Image noiseMeshCanvas)
        {
            noiseMeshCanvas.gameObject.SetActive(true);
            noiseMeshCanvas.material.SetFloat("_DistortionPower", 2.6f);
            DOVirtual.DelayedCall(0.2f, () =>
            {
                noiseMeshMap.material.DOFloat(0, "_DistortionPower", 0.2f).OnComplete(() => noiseMeshMap.gameObject.SetActive(false));
                noiseMeshBattle.material.DOFloat(0, "_DistortionPower", 0.2f).OnComplete(() => noiseMeshBattle.gameObject.SetActive(false));
                noiseMeshCanvas.material.DOFloat(0, "_DistortionPower", 0.2f).OnComplete(() => noiseMeshCanvas.gameObject.SetActive(false));
            });
        }

        public IEnumerator Zooming(float size, float wait)
        {
            if (IsZooming)
                yield break;

            IsZooming = true;
            var mag = 2.0f;
            var timediff = (Time.deltaTime / wait);
            var diff = (size - _camera.orthographicSize) * timediff;
            var startTime = Time.time;
            while (startTime + wait > Time.time)
            {
                _camera.orthographicSize += diff * mag;
                mag -= timediff * 2;
                yield return 0;
            }
            _camera.orthographicSize = size;

            IsZooming = false;
        }

        public IEnumerator Idling(float m = -1)
        {
            if (IsIdling)
                yield break;

            float magnification;
            if (m <= 0)
                magnification = DEFAULTSIZE;
            else
                magnification = m;

            IsIdling = true;
            _camera.DOOrthoSize(magnification, 4.0f);
            IsIdling = false;
        }
    }
}
