﻿using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    /// <summary>
    /// カメラなどどうしようもないメモリに置いときたいオブジェクト用
    /// </summary>
    public partial class BattleMain
    {
        public List<GameObject> SpawnObjects { get; private set; } = new List<GameObject>();
        public Camera UseCamera { get; private set; }
        public RectTransform PlayerSpawnDir { get { return _playerSpawnDir; } }
        [SerializeField] Battle_SkillTray _skillTray;
        [SerializeField] Battle_ItemTray _itemTray;
        [SerializeField] RunAwayTray _runAwayTray;
        public Battle_SkillTray SkillTray { get; private set; }
        public Battle_ItemTray ItemTray { get; private set; }
        public RunAwayTray RunAwayTray { get; private set; }

        // ----------- Tray -----------------

        private void SetBattleRefObject()
        {
            SkillTray = _skillTray;
            ItemTray = _itemTray;
            RunAwayTray = _runAwayTray;
            RunAwayTray._players = _playerCharacter;
        }

        // ------------ GameObject ------------

        public void RegistDeleteObject(GameObject obj)
        {
            SpawnObjects.Add(obj);
        }

        public void DeleteRegistedObject()
        {
            foreach (var o in SpawnObjects)
            {
                if (o != null)
                    Destroy(o, 1.0f);
            }
            SpawnObjects.Clear();
        }

        // ------------ --------------

        public void SetBattleCamera(Camera cam)
        {
            UseCamera = cam;
        }
    }
}
