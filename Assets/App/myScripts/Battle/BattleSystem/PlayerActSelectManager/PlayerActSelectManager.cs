﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Threading.Tasks;
using System.Linq;

namespace Battle {
    /// <summary>
    /// キャラ選択の進み戻りを管理する。
    /// </summary>
    public class PlayerActSelectManager : MonoBehaviour, IBattleEndProcessableObject
    {
        // --------------- IBattleEndProcessableObject ------------

        public void EndProcessing()
        {

        }

        // --------------------------------------------------------

        PlayerCharacter[] _players;
        [SerializeField] PlayerActSelecter[] _selecters;
        public bool _isWakeup;
        public int _idx;

        public void Init(IEnumerable<PlayerCharacter> players, IEnumerable<PlayerActSelecter> selecters)
        {
            _players = players.ToArray();
            _selecters = selecters.ToArray();
        }

        /// <summary>
        /// プレイヤー操作 選択開始処理
        /// </summary>
        /// <returns></returns>
        public async Task<bool> WakeupSelecters()
        {
            _isWakeup = true;
            for (int i = 1; i < _selecters.Length; i++)
            {
                var ii = i;
                _selecters[i].Init(() => ToCancel(ii));
            }
            await _selecters[0].Init(() => ToCancel(0));
            _selecters[0].StartSelecting();

            _idx = 0;
            return true;
        }

        private void SelecterEnd()
        {
            _isWakeup = false;
            _idx = 0;
        }

        private int GetCanActChara(int nowidx)
        {
            if (nowidx == 0)
                return 0;

            for (int i = nowidx - 1; i >= 0; i--) {
                if (_players[i].BattleStatus.Hp > 0)
                    return i;
            }

            return nowidx;
        }

        private async void ToCancel(int idx)
        {
            if (idx == 0)
            {
                // 0番目のキャラ(あれすた)の選択を開始する。
                await _selecters[0].Init(() => ToCancel(0));
                _selecters[idx].StartSelecting();
                return;
            }
            
            _idx = GetCanActChara(idx);
            int ii = _idx;
            await _selecters[ii].Init(() => ToCancel(ii));
            _selecters[_idx].StartSelecting(); // 一つ前のキャラクターの選択を開始する。
        }

        // ------------------- update -----------------------

        /// <summary>
        /// 選択されているセレクタが選択完了なら次のセレクタを選択する。
        /// </summary>
        private async Task<bool> IncIdx()
        {
            // 戦闘不能を飛ばす
            if (_players[_idx].BattleStatus.Hp <= 0)
            {
                _players[_idx].BattleStatus.NextAct = Battle_Status.Act.none;
                _selecters[_idx]._isDecided = true;
                await IncIdx();
                return true;
            }

            if (_selecters[_idx]._isDecided == true)
            {
                _idx++;
                if (_idx >= _selecters.Length)
                {
                    SelecterEnd();              // 選択完了。
                    return true;
                }
                // 選択されているセレクタをスタートさせる。
                int ii = _idx;
                await _selecters[ii].Init(() => ToCancel(ii));
                _selecters[ii].StartSelecting();
            }
            return true;
        }

        // Update is called once per frame
        async void Update() {
            if (!_isWakeup)
                return;

            await IncIdx();
        }
    }
}