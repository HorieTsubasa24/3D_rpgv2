﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Battle
{
    public class StatusLoader
    {
        Random rand;
        public void Init()
        {
            rand = new Random((int)UnityEngine.Time.time);
        }


        public StatusLoader()
        {
            Init();
        }

        public StatusLoader(IEnumerable<PlayerCharacter> playerCharacters)
        {
            Init();

            int idx = 0;
            foreach (var c in playerCharacters)
            {
                CommonData.Instance.SetCommonStatusToBattlePlayer(c.BattleStatus, idx);
                idx++;
            }
            InitBattleStatusParams(playerCharacters);
        }

        /// <summary>
        /// 読み込んだバトル能力(味方)、または設定されているバトル能力(敵)の設定
        /// </summary>
        /// <param name="battlecharacters">Battlecharacters.</param>
        public void InitBattleStatusParams(IEnumerable<BattleCharacter> battlecharacters)
        {
            foreach (var c in battlecharacters)
            {
                c.BattleStatus.B_Abn = StatusTable.Abnormality.Normal;
                c.BattleStatus.B_Agi = c.BattleStatus.AGI;
                c.BattleStatus.B_Cri = c.BattleStatus.CRI;
                c.BattleStatus.B_Int = c.BattleStatus.INT;
                c.BattleStatus.B_Luk = c.BattleStatus.LUK;
                c.BattleStatus.B_Str = c.BattleStatus.STR;
                c.BattleStatus.B_Vit = c.BattleStatus.VIT;
                c.BattleStatus.B_Priority = rand.Next(0, 11); // 行動優先度はランダム
            }
        }
    }
}