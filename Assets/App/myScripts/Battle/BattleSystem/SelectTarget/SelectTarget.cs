﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

namespace Battle {
    public abstract class SelectTarget : UITray {
        // ------------- Field -----------------

        protected List<Battle_Status> _playerStatuses;
        protected List<Battle_Status> _enemyStatuses;

        // ------------- 初期化 -----------------

        public void Init(IEnumerable<PlayerCharacter> players, IEnumerable<EnemyCharacter> enemys)
        {
            _playerStatuses = players.Select(pl => pl.BattleStatus).ToList();
            _enemyStatuses = enemys.Select(en => en.BattleStatus).ToList();
        }

        // ------------- Animation ------------------
        // ----------- private -----------

        private void AllLeaveAnime()
        {
            foreach (var flame in _objectFlame)
            {
                flame._anim.Play("Hide");
            }
        }

        private void AllCursorAnime()
        {
            int idx = 0;
            foreach (var flame in _objectFlame)
            {
                var bs = _character[idx].BattleStatus;
                if (bs.Hp > 0 || bs.PlyOrEnm == Battle_Status.PLYorENM.PLY)
                    flame._anim.Play("View");

                idx++;
            }
        }

        // ----------- public ------------

        [SerializeField] protected int _beforeIdx;
        public override void LeaveAnime(int befidx)
        {
            if (_target == TableValues.Tgt.EnemyAll || _target == TableValues.Tgt.PlayerAll)
            {
                AllLeaveAnime();
                return;
            }
            _objectFlame[befidx]._anim.Play("Hide");
        }

        public override void CursorAnime(int nowidx)
        {
            if (_target == TableValues.Tgt.EnemyAll || _target == TableValues.Tgt.PlayerAll)
            {
                AllCursorAnime();
                return;
            }
            var bs = _character[nowidx].BattleStatus;
            if (bs.Hp > 0 || bs.PlyOrEnm == Battle_Status.PLYorENM.PLY)
                _objectFlame[nowidx]._anim.Play("View");
        }

        public void AnimationClose()
        {
            foreach (var flame in _objectFlame)
            {
                flame._anim.Play("Hide");
            }
        }

        // -----------------------------------------

        public Transform _spawnCursorFlame;
        protected Battle_Status _callerChara;
        protected List<BattleCharacter> _character;
        protected PlayerController _plycnt;
        protected TableValues.Tgt _target;

        /// <summary>
        /// キャラの登録。
        /// 子オブジェクトに各キャラクターの座標に対応する
        /// カーソルアニメをするオブジェクトを配置する。
        /// </summary>
        /// <param name="charas">Players.</param>
        public abstract void InitCursorObject(IEnumerable<BattleCharacter> charas);

        /// <summary>
        /// 実際にキャラを選択する前に呼ばれる。
        /// </summary>
        /// <param name="callerChara">Caller chara.</param>
        public void Wakeup(Battle_Status callerChara, TableValues.Tgt target)
        {
            _plycnt = PlayerController.Instance;
            _target = target;
            _callerChara = callerChara;
            _nowFlameCursor = _callerChara.ObjectID;
            CursorAnime(0);
        }

        // -------------- 対象の登録 -----------------

        public virtual void RegistTarget()
        {
            _callerChara.TargetObject = _target;            // 自分、プレイヤー、プレイヤー全員、敵、敵全体
            _callerChara.SelectedTgtNo = _nowFlameCursor;   // プレイヤー、敵の時の選択ID
        }

        // -------------- CursorMove ---------------

        public void CursorMoveSelect(Vector2 input, int numOfline = 1, bool isSound = true)
        {
            if (_target == TableValues.Tgt.Me || _target == TableValues.Tgt.EnemyAll || _target == TableValues.Tgt.PlayerAll)
            {
                return;
            }

            if (input.x == 0 && input.y == 0)
                return;

            int befidx = _nowFlameCursor;
            _beforeIdx = befidx;

            bool ischange = false;
            if (input.y > 0 || input.x < 0)
            {
                ChangeIdxFromCursorMove(-numOfline);
                ischange = true;
            }
            if (input.y < 0 || input.x > 0)
            {
                ChangeIdxFromCursorMove(numOfline);
                ischange = true;
            }

            if (_nowFlameCursor >= _numFlame)
            {
                _nowFlameCursor = _nowFlameCursor % numOfline;
                _beforeIdx = -99;
            }
            if (_nowFlameCursor < 0)
            {
                _nowFlameCursor = (_numFlame + _nowFlameCursor) % _numFlame;
                _beforeIdx = 99;
            }

            if (ischange)
            {
                if (befidx != _nowFlameCursor)
                    LeaveAnime(befidx);
                CursorAnime(_nowFlameCursor);
                if (isSound)
                    Map_Sound.Instance.CursorMoveSoundPlay();
            }
        }


        // -------------- GotoTray -----------------

        public override void GotoNextTray(bool isgoto, Action act = null)
        {
            if (!isgoto)
                return;

            Map_Sound.Instance.DecisionSoundPlay();
            _closeAction.Invoke();
            EndProcessManual();
            AnimationClose();
            RegistTarget();
        }

        public override void GotoBackTray(bool isgoto)
        {
            if (!isgoto)
                return;

            if (_parent.gameObject.activeSelf == false)
                _parent.gameObject.SetActive(true);

            Map_Sound.Instance.CancelSoundPlay();
            EndProcessManual(); // キャンセル時はPlayerActSelecterの（）=> EndSelecting()を実行しない。
            AnimationClose();
        }
    }
}