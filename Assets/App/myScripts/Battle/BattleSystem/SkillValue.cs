﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle {
    public abstract class SkillValue {
        public TableValues.Effect _skillEffect;
        public int _calculatedAmount;
        public readonly int MAXVALUE = 500;

        public SkillValue(TableValues.Effect skillEffect) 
        {
            _skillEffect = skillEffect;
        }

        public void SetCalculatedAmount(int v)
        {
            _calculatedAmount = v;
        }

        public abstract void RunSkillActionToTgt(Battle_Status targetStatus);

        public static SkillValue CreateSkillValue(TableValues.Effect efc)
        {
            switch(efc)
            {
                case TableValues.Effect.HPRecover:
                    return new HPRecover(efc);
                case TableValues.Effect.MPRecover:
                    return new MPRecover(efc);
                case TableValues.Effect.Attack:
                    return new Attack(efc);
                case TableValues.Effect.MPDown:
                    return new MpDown(efc);
                case TableValues.Effect.STRUp:
                    return new STRUp(efc);
                case TableValues.Effect.AGIUp:
                    return new AGIUp(efc);
                case TableValues.Effect.VITUp:
                    return new VITUp(efc);
                case TableValues.Effect.LUCKUp:
                    return new LUCKUp(efc);
                case TableValues.Effect.CRIUp:
                    return new CRIUp(efc);
                case TableValues.Effect.STRDown:
                    return new STRDown(efc);
                case TableValues.Effect.AGIDown:
                    return new AGIDown(efc);
                case TableValues.Effect.VITDown:
                    return new VITDown(efc);
                case TableValues.Effect.LUCKDown:
                    return new LUCKDown(efc);
                case TableValues.Effect.CRIDown:
                    return new CRIDown(efc);
                case TableValues.Effect.Event:
                    return new EventSkillValue(efc, null);
                default:
                    throw new System.IndexOutOfRangeException("SkillValueのEffectがないよ！");
            }
        }
    }
    // ------- 全効果 -------
    /*
    public enum Effect
    {
        HPRecover, MPRecover, HPMPRecover,
        Attack, MPDown,
        STRUp, STRDown,
        AGIUp, AGIDown,
        INIUp, INIDown,
        VITUp, VITDown,
        LUCKUp, LUCKDown,
        CRIUp, CRIDown,
        Event,
    }
    */
    public class HPRecover : SkillValue
    {
        public override void RunSkillActionToTgt(Battle_Status targetStatus)
        {
            targetStatus.Hp = Mathf.Clamp(targetStatus.Hp + _calculatedAmount, 0, targetStatus.M_Hp);
        }
        public HPRecover(TableValues.Effect skillEffect) : base(skillEffect) { }
    }

    public class MPRecover : SkillValue
    {
        public override void RunSkillActionToTgt(Battle_Status targetStatus)
        {
            targetStatus.Mp = Mathf.Clamp(targetStatus.Mp + _calculatedAmount, 0, targetStatus.M_Mp);
        }
        public MPRecover(TableValues.Effect skillEffect) : base(skillEffect) { }
    }

    public class HPMPRecover : SkillValue
    {
        public override void RunSkillActionToTgt(Battle_Status targetStatus)
        {
            targetStatus.Hp = Mathf.Clamp(targetStatus.Hp + _calculatedAmount, 0, targetStatus.M_Hp);
            targetStatus.Mp = Mathf.Clamp(targetStatus.Mp + _calculatedAmount, 0, targetStatus.M_Hp);
        }
        public HPMPRecover(TableValues.Effect skillEffect) : base(skillEffect) { }
    }

    public class Attack : SkillValue
    {
        public override void RunSkillActionToTgt(Battle_Status targetStatus)
        {
            targetStatus.Hp = Mathf.Clamp(targetStatus.Hp - _calculatedAmount, 0, targetStatus.M_Hp);
        }
        public Attack(TableValues.Effect skillEffect) : base(skillEffect) { }
    }

    public class MpDown : SkillValue
    {
        public override void RunSkillActionToTgt(Battle_Status targetStatus)
        {
            targetStatus.Mp = Mathf.Clamp(targetStatus.Mp - _calculatedAmount, 0, targetStatus.M_Mp);
        }
        public MpDown(TableValues.Effect skillEffect) : base(skillEffect) { }
    }

    public class STRUp : SkillValue
    {
        public override void RunSkillActionToTgt(Battle_Status targetStatus)
        {
            targetStatus.B_Str = Mathf.Clamp(targetStatus.B_Str + _calculatedAmount, 0, MAXVALUE);
        }
        public STRUp(TableValues.Effect skillEffect) : base(skillEffect) { }
    }

    public class STRDown : SkillValue
    {
        public override void RunSkillActionToTgt(Battle_Status targetStatus)
        {
            targetStatus.B_Str = Mathf.Clamp(targetStatus.B_Str - _calculatedAmount, 0, MAXVALUE);
        }
        public STRDown(TableValues.Effect skillEffect) : base(skillEffect) { }
    }

    public class AGIUp : SkillValue
    {
        public override void RunSkillActionToTgt(Battle_Status targetStatus)
        {
            targetStatus.B_Agi = Mathf.Clamp(targetStatus.B_Agi + _calculatedAmount, 0, MAXVALUE);
        }
        public AGIUp(TableValues.Effect skillEffect) : base(skillEffect) { }
    }

    public class AGIDown : SkillValue
    {
        public override void RunSkillActionToTgt(Battle_Status targetStatus)
        {
            targetStatus.B_Agi = Mathf.Clamp(targetStatus.B_Agi - _calculatedAmount, 0, MAXVALUE);
        }
        public AGIDown(TableValues.Effect skillEffect) : base(skillEffect) { }
    }

    public class INIUp : SkillValue
    {
        public override void RunSkillActionToTgt(Battle_Status targetStatus)
        {
            targetStatus.B_Int = Mathf.Clamp(targetStatus.B_Int + _calculatedAmount, 0, MAXVALUE);
        }
        public INIUp(TableValues.Effect skillEffect) : base(skillEffect) { }
    }

    public class INIDown : SkillValue
    {
        public override void RunSkillActionToTgt(Battle_Status targetStatus)
        {
            targetStatus.B_Int = Mathf.Clamp(targetStatus.B_Int - _calculatedAmount, 0, MAXVALUE);
        }
        public INIDown(TableValues.Effect skillEffect) : base(skillEffect) { }
    }

    public class VITUp : SkillValue
    {
        public override void RunSkillActionToTgt(Battle_Status targetStatus)
        {
            targetStatus.B_Vit = Mathf.Clamp(targetStatus.B_Vit + _calculatedAmount, 0, MAXVALUE);
        }
        public VITUp(TableValues.Effect skillEffect) : base(skillEffect) { }
    }

    public class VITDown : SkillValue
    {
        public override void RunSkillActionToTgt(Battle_Status targetStatus)
        {
            targetStatus.B_Vit = Mathf.Clamp(targetStatus.B_Vit - _calculatedAmount, 0, MAXVALUE);
        }
        public VITDown(TableValues.Effect skillEffect) : base(skillEffect) { }
    }

    public class LUCKUp : SkillValue
    {
        public override void RunSkillActionToTgt(Battle_Status targetStatus)
        {
            targetStatus.B_Luk = Mathf.Clamp(targetStatus.B_Luk + _calculatedAmount, 0, MAXVALUE);
        }
        public LUCKUp(TableValues.Effect skillEffect) : base(skillEffect) { }
    }

    public class LUCKDown : SkillValue
    {
        public override void RunSkillActionToTgt(Battle_Status targetStatus)
        {
            targetStatus.B_Luk = Mathf.Clamp(targetStatus.B_Luk - _calculatedAmount, 0, MAXVALUE);
        }
        public LUCKDown(TableValues.Effect skillEffect) : base(skillEffect) { }
    }

    public class CRIUp : SkillValue
    {
        public override void RunSkillActionToTgt(Battle_Status targetStatus)
        {
            targetStatus.B_Cri = Mathf.Clamp(targetStatus.B_Cri + _calculatedAmount, 0, MAXVALUE);
        }
        public CRIUp(TableValues.Effect skillEffect) : base(skillEffect) { }
    }

    public class CRIDown : SkillValue
    {
        public override void RunSkillActionToTgt(Battle_Status targetStatus)
        {
            targetStatus.B_Cri = Mathf.Clamp(targetStatus.B_Cri - _calculatedAmount, 0, MAXVALUE);
        }
        public CRIDown(TableValues.Effect skillEffect) : base(skillEffect) { }
    }

    public class EventSkillValue : SkillValue
    {
        public System.Action _act;
        public override void RunSkillActionToTgt(Battle_Status targetStatus)
        {
            if (_act != null)
                _act.Invoke();
            else
                Debug.LogError("スキルのメソッドがありません。 EventSkillValue");
            return;
        }
        public EventSkillValue(TableValues.Effect skillEffect, System.Action act) : base(skillEffect) { }
    }
}