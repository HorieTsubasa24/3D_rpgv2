﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    /// <summary>
    /// バトル中の音楽の再生。
    /// </summary>
    public class BattleMusic : MonoBehaviour, IAudioSaveLoad
    {
        static BattleMusic instance;
        public static BattleMusic Instance
        {
            get { if (instance == null) instance = FindObjectOfType<BattleMusic>(); return instance; }
        }
        public int MusicNo;
        AudioSource _audioSource;
        public AudioClip[] _audios;
        public AudioClip battle1;
        public AudioClip win;
        public AudioClip lose;

        private void OnEnable()
        {
            instance = this;
            _audioSource = GetComponent<AudioSource>();
            _audioSource.loop = true;
        }

        public static void SetVolume(float v)
        {
            Instance._audioSource.volume = v * baseVolume;
        }

        public static void PlayAudio(int mno)
        {
            Instance.MusicNo = mno;
            Instance._audioSource.clip = Instance._audios[Instance.MusicNo];
            Instance._audioSource.Play();
        }

        public static void PlayAudio(AudioClip audio)
        {
            Instance._audioSource.clip = audio;
            Instance._audioSource.Play();
        }

        public static void PlayBattle1()
        {
            SetVolume(1.0f);
            BattleMusic.PlayAudio(Instance.battle1);
        }

        public static void PlayWin()
        {
            SetVolume(1.1f);
            BattleMusic.PlayAudio(Instance.win);
        }

        public static void PlayLose()
        {
            SetVolume(1.05f);
            BattleMusic.PlayAudio(Instance.lose);
        }

        // --------------- セーブロード -------------------

        static float baseVolume;
        /// <summary>
        /// コンフィグ変更時、ロード時、ボリュームを変更。
        /// </summary>
        /// <param name="volume">Volume.</param>
        public void SetVolume(int volume)
        {
            baseVolume = volume * 0.01f;
        }

        /// <summary>
        /// セーブ時に現在のボリューム値を取得。
        /// </summary>
        /// <returns>The volume cache.</returns>
        public int GetVolumeCache()
        {
            return (int)(baseVolume * 100);
        }
    }
}
