﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle { 
    public class RunAwayTray : UITray
    {
        // --------------------- override --------------------------

        public override void LeaveAnime(int befidx)
        {
        }

        public override void CursorAnime(int nowidx)
        {
        }

        /// <summary>
        /// 逃げるセット。
        /// </summary>
        public override void OnOpenTray()
        {
            // Battle_UseBookActから戻る時に有効化
            if (gameObject.activeSelf == false)
            {
                gameObject.SetActive(true);
            }
        }

        // ---------------------------------------------------------

        public List<PlayerCharacter> _players;
        public PlayerActSelectManager _playerActManager;
        public void Update()
        {
            _players[_playerActManager._idx].BattleStatus.NextAct = Battle_Status.Act.RunAway;
            CloseTray();
        }
    }
}
