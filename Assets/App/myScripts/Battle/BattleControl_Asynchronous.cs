﻿//using System.Collections;
//using System.Collections.Generic;
//using System;
//using System.Threading.Tasks;

///// <summary>
///// バトル中別スレッドで行われる処理
///// </summary>
//public partial class BattleControl
//{
//    private void WaitPheseWait(float t, ST st, string mtd = null)
//    {
//        Task.Run(() => t_WaitPheseWait(t, st));
//        if (mtd != null)
//            Invoke(mtd, t + 0.1f);
//    }

//    private void WaitPhese(float t, ST st, string mtd = null)
//    {
//        Task.Run(() => t_WaitPhese(t, st));
//        if (mtd != null)
//            Invoke(mtd, t + 0.1f);
//    }

//    /// <summary>
//    /// WAITフェーズでt秒待ってフラグ変化
//    /// </summary>
//    /// <param name="t"></param>
//    /// <param name="st"></param>
//    /// <returns></returns>
//    private async Task t_WaitPheseWait(float t, ST st)
//    {
//        Phese = ST.WAIT;
//        int tim = (int)(t * 1000);
//        await Task.Delay(tim);
//        Phese = st;
//    }

//    private async Task t_WaitPhese(float t, ST st)
//    {
//        // Phese = ST.WAIT;
//        int tim = (int)(t * 1000);
//        await Task.Delay(tim);
//        Phese = st;
//    }

//}
