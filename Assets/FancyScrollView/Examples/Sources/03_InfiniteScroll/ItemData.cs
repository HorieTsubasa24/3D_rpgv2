﻿using System;
using UnityEngine;

namespace FancyScrollView.Example03
{
    [Serializable]
    public class ItemData
    {
        [SerializeField]
        private string message;
        public string Message { get { return message; } }

        public int Index;

        public ItemData(string message)
        {
            this.message = message;
        }

        public ItemData(string message, int index)
        {
            this.message = message;
            Index = index;
        }
    }
}
