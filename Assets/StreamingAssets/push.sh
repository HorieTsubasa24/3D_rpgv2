cd `dirname $0`


function upload() {
ftp -n << END
    open terainast.html.xdomain.jp
    user terainast.html.xdomain.jp Passtsu0
    binary
    put $1 $2
    close
    bye
END
    echo "push"
    echo $1
    echo $2
}

local_paths=()
remote_paths=()
echo $PWD
can="OK"

while read row; do
  local=`echo ${row} | cut -d , -f 1`
  remote=`echo ${row} | cut -d , -f 2`
  local_to_remote=`echo ${row} | cut -d , -f 3`

  if [ $local_to_remote = $can ]; then
    local_paths+=( $PWD$local )
    remote_paths+=( $remote )
  fi
done < TableUpdateListUnity.csv

idx=0
for loc in ${local_paths[@]}
do
  upload $loc ${remote_paths[$idx]}
  let idx++
done

#if [test -e VERSION.txt]; then
#  rm VERSION.txt
#fi
touch VERSION.txt
date "+%Y/%m/%d %H:%M:%S" > VERSION.txt

upload $PWD"/VERSION.txt" /VERSION.txt
