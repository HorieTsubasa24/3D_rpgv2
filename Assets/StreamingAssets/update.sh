cd `dirname $0`


function download() {
ftp -n << END
    open terainast.html.xdomain.jp
    user terainast.html.xdomain.jp Passtsu0
    get $1 $2
    close
    bye
END
    #echo $1
    #echo $2
}

l1=0
l2=0
l3=0
l4=0
l5=0
l6=0
local_version=""
if [[ -f $PWD"/VERSION.txt" ]]; then
  local_version=`cat VERSION.txt`
  echo "local"
  echo ${local_version}
  l1=`echo $local_version | cut -c 1-4`
  l1=`expr ${l1} + 0`
  l2=`echo $local_version | cut -c 6-7`
  l2=`expr ${l2} + 0`
  l3=`echo $local_version | cut -c 9-10`
  l3=`expr ${l3} + 0`
  l4=`echo $local_version | cut -c 12-13`
  l4=`expr ${l4} + 0`
  l5=`echo $local_version | cut -c 15-16`
  l5=`expr ${l5} + 0`
  l6=`echo $local_version | cut -c 18-19`
  l6=`expr ${l6} + 0`
fi

download /VERSION.txt $PWD"/VERSION_tmp.txt"
remote_version=`cat VERSION_tmp.txt`
rm VERSION_tmp.txt
echo "remote"
echo ${remote_version}

r1=`echo $remote_version | cut -c 1-4`
r1=`expr ${r1} + 0`
r2=`echo $remote_version | cut -c 6-7`
r2=`expr ${r2} + 0`
r3=`echo $remote_version | cut -c 9-10`
r3=`expr ${r3} + 0`
r4=`echo $remote_version | cut -c 12-13`
r4=`expr ${r4} + 0`
r5=`echo $remote_version | cut -c 15-16`
r5=`expr ${r5} + 0`
r6=`echo $remote_version | cut -c 18-19`
r6=`expr ${r6} + 0`

# 年月日時分秒のどれかがリモートの方が新しい
if [ $l1 -lt $r1 ] || [ $l2 -lt $r2 ] || [ $l3 -lt $r3 ] || [ $l4 -lt $r4 ] || [ $l5 -lt $r5 ] || [ $l6 -lt $r6 ]; then
  sh pull.sh
else
  sh push.sh
fi

