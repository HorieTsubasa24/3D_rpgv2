﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;

public class ToDo : EditorWindow {

	//Position of the vertical scoll slider
	private Vector2 scrollPos = Vector2.zero;
    private bool haveRead = false;

	//Initialize window
	[MenuItem("Window/To Do")]
	static void Init(){
        ToDo window = (ToDo)EditorWindow.GetWindow (typeof(ToDo));
        window.haveRead = false;
        window.importants.Clear();
        window.monoScripts.Clear();
        window.str_inscript.Clear();
        window.str_todo.Clear();
        window.lineCounts.Clear();
        window.Show ();
	}

    private List<bool> importants = new List<bool>();
    private List<MonoScript> monoScripts = new List<MonoScript>();
    private List<string> str_inscript = new List<string>();
    private List<string> str_todo = new List<string>();
    private List<int> lineCounts = new List<int>();

    void OnGUI()
	{
        if (haveRead == true) {
            //Create scroll slider
            scrollPos = GUILayout.BeginScrollView(scrollPos);
            

            for (int i = 0; i < str_todo.Count; i++) {
                if (i == 0 || monoScripts[i].name != monoScripts[i - 1].name)
                    GUILayout.Label("In script \"" + monoScripts[i].name + "\"", EditorStyles.boldLabel);

                //Draw ToDo line
                GUILayout.BeginHorizontal();
                if (!importants[i]) {
                    GUI.backgroundColor = Color.yellow;
                } else {
                    GUI.backgroundColor = Color.red;
                }
                if (GUILayout.Button(str_todo[i] + " at line " + lineCounts[i].ToString(), EditorStyles.textArea)) {
                    AssetDatabase.OpenAsset(monoScripts[i], lineCounts[i]);
                }
                GUILayout.EndHorizontal();
            }
            GUILayout.EndScrollView();
            return;
        }

		//Get all directories in assets folder
		string[] assetPaths = AssetDatabase.GetAllAssetPaths();

		//List of scripts
		List<MonoScript> scripts = new List<MonoScript> ();

		//Get all scripts in assets folder
		foreach(string assetPath in assetPaths)
		{
			if (!assetPath.Contains ("/Editor/")) {
				if (assetPath.EndsWith (".cs")) {
					scripts.Add (AssetDatabase.LoadAssetAtPath<MonoScript> (assetPath));
				}
			}
		}

		//Create scroll slider
		scrollPos = GUILayout.BeginScrollView (scrollPos);
        
		//Finding ToDos
		foreach (MonoScript script in scripts) {


            string sub;
			sub = script.text;

			//For counting the lines
			int lineCount = 0;

			if (sub.Contains ("//TODO:")) {
				GUILayout.Label ("In script \"" + script.name + "\"", EditorStyles.boldLabel);
                str_inscript.Add("In script \"" + script.name + "\"");

            }

			while (sub != "") {
				if (sub.Contains ("//TODO:")) {
					//Find index of next ToDo, do not desplay the //todo part(hence the 8)
					int start = sub.IndexOf ("//TODO:") + 7;

					bool important = (sub[start] == '!') || (sub[start + 1] == '!');

					//Remove space
					if (sub [start] == ' ') {
						sub = sub.Substring (1);
					}

					//Remove the '!' from substring
					if (sub[start] == '!') {
						sub = sub.Substring (1);
					}

					//Find /n ending the line of the current ToDo
					int end = sub.Substring (start).IndexOf ("\n");

					//Count line number
					for (int i = 0; i < start + end + 1; i++) {
						if (sub [i] == '\n') {
							lineCount++;
						}
					}

                    //Get the string of the current ToDo
                    string todo = sub.Substring (start, end);
                    //Decrease the size of current script
                    sub = sub.Substring (start + end + 1);

                    importants.Add(important);
                    str_todo.Add(todo);
                    lineCounts.Add(lineCount);
                    monoScripts.Add(script);

                    //Draw ToDo line
                    GUILayout.BeginHorizontal ();
					if (!important) {
						GUI.backgroundColor = Color.yellow;
					} else {
						GUI.backgroundColor = Color.red;
					}
					if (GUILayout.Button (todo + " at line " + lineCount.ToString (), EditorStyles.textArea)) {
						AssetDatabase.OpenAsset (script, lineCount);
					}
					GUILayout.EndHorizontal ();
				} else {
					//Reset the substring and line count to prepare the next file
					lineCount = 0;
                    sub = "";
				}
			}
		}

        haveRead = true;
		GUILayout.EndScrollView ();
	}

}
